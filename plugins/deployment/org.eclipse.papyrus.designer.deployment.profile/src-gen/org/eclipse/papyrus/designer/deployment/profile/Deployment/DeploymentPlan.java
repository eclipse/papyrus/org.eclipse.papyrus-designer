/**
 */
package org.eclipse.papyrus.designer.deployment.profile.Deployment;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A deployment plan contains a set of instances along with their configuration. It can reference a transformation chain and optionally a set of additional transformations via the supertype ExecuteTransformationChain. The projectMapping allows to control the default name
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.deployment.profile.Deployment.DeploymentPlan#getProjectMappings <em>Project Mappings</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.deployment.profile.Deployment.DeploymentPackage#getDeploymentPlan()
 * @model
 * @generated
 */
public interface DeploymentPlan extends ExecuteTrafoChain {
	/**
	 * Returns the value of the '<em><b>Project Mappings</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Mappings</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Mappings</em>' attribute list.
	 * @see org.eclipse.papyrus.designer.deployment.profile.Deployment.DeploymentPackage#getDeploymentPlan_ProjectMappings()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	EList<String> getProjectMappings();

} // DeploymentPlan
