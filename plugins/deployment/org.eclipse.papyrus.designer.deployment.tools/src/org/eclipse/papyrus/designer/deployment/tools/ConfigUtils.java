/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.deployment.tools;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.deployment.profile.Deployment.ConfigurationProperty;
import org.eclipse.papyrus.designer.deployment.profile.Deployment.UseInstanceConfigurator;
import org.eclipse.papyrus.designer.transformation.base.preferences.TransfoBasePreferenceUtils;
import org.eclipse.papyrus.designer.transformation.extensions.InstanceConfigurator;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ConfigUtils {

	/**
	 * Get the configuration properties of a class
	 *
	 * @param component
	 *            a class or component
	 * @return a list of configuration properties
	 */
	public static EList<Property> getConfigAttributes(Classifier component) {

		EList<Property> list = new BasicEList<Property>();

		for (Property part : component.getAllAttributes()) {
			if (TransfoBasePreferenceUtils.allAttributesAreConfigAttributs()) {
				// return all attributes
				list.add(part);
			}
			// otherwise add only attributes tagged as configuration properties
			else if (StereotypeUtil.isApplied(part, ConfigurationProperty.class)) {
				list.add(part);
			}
		}
		return list;
	}

	/**
	 * Configure an instance. The configurator is selected by means of a stereotype on the classifier of
	 * the passed instance.
	 *
	 * @see org.eclipse.papyrus.designer.transformation.extensions.IInstanceConfigurator
	 * @param instance
	 *            specification of the instance that should be configured
	 * @param part
	 *            the part representing the instance (before container expansion)
	 * @param parentInstance
	 *            instance specification of the parent (i.e. having the passed instance as instance value)
	 */
	public static void configureInstance(InstanceSpecification instance, Property part, InstanceSpecification parentInstance) {
		Classifier component = DepUtils.getClassifier(instance);
		UseInstanceConfigurator useInstanceConfigurator = UMLUtil.getStereotypeApplication(component, UseInstanceConfigurator.class);
		InstanceConfigurator.configureInstance(useInstanceConfigurator, instance, part, parentInstance);
	}
}
