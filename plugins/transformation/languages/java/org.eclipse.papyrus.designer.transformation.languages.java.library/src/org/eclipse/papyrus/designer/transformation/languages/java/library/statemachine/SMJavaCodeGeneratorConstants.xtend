/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *  Ported from C++ code - Bug 568883
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

/**
 * Code generation constants that are specific for Java
 */
class SMJavaCodeGeneratorConstants {
	public static final String PROCESSFROM = "processFrom"
	public static final String PROCESSEVENT = "processEvent"

	public static final String STRUCT_FOR_THREAD = "statemachine.StructForThread_t"

	public static final String THREAD_FUNC_TIMEEVENT_TYPE = "ThreadFunctions.TF_TIME_EVENT"
	public static final String THREAD_FUNC_CHANGEEVENT_TYPE = "ThreadFunctions.TF_CHANGE_EVENT"
	public static final String THREAD_FUNC_DOACTIVITY_TYPE = "ThreadFunctions.TF_DO_ACTIVITY"
	public static final String THREAD_FUNC_ENTER_REGION_TYPE = "ThreadFunctions.TF_ENTER_REGION"
	public static final String THREAD_FUNC_EXIT_REGION_TYPE = "ThreadFunctions.TF_EXIT_REGION"
	public static final String THREAD_FUNC_TRANSITION_TYPE = "ThreadFunctions.TF_TRANSITION"
	public static final String THREAD_FUNC_STATE_MACHINE_TYPE = "ThreadFunctions.TF_STATE_MACHINE_TYPE"
	
	/**
	 * Qualified names of used types
	 */
	public static final String RUNNABLE_QNAME = "java::lang::Runnable"
	public static final String SEMAPHORE_QNAME = "java::util::concurrent::Semaphore"	
}
