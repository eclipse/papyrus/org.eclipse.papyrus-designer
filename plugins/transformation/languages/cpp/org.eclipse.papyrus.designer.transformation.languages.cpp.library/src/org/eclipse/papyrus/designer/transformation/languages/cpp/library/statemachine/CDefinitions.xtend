/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine
 
import org.eclipse.uml2.uml.Class

class CDefinitions {
	new (Class contextClass) {
		this.contextClass = contextClass;
	}
	Class contextClass
	
	def String TIME_EVENT_LOWER_BOUND() {
		macro("TIME_EVENT_LOWER_BOUND")
	}
	
	def String CHANGE_EVENT_LOWER_BOUND() {
		macro("CHANGE_EVENT_LOWER_BOUND")
	}
	
	def String TE_INDEX() {
		macro("TE_INDEX")
	}
	
	def String CHE_INDEX() {
		macro("CHE_INDEX")
	}
	
	/**
	 * return "qualified" macro name, i.e. macro name prefixed with class name
	 */
	def macro(String shortMacroName) {
		return contextClass.name.toUpperCase + "_" + shortMacroName
	}
}
