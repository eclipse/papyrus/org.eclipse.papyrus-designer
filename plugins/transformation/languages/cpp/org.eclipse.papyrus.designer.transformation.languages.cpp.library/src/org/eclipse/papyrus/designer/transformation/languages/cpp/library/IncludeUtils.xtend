package org.eclipse.papyrus.designer.transformation.languages.cpp.library

import org.eclipse.uml2.uml.Class
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.*

class IncludeUtils {
	def static appendIncludeHeader(Class clazz, String appended) {
		val include = clazz.applyApp(Include)
		val header = include.header
		include.header = '''
			«header»
			«appended»'''
	}

	def static appendIncludeBody(Class clazz, String appended) {
		val include = clazz.applyApp(Include)
		val body = include.body
		include.body = '''
			«body»
			«appended»'''
	}
}