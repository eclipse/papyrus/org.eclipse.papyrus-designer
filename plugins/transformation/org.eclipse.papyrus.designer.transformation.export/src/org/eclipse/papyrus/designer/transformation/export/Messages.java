/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.export;

import org.eclipse.osgi.util.NLS;

/**
 * @author ansgar
 *
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.transformation.export.messages"; //$NON-NLS-1$
	public static String ExportElements_EXPORT_SELECTED_ELEMENT;
	public static String ExportElements_EXPORT_SELECTED_ELEMENT_NEW_UML;
	public static String ExportElements_FILE_NAME;
	public static String ExportElements_NEW_MODEL_NAME;
	public static String ExportElements_OUTPUT_DIRECTORY;
	public static String ExportElements_SELECT;
	public static String ExportElements_TRANSITIVE_CLOSURE;
	public static String ExportElements_ERROR_MSG;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
