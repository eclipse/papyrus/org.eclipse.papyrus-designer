package org.eclipse.papyrus.designer.transformation.library.transformations;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.Messages;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Package;

public class WriteModels implements IM2MTrafoCDP {

	/**
	 * The location of the generated models
	 */
	public static final String MODEL = "model"; //$NON-NLS-1$

	/**
	 * The location of the temporary model (relative to current project).
	 * TODO: make configurable?
	 */
	public static final String TEMP_MODEL_FOLDER = "tmpModel"; //$NON-NLS-1$

	public static final String TEMP_EXT = "." + ModelManagement.TEMP_UML;  //$NON-NLS-1$

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {

		TransformationContext tc = TransformationContext.current;
		boolean intermediateModel = tc.copier.source == TransformationContext.initialSourceRoot;

		// setURIs and save (first update all URIs to ensure that model references use the
		// correct URI before saving)
		String originalURI = tc.mm.getModel().eResource().getURI().toString();
		if (intermediateModel) {
			tc.mm.setURI(tc.project, TEMP_MODEL_FOLDER, TEMP_EXT);
		}
		else {
			tc.mm.setURI(tc.project, MODEL, null);
		}
		tc.mm.save();
		// restore original URI after saving
		tc.mm.setURI(originalURI);
		
		// also save additional root elements
		Map<String, Boolean> pathes = new HashMap<String, Boolean>(); 
		for (ModelManagement mm : tc.copier.getAdditionalRootPkgs()) {
			String originalAdditionalURI = tc.mm.getModel().eResource().getURI().toString();
			String pathAdditionalRoot;
			if (intermediateModel) {
				pathAdditionalRoot = mm.getPath(tc.project, TEMP_MODEL_FOLDER, TEMP_EXT);
			}
			else {
				pathAdditionalRoot = mm.getPath(tc.project, MODEL, null);
			}
			mm.setURI(pathAdditionalRoot);
				if (pathes.containsKey(pathAdditionalRoot)) {
				throw new TransformationException(String.format
						(Messages.ExecuteTransformation_ROOT_EXISTS_TWICE,
								pathAdditionalRoot));
			}
			pathes.put(pathAdditionalRoot, true);
			mm.save();
			mm.setURI(originalAdditionalURI);
		}
	}
}
