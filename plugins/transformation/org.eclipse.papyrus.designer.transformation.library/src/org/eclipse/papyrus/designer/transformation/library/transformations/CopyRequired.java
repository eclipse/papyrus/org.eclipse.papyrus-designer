/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.library.transformations;

import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoModelSplit;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyExtResources;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.core.transformations.filters.FilterProfiles;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * Copy elements to a new model. This implies that required elements are copied to the
 * "additional root" models. These are used in turn for code generation.
 */
public class CopyRequired implements IM2MTrafoModelSplit {
	
	@Override
	public List<TransformationContext> splitModel(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		Package existingModel = TransformationContext.current.modelRoot;
		ModelManagement genModelManagement = ModelManagement.createNewModel(existingModel);
		Package generatedModel = genModelManagement.getModel();

		LazyCopier targetCopier = new LazyCopier(existingModel, generatedModel, CopyExtResources.ALL, true);
		targetCopier.preCopyListeners.add(FilterProfiles.getInstance());

		// create a copy, side effect: referenced element outside the resource are copied into
		// additional models for which code gets generated.
		for (PackageableElement e : deploymentPlan.getPackagedElements()) {
			targetCopier.copy(e);
		}
		TransformationContext tc = new TransformationContext();

		tc.modelRoot = generatedModel;
		tc.copier = targetCopier;
		tc.mm = genModelManagement;
		tc.deploymentPlan = targetCopier.getCopy(deploymentPlan);
		// no need to update TransformationContext.current, will be handled by caller
		return Collections.singletonList(tc);
	}
}