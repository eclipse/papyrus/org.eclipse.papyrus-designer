/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.designer.transformation.library.xtend

import org.eclipse.uml2.uml.DataType
import org.eclipse.papyrus.designer.transformation.core.templates.TemplateInstantiation
import org.eclipse.papyrus.designer.transformation.extensions.ITextTemplate

class FIFO implements ITextTemplate {
	def activate(DataType datatype) '''
		if (m_size == 0) {
			cerr << "Warning: size of FIFO is not properly configured (size = 0)" << endl;
		}
		m_fifo = new «TemplateInstantiation.context.pkgTemplateParameter("T")»[m_size];
	'''
}