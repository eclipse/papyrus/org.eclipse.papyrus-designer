/*****************************************************************************
 * Copyright (c) 2016, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.library.transformations;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.deployment.tools.Activator;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.languages.common.base.HintUtils;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.transformation.base.UIContext;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.Messages;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

public class GenerateCode implements IM2MTrafoCDP {

	public void generate(InstanceSpecification node, String targetLanguage)
			throws TransformationException, InterruptedException
	{
		IProgressMonitor monitor = UIContext.monitor;
		IProject genProject = TransformationContext.current.project;
		
		Package genModel = TransformationContext.current.mm.getModel();

		monitor.worked(1);
		if (genProject == null) {
			throw new TransformationException(Messages.GenerateCode_NoProject);
		}

		if (node == null) {
			monitor.setTaskName(String.format(Messages.GenerateCode_GeneratingCode, targetLanguage));
		}
		else {
			monitor.setTaskName(String.format(Messages.GenerateCode_GeneratingCodeForNode, targetLanguage, node.getName()));
		}
	
		ILangCodegen codegen = LanguageCodegen.getGenerator(targetLanguage);

		codegen.generateCode(genProject, genModel, monitor);

		// the generated model can contain more than one top-level element due to copied external model references
		for (ModelManagement mm : TransformationContext.current.copier.getAdditionalRootPkgs()) {
			codegen.generateCode(genProject, mm.getModel(), monitor);
		}
		codegen.cleanCode(genProject, genModel, TransformationContext.current.keepFiles, monitor);
	
		if (monitor.isCanceled()) {
			return;
		}
		monitor.worked(1);
	}

	/**
	 * @param element
	 * @return the nearest classifier or package
	 */
	public PackageableElement getNearestClassifierOrPackage(Element element) {
		while (element != null) {
			if (element instanceof Classifier) {
				return (Classifier) element;
			}
			if (element instanceof Package) {
				return (Package) element;
			}
			element = element.getOwner();
		}
		return null;
	}

	/*
	 * public void deleteRemovedClasses(EObject diffModelOrElement) throws TransformationException {
	 * EList<DiffElement> diffs;
	 * if(diffModelOrElement instanceof DiffModel) {
	 * diffs = ((DiffModel)diffModelOrElement).getDifferences();
	 * }
	 * else if(diffModelOrElement instanceof DiffElement) {
	 * diffs = ((DiffElement)diffModelOrElement).getSubDiffElements();
	 * }
	 * else {
	 * return;
	 * }
	 * for(DiffElement diff : diffs) {
	 * EObject modifiedEObj = null;
	 *
	 * if(diff.getKind() == DifferenceKind.DELETION) {
	 * if(diff instanceof AttributeChange) {
	 * modifiedEObj = ((AttributeChange)diff).getRightElement();-
	 * }
	 * else if(diff instanceof ModelElementChangeRightTarget) {
	 * modifiedEObj = ((ModelElementChangeRightTarget)diff).getRightElement();
	 * }
	 * }
	 * else if(diff.getKind() == DifferenceKind.CHANGE) {
	 * if(diff instanceof AttributeChange) {
	 * modifiedEObj = ((AttributeChange)diff).getRightElement();
	 * }
	 * }
	 * if(modifiedEObj instanceof PackageableElement) {
	 * // => delete tree
	 * langSupport.cleanCode(monitor, (PackageableElement)modifiedEObj);
	 * }
	 * // no recursion needed?
	 * // deleteRemovedClasses(diff);
	 * }
	 * }
	 */

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		// get first language (restricted to single target language, acceptable?)
		EList<InstanceSpecification> topLevelInstances = DepUtils.getTopLevelInstances(deploymentPlan);
		String targetLanguage =
				topLevelInstances.size() > 0 ?
						DepUtils.getTargetLanguage(topLevelInstances.iterator().next()) :
						HintUtils.getLanguageFromElement(deploymentPlan);
		try {
			generate(null, targetLanguage);
		}
		catch (InterruptedException e) {
			Activator.log.error(e);
		}
	}
}
