/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.designer.transformation.library.statemachine

import org.eclipse.uml2.uml.State
import java.util.List
import java.util.Map.Entry
import java.util.ArrayList
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.*

import java.util.HashMap
import java.util.Map
import org.eclipse.uml2.uml.Region
import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Vertex
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.papyrus.designer.transformation.base.utils.OperationSync

class SMCommon {
	
	static boolean firstIf;
	
	def static int findMax(List<Integer> l) {
		var ret = l.head
		for (i : l) {
			if (i > ret) {
				ret = i
			}
		}
		return ret
	}
	
	def static int calculateDepth(Region topRegion, State s) {
		if (s.container == topRegion) {
			return 1
		} else {
			return 1 + topRegion.calculateDepth(s.container.state)
		}
	}
	
	def static List<Entry<List<State>, Integer>> calculateAcceptingStates(Region topRegion, State parent, List<State> acceptingStates) {
		var transitiveSubstates = parent.transitiveSubStates
		val retTmp = new ArrayList<State>
		transitiveSubstates.forEach [
			if (acceptingStates.contains(it)) {
				retTmp.add(it)
			}
		]

		// 0->sequential, 1->parallel
		val List<Entry<List<State>, Integer>> ret = new ArrayList
		// sorting the result, the first item is the deepest state
		val Map<Integer, List<State>> map = new HashMap
		retTmp.forEach [
			var depth = topRegion.calculateDepth(it)
			if (!map.containsKey(depth)) {
				map.put(depth, new ArrayList)
			}
			map.get(depth).add(it)
		]

		var maxDepth = findMax(map.keySet.toList)
		for (var i = 0; i <= maxDepth; i++) {
			if (map.containsKey(i)) {
				if (map.get(i).size == 1) {
					var newList = new ArrayList<State>
					newList.addAll(map.get(i))
					var mapEntry = new HashMap<List<State>, Integer>
					mapEntry.put(newList, 0)
					ret.addAll(mapEntry.entrySet)
				} else {
					val Map<State, List<State>> m = new HashMap
					for (s : map.get(i)) {
						if (!m.containsKey(s.container.state)) {
							m.put(s.container.state, new ArrayList)
						}
						m.get(s.container.state).add(s)
					}
					for (en : m.entrySet) {
						var mapEntry = new HashMap<List<State>, Integer>
						var newList = new ArrayList<State>
						newList.addAll(en.value)
						if (en.key.orthogonal) {
							mapEntry.put(newList, 1)
							ret.addAll(mapEntry.entrySet)
						} else {
							mapEntry.put(newList, 0)
							ret.addAll(mapEntry.entrySet)
						}
					}
				}
			}
		}

		return ret
	}
	
	// get transitive parent states
	def static List<State> getTransitiveParentStates(Vertex s) {
		var ret = new ArrayList<State>
		var parent = s.container.state
		while (parent !== null) {
			ret.add(parent)
			parent = parent.container.state
		}
		return ret
	}
	
//	def static List<State> getStateHierarchy(Region topRegion, State s) {
//		var ret = new ArrayList<State>
//		var parent = s.container.state
//		while (parent !== null && parent.container != topRegion) {
//			ret.add(0, parent)
//			parent = parent.container.state
//		}
//		return ret
//	}

	// check whether the substate of a parent state accepts the event
	def static checkSubStatesAcceptEvent(State parent, List<Transition> transitions) {
		val substates = new ArrayList<State>
		parent.regions.forEach [
			substates.addAll(it.subvertices.filter(State))
		]
		var ret = false
		for (t : transitions) {
			if (!ret && substates.contains(t.source)) {
				ret = true
			}
		}
		return ret
	}

	// check whether the transitive substates of a parent state accepts the event
	def static checkTransitiveSubStatesAcceptEvent(State parent, List<Transition> transitions) {
		val substates = parent.transitiveSubStates()
		var ret = false
		for (t : transitions) {
			if (!ret && substates.contains(t.source)) {
				ret = true
			}
		}
		return ret
	}

	def static copyParameters(Operation source, Operation target, boolean isCopyReturn) {
		var name = target.name
		OperationSync.sync(source, target)
		if (!isCopyReturn) {
			var ret = target.ownedParameters.filter[it.direction == ParameterDirectionKind.RETURN_LITERAL]
			target.ownedParameters.removeAll(ret)
		}
		for (stt : target.stereotypeApplications) {
			target.apply(stt.class)
		}
		target.name = name
	}

	/**
	 * init "else", see condElse
	 */
	def static initCondElse() {
		firstIf = true;
	}

	/**
	 * return an "else " when called more than once.
	 * => helps creating "else if" instead of "isProcessed" (bug 549801O) in multiple "if"
	 * statements
	 */
	def static condElse() {
		if (!firstIf) {
			return "else "
		}
		else {
			firstIf = false;
			return ""
		}
	}

	/**
	 * Return all outgoing transitions of a vertex, including those
	 * of targeted vertices that are not states (transitive hull).
	 */	
	def static List<Transition> findTrans(Vertex v) {
		var ret = new UniqueEList<Transition>
		ret.addAll(v.outgoings)
		for (out : v.outgoings) {
			if (!(out.target instanceof State)) {
				ret.addAll(out.target.findTrans)
			}
		}
		return ret
	}

	def static List<State> getRootStates(Region topRegion, List<State> states) {
		val ret = new ArrayList<State>
		ret.addAll(states.filter[it.container == topRegion])
		return ret
	}

	def static getRegionNumber(Region topRegion, State child) {
		if (child.container == topRegion) {
			return 0
		}
		return child.container.state.regions.indexOf(child.container)
	}
	
}