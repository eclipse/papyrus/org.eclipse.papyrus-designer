/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.Messages;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoElem;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoModelSplit;
import org.eclipse.papyrus.designer.transformation.extensions.IM2MTrafo;
import org.eclipse.papyrus.designer.transformation.extensions.M2MTrafoExt;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Package;

/**
 * Execute a transformation for each element of the model
 */
public class ExecuteTransformation {

	public static void apply(Iterator<M2MTrafo> m2mPropertyIter) throws TransformationException {
		if (!m2mPropertyIter.hasNext()) {
			return;
		}
		// is the monitor canceled? => stop execution
		if (TransformationContext.monitor.isCanceled()) {
			throw new TransformationException(ExecuteTransformationChain.USER_CANCEL);
		}
		M2MTrafo m2mTrafo = m2mPropertyIter.next();

		IM2MTrafo eTrafo = M2MTrafoExt.getM2MTrafo(m2mTrafo);
		Package deploymentPlan = TransformationContext.current.deploymentPlan;
		if (eTrafo instanceof IM2MTrafoElem) {
			ApplyRecursive ar = new ApplyRecursive(m2mTrafo, (IM2MTrafoElem) eTrafo);
			ar.applyRecursive(PackageUtil.getRootPackage(deploymentPlan));
			apply(m2mPropertyIter);
		} else if (eTrafo instanceof IM2MTrafoCDP) {
			((IM2MTrafoCDP) eTrafo).applyTrafo(m2mTrafo, deploymentPlan);
			apply(m2mPropertyIter);
		} else if (eTrafo instanceof IM2MTrafoModelSplit) {
			List<TransformationContext> newContexts = ((IM2MTrafoModelSplit) eTrafo).splitModel(m2mTrafo, deploymentPlan);
			// create list of remaining M2M transformations
			EList<M2MTrafo> remainingTrafos = new BasicEList<M2MTrafo>();
			while (m2mPropertyIter.hasNext()) {
				remainingTrafos.add(m2mPropertyIter.next());
			}
			// now apply to each created sub-model
			for (TransformationContext newContext : newContexts) {
				TransformationContext.setContext(newContext);
				apply(remainingTrafos.iterator());
				TransformationContext.popContext();

				newContext.mm.dispose();
				for (ModelManagement mm : newContext.copier.getAdditionalRootPkgs()) {
					mm.dispose();
				}
			}
		} else {
			throw new TransformationException(Messages.ExecuteTransformation_UNKNOWN_M2M + m2mTrafo.toString());
		}
	}
}
