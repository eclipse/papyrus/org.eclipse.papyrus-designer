/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 */

package org.eclipse.papyrus.designer.transformation.core.templates;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.ConstInit;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.Messages;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.uml.tools.utils.StUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.util.UMLUtil;

public class BindingUtils {

	/**
	 * Bind an operation by copying the name, an eventual ConstInit and the signature from the actual.
	 *
	 * @param actual
	 *            the actual. If an operation, its signature is copied to the template
	 * @param operation
	 *            The operation template
	 */
	public static void instantiateOperation(Element actual, Operation operation) {
		try {
			TransformationContext.current.classifier = operation.getClass_();
			// set new name
			String newName = TextTemplateBinding.bind(operation.getName(), actual, null);
			operation.setName(newName);
			// perform binding in case of C++ initializer
			ConstInit cppConstInit = UMLUtil.getStereotypeApplication(operation, ConstInit.class);
			if (cppConstInit != null) {
				// TODO: specific to C++
				String init = cppConstInit.getInitialisation();
				String newInit = TextTemplateBinding.bind(init, actual);
				cppConstInit.setInitialisation(newInit);
			}
			if (actual instanceof Operation) {
				Operation actualOp = (Operation) actual;
				for (Parameter p : actualOp.getOwnedParameters()) {
					Parameter pCopy = EcoreUtil.copy(p);
					operation.getOwnedParameters().add(pCopy);
					StUtils.copyStereotypes(p,  pCopy);
				}

			}
		} catch (TransformationException e) {
			// throw runtime exception
			throw new RuntimeException(String.format(Messages.TemplateInstantiationListener_TrafoException, e.getMessage()));
		}
	}

	/**
	 * Instantiate a behavior, i.e. check whether the body text consists of a text template
	 * reference.  If yes, instantiate the text template.
	 *
	 * @param actual
	 *            actual in template instantiation
	 * @param opaqueBehavior
	 *            behavior with body containing eventually a text template reference
	 * @throws TransformationException
	 */
	public static void instantiateBehavior(Element actual, OpaqueBehavior opaqueBehavior) throws TransformationException {
		if (actual instanceof NamedElement) {
			String newName = TextTemplateBinding.bind(opaqueBehavior.getName(), actual, null);
			if (newName != null && !newName.equals(opaqueBehavior.getName())) {
				opaqueBehavior.setName(newName);
			}
		}
		EList<String> bodyList = opaqueBehavior.getBodies();
		for (int i = 0; i < bodyList.size(); i++) {
			String body = bodyList.get(i);
			TransformationContext.current.classifier = (Classifier) opaqueBehavior.getOwner();
			// pass qualified operation name as template name. Used to identify script in case of an error
			String newBody = TextTemplateBinding.bind(body, actual);
			if (newBody != null && !newBody.equals(body)) {
				bodyList.set(i, newBody);
			}
		}
	}
}
