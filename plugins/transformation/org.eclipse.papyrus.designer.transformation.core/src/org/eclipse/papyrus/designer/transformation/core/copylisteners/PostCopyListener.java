/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.copylisteners;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;

/**
 * Interface for listeners that are notified when an eObject
 * is copied
 *
 * @see org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier
 */
public interface PostCopyListener {

	/**
	 * Is called for each EObject after is has been copied.
	 *
	 * @param copier
	 *            a lazy copier
	 * @param targetEObj
	 *            the EObject that has been copied
	 */
	public void postCopyEObject(LazyCopier copier, EObject targetEObj);
}
