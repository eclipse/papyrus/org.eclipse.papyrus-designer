/*****************************************************************************
 * Copyright (c) 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Utility function to obtain M2M rules and M2M chains
 */
public class M2MTrafoUtil {
	/**
	 * get a list of transformation rules from a package
	 * 
	 * @param pkg
	 *            a package in which to search for rules
	 * @return the list of rules (eventually empty)
	 */
	public static List<M2MTrafo> getTransformations(Package pkg) {
		List<M2MTrafo> m2mRuleList = new ArrayList<M2MTrafo>();
		getTransformations(pkg, new ArrayList<Package>(), m2mRuleList);
		return m2mRuleList;
	}

	/**
	 * get a list of transformation chains from a package
	 * 
	 * @param pkg
	 *            a package in which to search for chains
	 * @return the list of chains (eventually empty)
	 */
	public static List<M2MTrafoChain> getTransformationChains(Package pkg) {
		List<M2MTrafoChain> chainList = new ArrayList<M2MTrafoChain>();
		getTransformationChains(pkg, new ArrayList<Package>(), chainList);
		return chainList;
	}

	/**
	 * Internal helper to get the list of rules
	 * 
	 * @param pkg
	 *            a package in which to search for rules
	 * @param visitedPackages
	 *            a list of packages already visited
	 * @param m2mRuleList
	 *            the list of found rules
	 */
	protected static void getTransformations(Package pkg, List<Package> visitedPackages, List<M2MTrafo> m2mRuleList) {
		for (Element el : pkg.getMembers()) {
			if (el instanceof Package) {
				if (!visitedPackages.contains(el)) {
					visitedPackages.add((Package) el);
					getTransformations((Package) el, visitedPackages, m2mRuleList);
				}
			} else if (el instanceof Class) {
				M2MTrafo rule = UMLUtil.getStereotypeApplication(el, M2MTrafo.class);
				if (rule != null) {
					m2mRuleList.add(rule);
				}
			}
		}
	}

	/**
	 * Internal helper to get the list of chains
	 * 
	 * @param pkg
	 *            a package in which to search for chains
	 * @param visitedPackages
	 *            a list of packages already visited
	 * @param m2mRuleList
	 *            the list of found chains
	 */
	protected static void getTransformationChains(Package pkg, List<Package> visitedPackages, List<M2MTrafoChain> chainList) {
		for (Element el : pkg.getMembers()) {
			if (el instanceof Package) {
				if (!visitedPackages.contains(el)) {
					visitedPackages.add((Package) el);
					getTransformationChains((Package) el, visitedPackages, chainList);
				}
			} else if (el instanceof Class) {
				M2MTrafoChain chain = UMLUtil.getStereotypeApplication(el, M2MTrafoChain.class);
				if (chain != null) {
					chainList.add(chain);
				}
			}
		}
	}
}
