/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.core.transformations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.designer.deployment.tools.AllocUtils;
import org.eclipse.papyrus.designer.deployment.tools.ConfigUtils;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.transformation.core.Activator;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;

public class TransformationUtil {
	
	/**
	 * 
	 * @param instance an instance specification
	 * @param part the defining feature in a tree hierarchy (null for top-level instances)
	 * @param parentInstance the parent instance (null for top-level instance)
	 */
	public static void applyInstanceConfigurators(InstanceSpecification instance, Property part, InstanceSpecification parentInstance) {
		ConfigUtils.configureInstance(instance, part, parentInstance);
		for (Slot slot : instance.getSlots()) {
			InstanceSpecification subInstance = DepUtils.getInstance(slot);
			if (slot.getDefiningFeature() instanceof Property && !DepUtils.isShared(slot) && (subInstance != null)) {
				applyInstanceConfigurators(subInstance, (Property) slot.getDefiningFeature(), instance);
			}
		}
	}

	public static void propagateAllocation(InstanceSpecification instance) {
		propagateAllocation(instance, new UniqueEList<InstanceSpecification>());
	}

	/**
	 * Propagate node allocation.
	 * Background: some instances reference others via a shared slot. This means that the
	 * same instance can be accessed by multiple instances. It must therefore be allocated
	 * to all nodes that the referencing instances are allocated to.
	 *
	 * @param instance
	 * @param nodes
	 */
	public static void propagateAllocation(InstanceSpecification instance, EList<InstanceSpecification> nodes) {
		// create copy of node (otherwise, more and more nodes get aggregated.
		UniqueEList<InstanceSpecification> nodesCopy = new UniqueEList<InstanceSpecification>();
		nodesCopy.addAll(nodes);
		nodesCopy.addAll(AllocUtils.getNodes(instance));
		for (Slot slot : instance.getSlots()) {
			InstanceSpecification subInstance = DepUtils.getInstance(slot);
			if (subInstance != null) {
				if (DepUtils.isShared(slot)) {
					for (InstanceSpecification node : nodesCopy) {
						Activator.log.info(String.format("Propagate node allocation: %s to %s", subInstance.getName(), node.getName())); //$NON-NLS-1$
						AllocUtils.allocate(subInstance, node);
					}
				}
				else {
					propagateAllocation(subInstance, nodesCopy);
				}
			}
		}
	}
}