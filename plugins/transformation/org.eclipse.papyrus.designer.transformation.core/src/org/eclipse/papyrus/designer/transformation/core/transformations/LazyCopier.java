/*****************************************************************************
 * Copyright (c) 2013, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr, initial, bug 572601
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.papyrus.designer.transformation.base.utils.CopyUtils;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.core.Activator;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PostCopyListener;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PreCopyListener;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;

/**
 * A specific copier that enables to make iterative and shallow copies of model elements
 * from a source to a target model. It also supports copy-listeners, i.e. listeners that
 * might apply modifications before and after an element is copied.
 * This class is very useful for model transformations that make a lazy copy of elements,
 * i.e. copy only elements that are needed in the target model.
 *
 * Iterative means that you can copy one element after another, i.e. you do not need
 * to copy all elements in a single call.
 * Shallow means that some elements are incomplete copies. For instance, if you copy an
 * element of a package, the copy routine will create thus element within a shallow copy
 * of the original package, i.e. a package containing only a subset of the original elements.
 * Shallow copies are currently restricted to packages. A shallow copy can be transformed
 * into a "full" copy by explicitly copying it.
 */
public class LazyCopier extends Copier {

	public enum CopyStatus {
		/**
		 * The status is not known, in most cases this indicates that the object has not yet been copied.
		 */
		UNKNOWN,

		/**
		 * A full copy obtained via the copy function. Full means that the contained features have been completely
		 * copied
		 */
		FULL,

		/**
		 * A full copy in progress. Intermediate state of a target element after creation within the copy function,
		 * before all attributes & references have been copied.
		 */
		INPROGRESS,

		/**
		 * A shallow copy, i.e. a copy only containing a subset of the original element. These are typically packages.
		 * A shallow copy may become a full copy later on.
		 */
		SHALLOW
	}

	/**
	 * This enumeration controls how elements from additional resources (i.e. != resource containing the source package)
	 * should be copied.
	 */
	public enum CopyExtResources {

		/**
		 * Don't copy any additional resources, references remain in these resources
		 */
		NONE,

		/**
		 * Copy selected resources, provided via the method addResource()
		 */
		SELECTED,

		/**
		 *  Copy all, except the selected resources, provided via the method addResource()
		 */
		ALL_EXCEPT,

		/**
		 * Copy elements from all resources that are referenced by an element of the source model.
		 */
		ALL
	}

	public static final EObject USE_SOURCE_OBJECT = EcoreFactory.eINSTANCE.createEObject();
	
	/**
	 *
	 * @param source
	 *            source package (root)
	 * @param target
	 *            target package (root)
	 * @param copyExtResources
	 *            A list of resources that should be copied elements that are not within the same resource instead of referencing them.
	 * @param copyID
	 *            copyID true, if XML IDs should be copied as well.
	 */
	public LazyCopier(Package source, Package target, CopyExtResources copyExtResources, boolean copyID) {
		this.source = source;
		this.target = target;
		// useOriginalReferences = false;
		this.copyExtResources = copyExtResources;
		selectedResourceList = new UniqueEList<Resource>();
		// add resource of source package to "resources to be copied" list
		if (copyExtResources == CopyExtResources.NONE || copyExtResources == CopyExtResources.SELECTED) {
			selectedResourceList.add(source.eResource());
		}
		preCopyListeners = new BasicEList<PreCopyListener>();
		postCopyListeners = new BasicEList<PostCopyListener>();
		statusMap = new HashMap<EObject, CopyStatus>();
		put(source, target);
		setStatus(target, CopyStatus.SHALLOW);
		this.copyID = copyID;
		if (copyID) {
			CopyUtils.copyID(source, target);
		}
		rootPkgs = new BasicEList<ModelManagement>();
	};

	/**
	 *
	 */
	private static final long serialVersionUID = -1664013545661635289L;

	/**
	 * Source model within a transformation
	 */

	public Package source;

	/**
	 * Target model within a transformation
	 */
	public Package target;

	/**
	 * if true, copy packages or elements that are imported into the target
	 * model
	 */
	protected CopyExtResources copyExtResources;

	/**
	 * The list of external resources that should be copied (depending on the copyExtResource setting)
	 */
	protected EList<Resource> selectedResourceList;

	/**
	 * Map using a target EObject as key
	 */
	protected Map<EObject, CopyStatus> statusMap;

	protected boolean copyID;

	protected EList<ModelManagement> rootPkgs;
	
	/**
	 * Add the passed resource to the resources to be copied list
	 *
	 * @param resource the resource to be added
	 */
	public void addResource(Resource resource) {
		selectedResourceList.add(resource);
	}

	/**
	 * Elements from referenced resources might get copied as well and become
	 * additional root pkgs.
	 * @return a list of model management instances representing additional root packages
	 */
	public EList<ModelManagement> getAdditionalRootPkgs() {
		return rootPkgs;
	}
	
	/**
	 * Put a pair into the copy map. Unlike the standard put operation,
	 * the target object is marked as full copy.
	 * Just using the put operation leads to bug 422899 - [QDesigner] Regression in
	 * template instantiation
	 *
	 * @return the target EObject
	 */
	public EObject putPair(EObject sourceEObj, EObject targetEObj) {
		EObject target = put(sourceEObj, targetEObj);
		setStatus(targetEObj, CopyStatus.FULL);
		return target;
	}

	/**
	 * Set the status of a copy object
	 *
	 * @param targetEObj
	 * @param status
	 */
	public void setStatus(EObject targetEObj, CopyStatus status) {
		statusMap.put(targetEObj, status);
	}

	/**
	 * return true, if a shallow copy of the passed EObject exists
	 *
	 * @param targetEObj a target EObject
	 * @return the copy status of the object
	 */
	public CopyStatus getStatus(EObject targetEObj) {
		if (targetEObj != null) {
			CopyStatus status = statusMap.get(targetEObj);
			if (status != null) {
				return status;
			}
		}
		return CopyStatus.UNKNOWN;
	}

	@SuppressWarnings("unchecked")
	public EList<EObject> getRefs(EReference eReference, EObject eObject) {
		EList<EObject> refs = new BasicEList<EObject>();
		if (eObject.eIsSet(eReference)) {
			if (eReference.isMany()) {
				// @SuppressWarnings("unchecked")
				refs.addAll((List<EObject>) eObject.eGet(eReference));
			} else {
				refs.add((EObject) eObject.eGet(eReference));
			}
		}
		return refs;
	}


	public boolean copyResource(EObject sourceEObj) {
		if (copyExtResources == CopyExtResources.ALL) {
			return true;
		}
		else if (copyExtResources == CopyExtResources.ALL_EXCEPT){
			return !selectedResourceList.contains(sourceEObj.eResource());
		}
		else {
			// SELECTED or NONE
			return selectedResourceList.contains(sourceEObj.eResource());
		}
	}

	/**
	 * Returns a copy of the given eObject.
	 *
	 * Normally, we do not want to copy elements that are from a different
	 * resource. There are two exceptions (1) if this is explicitly specified
	 * (for producing "complete" models) (2) if we want to copy elements from a
	 * template into the target model.
	 *
	 * @param sourceEObj
	 *            the object to copy.
	 * @return the copy.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public EObject copy(EObject sourceEObj) {
		if (sourceEObj == null) {
			// this case may happen, if elements are systematically copied without checking for
			// null references in the application code (e.g. if we copy a part-with-port which might
			// be null in case of delegation or connectors without ports
			return null;
		}
		EObject targetEObj = get(sourceEObj);

		CopyStatus status = getStatus(targetEObj);

		if (status == CopyStatus.FULL || status == CopyStatus.INPROGRESS) {
			// copy already exists, return targetEObj
			return targetEObj;
		}
		if (status == CopyStatus.SHALLOW) {
			if (sourceEObj instanceof Package) {
				// a shallow copy of a package is not transformed into a full copy
				return targetEObj;
			}
		}

		if (!copyResource(sourceEObj)) {
			// do not copy if within different resource, unless
			// 1. copyExtResources
			// 2. within package template
			return sourceEObj;
		}
	
		if (sourceEObj instanceof Stereotype || sourceEObj instanceof EPackage) {
			// do not copy Stereotypes, as it would imply copying meta-model elements (the base_X
			// attribute of the stereotype is typed with a meta-model element)
			// do not copy EPackage as well (definition of a profile application)
			return sourceEObj;
		}

		for (PreCopyListener listener : preCopyListeners) {
			EObject result = listener.preCopyEObject(this, sourceEObj);
			if (result != sourceEObj) {
				if (result == USE_SOURCE_OBJECT) {
					return sourceEObj;
				}
				return result;
			}
		}

		if (sourceEObj instanceof NamedElement) {
			String name = ((NamedElement) sourceEObj).getQualifiedName();
			if ((name != null) && name.startsWith("uml::")) { //$NON-NLS-1$
				Activator.log.info("copy for meta-model element \"" + name + //$NON-NLS-1$
						"\" requested. Return original element"); //$NON-NLS-1$
				return sourceEObj;
			}
		}

		// additional sanity check: want to avoid copying (instead of instantiating) elements
		// of a package template
		if (sourceEObj instanceof Package) {
			if (((Package) sourceEObj).getOwnedTemplateSignature() != null) {
				Activator.log.info("warning: copying a package template without instantiating a template"); //$NON-NLS-1$
			}
		}

		if (status == CopyStatus.SHALLOW) {
			// copy exists, but was a shallow copy, change status to INPROGRESS
			setStatus(targetEObj, CopyStatus.INPROGRESS);
		}
		else {
			targetEObj = createCopy(sourceEObj);
			put(sourceEObj, targetEObj);
			setStatus(targetEObj, CopyStatus.INPROGRESS);
			if (copyID) {
				CopyUtils.copyID(sourceEObj, targetEObj);
			}
			// creates a shallow copy of the container. This container will update containment references (such as packagedElement)
			// and thus update links
			if (sourceEObj.eContainer() != null) {
				shallowCopy(sourceEObj.eContainer());
			}
		}
		EClass eClass = sourceEObj.eClass();
		for (int i = 0, size = eClass.getFeatureCount(); i < size; ++i)
		{
			EStructuralFeature eStructuralFeature = eClass.getEStructuralFeature(i);
			if (eStructuralFeature.isChangeable() && !eStructuralFeature.isDerived())
			{
				if (eStructuralFeature instanceof EAttribute) {
					copyAttribute((EAttribute) eStructuralFeature, sourceEObj, targetEObj);
				}
				else {
					EReference eReference = (EReference) eStructuralFeature;
					if (eReference.isContainment()) {
						copyContainment(eReference, sourceEObj, targetEObj);
					}
					// some containment relationships require copying the container completely
					// e.g. if an owned template signature is referenced, we need to follow the "template"
					// reference, which subsets the "owner" relationship.
					// e.g. if an operation is referenced, we need to copy the whole interface
					// Currently: only the standard owning reference is not copied recursively.

					else if (!eReference.getName().equals("owner") && //$NON-NLS-1$
							(!eReference.getName().equals("owningInstance"))) { //$NON-NLS-1$
						Object feature = sourceEObj.eGet(eStructuralFeature);
						if (feature instanceof Element) {
							copy((Element) feature);
						} else if (feature instanceof EList) {
							copyAll((EList<Object>) feature);
						}
						copyReference(eReference, sourceEObj, targetEObj);
					}
				}
			}
			else if ((eStructuralFeature instanceof EReference)) {
				if (eStructuralFeature.getName().equals("clientDependency")) { //$NON-NLS-1$
					Object feature = sourceEObj.eGet(eStructuralFeature);

					if (feature instanceof Element) {
						copy((Element) feature);
					} else if (feature instanceof EList) {
						copyAll((EList<Object>) feature);
					}
				}
			}
		}
		copyProxyURI(sourceEObj, targetEObj);
		if (copyID) {
			CopyUtils.copyID(sourceEObj, targetEObj);
		}
		copyStereotypes(sourceEObj);
		setStatus(targetEObj, CopyStatus.FULL);

		for (PostCopyListener listener : postCopyListeners) {
			listener.postCopyEObject(this, targetEObj);
		}

		return targetEObj;
	}

	/**
	 * @param sourceEObj
	 * @return a copy, if it already exists. If it does not exist, return the
	 *         source object itself, if no copy is required, otherwise return null.
	 */
	public EObject noCopy(EObject sourceEObj) {
		if (!copyResource(sourceEObj)) {
			return sourceEObj;
		}
		else {
			// check copy filters
			for (PreCopyListener listener : preCopyListeners) {
				EObject result = listener.preCopyEObject(this, sourceEObj);
				if (result != sourceEObj) {
					if (result == USE_SOURCE_OBJECT) {
						return sourceEObj;
					}
					return result;
				}
			}
			return get(sourceEObj);
		}
	}

	/**
	 * Copy stereotype applications. Since stereotype applications are not part of the containment of an eObject, they are not copied by the
	 * generic function.
	 * All attributes are fully copied. This could imply that unwanted elements are copied as well, but this could not be avoided. If
	 * necessary, use specific copy filters
	 *
	 * @param sourceEObj the source object whose stereotypes should copied
	 */
	public void copyStereotypes(EObject sourceEObj) {
		if (sourceEObj instanceof Element) {

			for (EObject stereoApplication : ((Element) sourceEObj).getStereotypeApplications()) {
				EObject copiedStereoApplication = copy(stereoApplication);
				EObject targetEObj = get(sourceEObj);

				if (copiedStereoApplication != null && targetEObj != null) {
					// add copied stereotype application to the resource (as top-level objects).
					if (targetEObj.eResource() != null) {
						if (!targetEObj.eResource().getContents().contains(copiedStereoApplication)) {
							targetEObj.eResource().getContents().add(copiedStereoApplication);
						}
					}
				}
			}
		}
	}

	/**
	 * Copy the containment of an element with respect to a certain reference
	 *
	 * @see org.eclipse.emf.ecore.util.EcoreUtil.Copier#copyContainment(org.eclipse.emf.ecore.EReference, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 *      Differences to referenced function in ECoreUtil
	 *      - If an element in copyAll is null, it is not added
	 *      - List elements are always cleared before copying, since the list elements may already have been
	 *      partially filled by a previous shallow copy
	 *
	 * @param eReference
	 *            a reference, such as for instance packagedElement (the
	 *            caller needs to check, is this reference is a containment reference).
	 * @param eObject
	 *            the source eObject
	 * @param copyEObject
	 *            the copy of this eObject
	 */
	@Override
	protected void copyContainment(EReference eReference, EObject eObject, EObject copyEObject) {
		if (eObject.eIsSet(eReference)) {
			if (eReference.isMany()) {
				@SuppressWarnings("unchecked")
				List<EObject> source = (List<EObject>) eObject.eGet(eReference);
				@SuppressWarnings("unchecked")
				List<EObject> target = (List<EObject>) copyEObject.eGet(getTarget(eReference));
				// do not clear target element (would remove elements that are added by copy listeners)
				// But: better enforce exact copy? (listeners could only add in a post-copy step)
				// target.clear();
				if (!source.isEmpty()) {
					for (EObject copyEObj : copyAll(source)) {
						if (copyEObj != null) {
							target.add(copyEObj);
						}
					}
				}
			} else {
				EObject childEObject = (EObject) eObject.eGet(eReference);
				copyEObject.eSet(getTarget(eReference), childEObject == null ? null : copy(childEObject));
			}
		}
	}

	/**
	 * Copy the containment in a "shallow" way, i.e. copy references to contained objects, if these exist already.
	 * If called for instance for a package, it will add those elements to the packagedElements list of the
	 * target package, that have already been copied.
	 *
	 * @param eReference
	 * @param eObject
	 * @param copyEObject
	 */
	protected void shallowCopyContainment(EReference eReference, EObject eObject, EObject copyEObject) {
		if (eObject.eIsSet(eReference)) {
			if (eReference.isMany()) {
				@SuppressWarnings("unchecked")
				List<EObject> source = (List<EObject>) eObject.eGet(eReference);
				@SuppressWarnings("unchecked")
				List<EObject> target = (List<EObject>) copyEObject.eGet(getTarget(eReference));
				if (source.isEmpty()) {
					target.clear();
				} else {
					for (EObject sourceEObj : source) {
						// if eObject has already been copied, add it to target
						// don't add, if copyEObj is identical to sourceEObj. This would imply manipulating an
						// element of the source model.
						EObject copyEObj = noCopy(sourceEObj);
						if ((copyEObj != null) && (copyEObj != sourceEObj) && (!target.contains(copyEObj))) {
							try  {
								target.add(copyEObj);
							}
							catch (Exception e) {
								Activator.log.error(e);
							}
						}
					}
				}
			} else {
				EObject childEObject = (EObject) eObject.eGet(eReference);
				// get will return null, if object should not be copied. In this case, we do not want to replace
				copyEObject.eSet(getTarget(eReference), childEObject == null ? null : noCopy(childEObject));
			}
		}
	}

	/**
	 * Make a shallow copy of an element, i.e. only create the element itself and not
	 * all of its contents. If a subset of the containing elements already exist in the copied
	 * model, update the containment references pointing to these. The function may be called
	 * multiple times in order to add elements to the containment references that
	 * have been copied since the previous call (i.e. it is possible to make a shallow copy
	 * of a package after a single class within it has been copied. It may be called again,
	 * once a second class within the package has been copied => the packagedElements reference
	 * of the package will be updated).
	 *
	 * It is important that the implementation of this object does not make recursive calls.
	 * In particular, stereotypes are based on shallow copy as well. This means that stereotype
	 * attributes that reference other model elements will only be initialized if these elements
	 * exist already.
	 *
	 * @param sourceEObj
	 * @return the shallow copy
	 */
	public EObject shallowCopy(EObject sourceEObj) {
		boolean first = false;
		EObject targetEObj = get(sourceEObj);
		for (PreCopyListener listener : preCopyListeners) {
			EObject result = listener.preCopyEObject(this, sourceEObj);
			if (result != sourceEObj) {
				if (result == USE_SOURCE_OBJECT) {
					return sourceEObj;
				}
				return result;
			}
		}

		if (targetEObj == null) {
			targetEObj = createCopy(sourceEObj);
			put(sourceEObj, targetEObj);
			setStatus(targetEObj, CopyStatus.SHALLOW);
			if (copyID) {
				CopyUtils.copyID(sourceEObj, targetEObj);
			}
			first = true;
			if (sourceEObj.eContainer() != null) {
				shallowCopy(sourceEObj.eContainer());
			}
			else {
				// reach top level element, but not the source pkg.
				if (sourceEObj instanceof Package && sourceEObj != source) {
					Package targetPkg = (Package) targetEObj;

					// if we copy external resources, we might reach the "top" on the source level
					// which becomes a new top-level element that is added to a new resource (managed by the
					// model management instance) below.
					ModelManagement mm = new ModelManagement(targetPkg);
					// set a preliminary URI that can be exploited by transformations or
					// for debugging purposes (may be set again later)
					mm.setURI(ModelManagement.getTempURI(sourceEObj)); 
					rootPkgs.add(mm);
				}
			}
		}
		else if (getStatus(targetEObj) == CopyStatus.FULL) {
			// object has already been completely copied. Nothing to do, return targetEObj.
			// Note that this implies that the update of references below is called for full copies
			// in progress and shallow copies. The former assures that all copied elements have an
			// eContainer during the call of pre-copy listeners (example: if a class is copied, its
			// operations are recursively copied, the ownedOperation relationship is only updated
			// *afterwards* by the code within the (full) copy operation).
			return targetEObj;
		}

		EClass eClass = sourceEObj.eClass();

		for (int i = 0, size = eClass.getFeatureCount(); i < size; ++i) {
			EStructuralFeature eStructuralFeature = eClass.getEStructuralFeature(i);
			if (eStructuralFeature.isChangeable() && !eStructuralFeature.isDerived()) {
				if (eStructuralFeature instanceof EAttribute) {
					// copy all attributes during first pass after creation of target object
					if (first) {
						copyAttribute((EAttribute) eStructuralFeature, sourceEObj, targetEObj);
					}
				} else {
					EReference eReference = (EReference) eStructuralFeature;
					// create a shallow copy of the containment: update only references already in the copy map
					if (sourceEObj != targetEObj) {
						// three cases: shallowCopy (copy, if exists, (2) reference original and (3) create copy
						if (eReference.getName().equals("profileApplication") || //$NON-NLS-1$
							eReference.getName().equals("packageMerge") //$NON-NLS-1$
									) {
							// make a complete copy of the profileApplication
							copyContainment(eReference, sourceEObj, targetEObj);
						}
						else {
							shallowCopyContainment(eReference, sourceEObj, targetEObj);
						}
					}
				}
			}
		}

		copyStereotypes(sourceEObj);

		return targetEObj;
	}

	@SuppressWarnings("unchecked")
	public <T extends Element> T getCopy(T source) {
		return (T) copy(source);
	}

	public EList<PreCopyListener> preCopyListeners;

	public EList<PostCopyListener> postCopyListeners;

	/**
	 * Called to handle the copying of a cross reference;
	 * this adds values or sets a single value as appropriate for the multiplicity
	 * while omitting any bidirectional reference that isn't in the copy map.
	 *
	 * @param eReference
	 *            the reference to copy.
	 * @param eObject
	 *            the object from which to copy.
	 * @param copyEObject
	 *            the object to copy to.
	 */
	@Override
	protected void copyReference(EReference eReference, EObject eObject, EObject copyEObject)
	{
		if (eObject.eIsSet(eReference)) {
			if (eReference.isMany()) {
				@SuppressWarnings("unchecked")
				InternalEList<EObject> source = (InternalEList<EObject>) eObject.eGet(eReference);
				@SuppressWarnings("unchecked")
				InternalEList<EObject> target = (InternalEList<EObject>) copyEObject.eGet(getTarget(eReference));
				if (source.isEmpty()) {
					target.clear();
				}
				else {
					boolean isBidirectional = eReference.getEOpposite() != null;
					int index = 0;
					for (Iterator<EObject> k = resolveProxies ? source.iterator() : source.basicIterator(); k.hasNext();) {
						EObject referencedEObject = k.next();
						EObject copyReferencedEObject = get(referencedEObject);
						// check filters (modification compared to method in superclass)
						boolean noCopy = false;
						for (PreCopyListener listener : preCopyListeners) {
							EObject result = listener.preCopyEObject(this, referencedEObject);
							if (result != referencedEObject) {
								if (result == USE_SOURCE_OBJECT) {
									copyReferencedEObject = referencedEObject;
								}
								else {
									copyReferencedEObject = result;
								}
								noCopy = (result == null);
								break;
							}
						}
						if (noCopy) {
							continue;
						}
						if (copyReferencedEObject == null) {
							if (useOriginalReferences && !isBidirectional) {
								target.addUnique(index, referencedEObject);
								++index;
							}
						}
						else {
							if (isBidirectional) {
								int position = target.indexOf(copyReferencedEObject);
								if (position == -1) {
									target.addUnique(index, copyReferencedEObject);
								}
								else if (index != position) {
									target.move(index, copyReferencedEObject);
								}
							}
							else if (!target.contains(copyReferencedEObject)) {
								// TODO: does not allow multiple identical elements in the list. Problematic?
								// Check above is necessary, since some references that are not
								// part of the containment may have already been copied (e.g. in case of
								// a TemplateSignature "ownedParameter" subsets "parameter", thus copying
								// ownedParameter as part of the containment adds a template parameter)
								target.addUnique(index, copyReferencedEObject);
							}
							++index;
						}
					}
				}
			}
			else {
				Object referencedEObject = eObject.eGet(eReference, resolveProxies);
				if (referencedEObject == null) {
					copyEObject.eSet(getTarget(eReference), null);
				}
				else if (referencedEObject instanceof EObject) {
					// difference to original code in EcoreUtil: we obtain a copy (which might be null or the
					// source object) of the referenced EObject. This assures that we only set a value of a
					// reference to something we actually want to have in the target model.
					// Specific problematic case in original code: classifierBehavior is a reference, but assigning such
					// a behavior will also add an owned behavior. If we assign a referencedEObject (a behavior) from the
					// source model in the target, we will actually remove it from the source model (as it is uniquely owned).
					EObject copyReferencedEObject = copy((EObject) referencedEObject);
					if (copyReferencedEObject != null) {
						copyEObject.eSet(getTarget(eReference), copyReferencedEObject);
					}
				}
			}
		}
	}

	/**
	 * Copy all methods from the passed source-model class.
	 * This function is useful, if the passed class only exist
	 * as a shallow copy.
	 *
	 * @param source
	 *            A class within the source model
	 */
	public void copyMethods(Class source) {
		for (Behavior method : source.getOwnedBehaviors()) {
			getCopy(method);
		}
	}

	/**
	 * Copy all attributes from the source-model classifier
	 * This function is useful, if the passed class only exist
	 * as a shallow copy.
	 *
	 * @param source
	 *            A classifier within the source model
	 */
	public void copyAttributes(Classifier source) {
		for (Property attribute : source.getAttributes()) {
			getCopy(attribute);
		}
	}

	/**
	 * copy all operations from the source-model classifier.
	 * This function is useful, if the passed class only exist
	 * as a shallow copy.
	 *
	 * @param source
	 *            A classifier within the source model
	 */
	public void copyOperations(Classifier source) {
		for (Operation operation : source.getOperations()) {
			getCopy(operation);
		}
	}
}
