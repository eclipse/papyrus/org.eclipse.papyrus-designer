/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.templates;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PreCopyListener;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.papyrus.designer.transformation.extensions.IM2MTrafo;
import org.eclipse.papyrus.designer.transformation.extensions.M2MTrafoExt;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Evaluate ApplyTransformation stereotype within in the context
 * of template instantiation
 */

public class PreTemplateInstantiationListener implements PreCopyListener {

	public static PreTemplateInstantiationListener getInstance() {
		if (preTemplateInstantiationListener == null) {
			preTemplateInstantiationListener = new PreTemplateInstantiationListener();
			preTemplateInstantiationListener.treatTemplate = false;
		}
		return preTemplateInstantiationListener;
	}

	public void init() {
		preTemplateInstantiationListener.treatTemplate = false;
	}

	private boolean treatTemplate;

	private static PreTemplateInstantiationListener preTemplateInstantiationListener;

	@Override
	public EObject preCopyEObject(LazyCopier copier, EObject sourceEObj) {
		if (treatTemplate) {
			return sourceEObj;
		}
		treatTemplate = true;
		EObject targetEObj = checkEObject(copier, sourceEObj);
		treatTemplate = false;
		return targetEObj;
	}

	/**
	 * Check whether to apply an additional M2M transformation on one of the contained
	 * elements.
	 * @param copier
	 * @param sourceEObj
	 * @return
	 */
	protected EObject checkEObject(LazyCopier copier, EObject sourceEObj) {

		if (sourceEObj instanceof Element) {
			ApplyTransformation applyTrafo =
					UMLUtil.getStereotypeApplication((Element) sourceEObj, ApplyTransformation.class);
			if (applyTrafo != null) {
				for (M2MTrafo trafo : applyTrafo.getTrafo()) {
					IM2MTrafo ihelper = M2MTrafoExt.getM2MTrafo(trafo.getBase_Class().getQualifiedName());
					if (ihelper instanceof PreCopyListener) {
						return ((PreCopyListener) ihelper).preCopyEObject(copier, sourceEObj);
					}
				}
			}
		}
		return sourceEObj;
	}
}
