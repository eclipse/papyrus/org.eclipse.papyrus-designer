/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

public class AttributeLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof Property) {
			Property p = (Property) element;
			Type type = p.getType();
			return String.format("%s.%s%s", p.getClass_().getName(), p.getName(), //$NON-NLS-1$
					((type != null) ? " : " + type.getQualifiedName() : StringConstants.EMPTY)); //$NON-NLS-1$
		}
		else {
			return "invalid"; //$NON-NLS-1$
		}
	}
};
