/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

public class InstanceLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof NamedElement) {
			String instanceName = ((NamedElement) element).getName();
			Element owner = ((Element) element).getOwner();
			if (owner instanceof NamedElement) {
				return String.format("Plan %s: %s", ((NamedElement) owner).getName(), instanceName); //$NON-NLS-1$
			}
			else {
				return instanceName;
			}
		}
		else {
			return "not a named element"; //$NON-NLS-1$
		}
	}
};
