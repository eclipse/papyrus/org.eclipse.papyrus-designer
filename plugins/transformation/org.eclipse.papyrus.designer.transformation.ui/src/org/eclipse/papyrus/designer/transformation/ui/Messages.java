/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.transformation.ui.messages"; //$NON-NLS-1$

	public static String AllocateHandler_ALLOCATE_INSTANCES;
	public static String AllocateHandler_INSTANCE_ALLOCATION;
	public static String AllocateHandler_NO_INSTANCES;

	public static String AllocationDialog_ALLOCATE_TO_NODE;
	public static String AllocationDialog_EXPLICIT_ALLOCATION;
	public static String AllocationDialog_IMPLICIT_ALLOCATION;
	public static String AllocationDialog_INSTANCE;
	public static String AllocationDialog_NO_EXPLICIT_ALLOCATION;
	public static String AllocationDialog_SELECTED_INSTANCE;
	public static String AllocationDialog_ST_APPLICATION_FAILED;

	public static String ConfigureInstanceDialog_ASSOCIATED_INSTANCES;
	public static String ConfigureInstanceDialog_AVAILABLE_PROPERTIES;
	public static String ConfigureInstanceDialog_CONFIGURATION;
	public static String ConfigureInstanceDialog_DESCRIPTION;
	public static String ConfigureInstanceDialog_INSTANCE_LIST_EMPTY;
	public static String ConfigureInstanceDialog_NO_DEP_PLANS;
	public static String ConfigureInstanceDialog_NO_INSTANCES;
	public static String ConfigureInstanceDialog_NOT_AVAILABLE;

	public static String ConfigurePortHandler_ConfigurePorts;
	public static String ConfigurePortHandler_ConfigurePortsOfComponent;
	public static String ConfigurePortHandler_ConfigureInstance;

	public static String ConfigurePortDialog_AvailPorts;
	public static String ConfigurePortDialog_PortConfig;
	public static String ConfigurePortDialog_NotAvail;
	public static String ConfigurePortDialog_Kind;
	public static String ConfigurePortDialog_None;
	public static String ConfigurePortDialog_Type;
	public static String ConfigurePortDialog_ChangeType;
	public static String ConfigurePortDialog_IntfDerived;
	public static String ConfigurePortDialog_Provided;
	public static String ConfigurePortDialog_Required;
	public static String ConfigurePortDialog_Undef;

	public static String InteractiveImplementationChooser_MULTIPLE_IMPLEMENTATIONS;
	public static String InteractiveImplementationChooser_SELECT_AN_IMPLEMENTATION;

	public static String CreateDepPlanHandler_CreateDPs;
	public static String CreateDepPlanHandler_Sync;
	public static String CreateDepPlanHandler_CreateNew;
	public static String CreateDepPlanHandler_WhatShouldIDo;
	public static String CreateDepPlanHandler_DPwithNameExistsAlready;
	public static String CreateDepPlanHandler_DPwithNameExistsNoPackage;
	public static String CreateDepPlanHandler_CannotSync;
	public static String CreateDepPlanHandler_Cancel;
	public static String CreateDepPlanHandler_CreateDP;
	public static String CreateDepPlanHandler_CannotCreateDP;
	public static String CreateDepPlanHandler_StereoApplicationFailed;

	public static String ExecuteTransformationChainHandler_EXECUTE_CHAIN;

	public static String PapyrusDesignerPreferencePage_ALL_CONFIG_ATTRIBUTES;
	public static String PapyrusDesignerPreferencePage_GENERATION_PREFIX;
	public static String PapyrusDesignerPreferencePage_NONE_AS_COMPOSITE;
	public static String PapyrusDesignerPreferencePage_SHOW_ICONS;

	public static String SyncHandler_CANNOT_SYNCHRONIZE;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
