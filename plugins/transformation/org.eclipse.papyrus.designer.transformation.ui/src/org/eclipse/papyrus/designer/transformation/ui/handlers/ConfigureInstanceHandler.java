/********
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.RunnableWithResult;
import org.eclipse.papyrus.designer.transformation.ui.dialogs.ConfigureInstanceDialog;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Feature;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Implementation class for ClassAction action
 */
public class ConfigureInstanceHandler extends CmdHandler {
	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		if (!(selectedEObject instanceof NamedElement)) {
			return null;
		}

		final NamedElement element = (NamedElement) selectedEObject;
		final Shell shell = Display.getCurrent().getActiveShell();
		final ExecutionEvent from = event;

		// 1. select possible connectors according to port types
		// (only show compatible connectors check-box?)
		// 2. select implementation group according to connector type

		if (element instanceof Class) {
			// container dialog: either extension, rule or interceptor
			// how-to select? which? (and how-to add/remove?) - is standard dialog
			// sufficient?
			CommandSupport.exec(element, "Configure associated component instance", new RunnableWithResult() { //$NON-NLS-1$

				@Override
				public IStatus run() {
					ConfigureInstanceDialog configureInstanceDialog = new ConfigureInstanceDialog(shell);
					if (configureInstanceDialog.init((Class) element, from)) {
						configureInstanceDialog.setTitle("Configure instance"); //$NON-NLS-1$
						configureInstanceDialog.setMessage("Configure instance for component " + element.getName()); //$NON-NLS-1$
						configureInstanceDialog.open();
						if (configureInstanceDialog.getReturnCode() == IDialogConstants.OK_ID) {
							return Status.OK_STATUS;
						}
					}
					return Status.CANCEL_STATUS;
				}
			});
		} else if (element instanceof Feature) {
			CommandSupport.exec(element, "Configure associated instance", new RunnableWithResult() { //$NON-NLS-1$

				@Override
				public IStatus run() {
					ConfigureInstanceDialog configureInstanceDialog = new ConfigureInstanceDialog(shell);
					if (configureInstanceDialog.init((Feature) element, from)) {
						configureInstanceDialog.setTitle("Configure instance"); //$NON-NLS-1$
						configureInstanceDialog
								.setMessage("Configure instance for property/connector " + element.getName()); //$NON-NLS-1$
						configureInstanceDialog.open();
						if (configureInstanceDialog.getReturnCode() == IDialogConstants.OK_ID) {
							return Status.OK_STATUS;
						}
					}
					return Status.CANCEL_STATUS;
				}
			});

		} else if (element instanceof InstanceSpecification) {

			CommandSupport.exec(element, "Configure instance", new RunnableWithResult() { //$NON-NLS-1$

				@Override
				public IStatus run() {
					ConfigureInstanceDialog configureInstanceDialog = new ConfigureInstanceDialog(shell);
					if (configureInstanceDialog.init((InstanceSpecification) element, from)) {
						configureInstanceDialog.setMessage("Configure instance " + element.getName()); //$NON-NLS-1$
						configureInstanceDialog.open();
						if (configureInstanceDialog.getReturnCode() == IDialogConstants.OK_ID) {
							return Status.OK_STATUS;
						}
					}
					return Status.CANCEL_STATUS;
				}
			});
		}
		return null;
	}
}
