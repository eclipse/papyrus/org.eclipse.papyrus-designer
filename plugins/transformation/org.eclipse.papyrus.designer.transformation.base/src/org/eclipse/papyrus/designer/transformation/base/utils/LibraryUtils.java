/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.base.utils;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.UMLFactory;

public class LibraryUtils {
	
	/**
	 * Return the first top-level element of a UML resource (typically a model)
	 * @param resource
	 * @return the first top-level element of a resource
	 */
	public static Element getContent(Resource resource) {
		EList<EObject> contentObj = resource.getContents();
		if ((contentObj.size() > 0) && (contentObj.get(0) instanceof Element)) {
			return (Element) contentObj.get(0);
		}
		return null;
	}
	
	/**
	 * Return the first top-level element of a UML resource (typically a model)
	 * with a given URI (and loaded into a specified resource set)
	 * 
	 * @param uri the URI of a resource
	 * @param rs a resource set
	 * @return the first top-level element of a resource
	 */
	public static Element getContent(URI uri, ResourceSet rs) {
		Resource resource = rs.getResource(uri, true);
		return getContent(resource);
	}
	
	/**
	 * Retrieve a model library from the repository and create a package import
	 *
	 * @param uri
	 *            the URI of the repository
	 *
	 * @return the created package import
	 */
	public static PackageImport getModelLibraryImportFromURI(URI uri, ResourceSet resourceSet) {
		// Try to reach model
		Element root = getContent(uri, resourceSet);
		if (root instanceof Package) {
			// Import model library
			Package libToImport = (Package) root;
			// create import package
			PackageImport modelLibImport = UMLFactory.eINSTANCE.createPackageImport();
			modelLibImport.setImportedPackage(libToImport);

			return modelLibImport;
		}
		return null;
	}
}
