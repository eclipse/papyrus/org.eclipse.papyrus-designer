/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.tracing.library;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.moka.tracepoint.service.ITraceMechanism;
import org.eclipse.papyrus.moka.tracepoint.service.MarkerUtils;
import org.eclipse.papyrus.moka.tracepoint.service.TracepointConstants;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Abstract super class for M2M transformation based trace transformations
 */
abstract public class DesignerTraceMechanism implements ITraceMechanism {
	
	/**
	 * Apply the trace mechanism, i.e. set or unset the appropriate M2MTrafo for tracing.
	 *
	 * TODO: transformation chain is currently not applied automatically (nothing is done)
	 */
	@Override
	public boolean applyTraceMechanism(EObject eObj, String id, int traceOption) {
		return true;
	}

	/**
	 * TODO: function does currently nothing
	 *
	 * @see org.eclipse.papyrus.moka.tracepoint.service.ITraceMechanism#configureTraceMechanisms()
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean configureTraceMechanisms() {
		@SuppressWarnings("unused")
		String config = ""; //$NON-NLS-1$
		try {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

			if (root != null) {
				Object tracePoints[] = root.findMarkers(TracepointConstants.tracepointMarker, true, IResource.DEPTH_INFINITE);
				for (Object tracePointObj : tracePoints) {
					if (tracePointObj instanceof IMarker) {
						IMarker tracePoint = (IMarker) tracePointObj;
						EObject eobj = MarkerUtils.getEObjectOfMarker(tracePoint);
						if (MarkerUtils.isActive(tracePoint)) {
							if (eobj instanceof NamedElement) {
								config += ((NamedElement) eobj).getQualifiedName();
							}
						}
					}
				}
			}
		} catch (CoreException e) {
			Activator.log.error(e);
		}
		return true;
	}
}
