/*****************************************************************************
 * Copyright (c) 2016, 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.library.utils;

import org.eclipse.emf.common.util.URI;

/**
 * URI constants to access the tracing library
 */
public class TracingUriConstants {
	public static final String PATHMAP = "pathmap://DML_TRACE/"; //$NON-NLS-1$
	
	public static final String TRACING_LIB_PATH = PATHMAP + "tracing.uml"; //$NON-NLS-1$

	public static final URI TRACING_LIB_URI = URI.createURI(TRACING_LIB_PATH);
}
