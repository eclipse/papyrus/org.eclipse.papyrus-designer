/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.library

import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.profile.standard.Destroy
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import java.util.Map
import java.util.HashMap

/**
 * The names of several trace events. Independent of a concrete trace implementation
 */
class EventNames {

	static Map<Element, Integer> idMap
	
	static int counter
	
	def static init() {
		idMap = new HashMap<Element, Integer>()
		counter = 0
	}

	/**
	 * The name of an operation event start
	 * If the port name is non null, it is taken into account
	 */
	def static operationStartsEventName(Operation operation, Port port) {
		if (port !== null)
			'''port_op_starts_«operation.class_.name»_«port.name»_«operation.name»_«operation.id»'''
		else if (StereotypeUtil.isApplied(operation, Create))
			'''creation_starts_«operation.class_.name»_«operation.id»'''
		else if (StereotypeUtil.isApplied(operation, Destroy))
			'''destruction_starts_«operation.class_.name»_«operation.id»'''
		else
			'''op_starts_«operation.class_.name»_«operation.name»_«operation.id»'''
	}

	/**
	 * The name of an operation event start
	 * If the port name is non null, it is taken into account
	 */
	def static operationEndsEventName(Operation operation, Port port) {
		if (port !== null)
			'''port_op_ends_«operation.class_.name»_«port.name»_«operation.name»_«operation.id»'''
		else if (StereotypeUtil.isApplied(operation, Create))
			'''creation_ends_«operation.class_.name»_«operation.id»'''
		else if (StereotypeUtil.isApplied(operation, Destroy))
			'''destruction_ends_«operation.class_.name»_«operation.id»'''
		else
			'''op_ends_«operation.class_.name»_«operation.name»_«operation.id»'''
	}

	/**
	 * The name of a state enter event
	 */
	def static enterStateEventName(State state)
		'''enter_state_«state.containingStateMachine.context.name»_«state.name»_«state.id»'''

	/**
	 * The name of a state exit/leave event
	 */
	def static exitStateEventName(State state)
		'''exit_state_«state.containingStateMachine.context.name»_«state.name»_«state.id»'''

	/**
	 * The name of a transition event
	 * if the transition has no name, use source & target instead
	 */
	def static transitionEventName(Transition t)
		'''transition_«t.containingStateMachine.context.name»_«t.transitionName»_«t.id»'''

	/**
	 * Helper function for transitionEventName - return either transition name or
	 * a concatenation of source and target name
	 */
	protected def static transitionName(Transition t) {
		if (t.name !== null && t.name.length > 0) {
			return t.name
		}
		else {
			return '''«t.source.name»_«t.target.name»'''
		}
	}

	protected def static getId(Element element) {
		var id = idMap.get(element)
		if (id === null) {
			idMap.put(element, counter)
			id = counter++
		}
		return String.format("%05d", id)
	}
}
