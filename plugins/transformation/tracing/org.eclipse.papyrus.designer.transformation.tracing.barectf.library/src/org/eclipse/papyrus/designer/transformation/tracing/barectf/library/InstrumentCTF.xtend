/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library

import java.util.List
import org.eclipse.papyrus.designer.deployment.profile.Deployment.ConfigurationProperty
import org.eclipse.papyrus.designer.deployment.profile.Deployment.InstanceConfigurator
import org.eclipse.papyrus.designer.deployment.profile.Deployment.UseInstanceConfigurator
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.designer.infra.base.StringUtils
import org.eclipse.papyrus.designer.uml.tools.utils.OperationUtils
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.flatten.NameType
import org.eclipse.papyrus.designer.transformation.tracing.library.InstanceNameConfigurator
import org.eclipse.papyrus.designer.transformation.tracing.library.utils.TracingUriConstants
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAOperation
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAState
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.LiteralString
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.profile.standard.Destroy

import static extension org.eclipse.papyrus.designer.transformation.tracing.barectf.library.flatten.Flattener.flattenParams
import static extension org.eclipse.papyrus.designer.transformation.tracing.library.EventNames.*
import static extension org.eclipse.papyrus.designer.transformation.tracing.library.utils.TraceUtils.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.apply
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.applyApp
import static extension org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication
import org.eclipse.papyrus.designer.deployment.profile.DepProfileResource
import org.eclipse.uml2.uml.Profile
import org.eclipse.papyrus.designer.languages.common.base.StdUriConstants
import org.eclipse.uml2.uml.SignalEvent
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TATransition
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TraceFeature
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAClass
import org.eclipse.papyrus.designer.uml.tools.utils.BehaviorUtils

class InstrumentCTF {
	val static prefix = "barectf_default_trace"

	/**
	 * Return a list of interfaces provided by ports (which are being traced)
	 */
	def static instrument(List<Element> events) {
		// prioritize ports (over operations)
		for (event : events.clone) {
			if (event instanceof Port) {
				val port = event as Port
				val clazz = port.class_
				for (intf : port.provideds) {
					for (iOp : intf.ownedOperations) {
						val cOp = OperationUtils.getSameOperation(iOp, clazz)
						if (cOp !== null) {
							// don't instrument as operation, if already instrumented
							// as port 
							cOp.instrumentOperation(port)
							if (events.contains(cOp)) {
								events.remove(cOp)
							}
						}
					}
				}
			}
		}

		for (event : events.clone) {
			if (event instanceof Transition) {
				val transition = event as Transition
				var effect = transition.effect
				if (effect === null) {
					effect = transition.createEffect(null, UMLPackage.eINSTANCE.opaqueBehavior)
				}
				var attrStr = "";
				if (transition.traceTransitionAction(TATransition.TriggerValues)) {
					for (trigger : transition.triggers) {
						if (trigger.event instanceof SignalEvent) {
							val sigEvent = trigger.event as SignalEvent
							val attrList = sigEvent.signal.attributes.flattenParams
							attrStr += '''«FOR attr : attrList», sig.«attr.castAndName»«ENDFOR»'''
						}
					}
				}
				BehaviorUtils.prefixBody(
					effect, '''«prefix»_«transition.transitionEventName»(BareCTFInit::ctx(), instanceName«attrStr»);''')
				val clazz = transition.containingStateMachine.context as Class
				clazz.addDependencies
				clazz.addInstanceName
			}

			if (event instanceof State) {
				val state = event as State
				var trace = false;
				if (state.traceStateAction(TAState.StateEnter)) {
					var entry = state.entry
					if (entry === null) {
						entry = state.createEntry(null, UMLPackage.eINSTANCE.opaqueBehavior)
					}
					BehaviorUtils.prefixBody(
						entry, '''«prefix»_«state.enterStateEventName»(BareCTFInit::ctx(), instanceName);''')
					trace = true
				}
				if (state.traceStateAction(TAState.StateLeave)) {
					var exit = state.exit
					if (exit === null) {
						exit = state.createExit(null, UMLPackage.eINSTANCE.opaqueBehavior)
					}
					BehaviorUtils.prefixBody(
						exit, '''«prefix»_«state.exitStateEventName»(BareCTFInit::ctx(), instanceName);''')
					trace = true
				}
				if (trace) {
					val clazz = state.containingStateMachine.context as Class
					clazz.addDependencies
					clazz.addInstanceName
				}
			}

			if (event instanceof Operation) {
				val operation = event as Operation
				operation.instrumentOperation(null)
			}

			if (event instanceof Class) {
				val clazz = event as Class
				val marker = clazz.getMarkerForTraceElement
				if (marker.isActionActive(TraceFeature.Class, TAClass.Creation.ordinal)) {
					val constructors = clazz.ownedOperations.filter[
						StereotypeUtil.isApplied(it, Create)]
					if (constructors.empty) {
						// create constructor a
						var ctor = clazz.createOwnedOperation(clazz.name, null, null)
						StereotypeUtil.apply(ctor, Create)
						BehaviorUtils.createOpaqueBehavior(clazz, ctor)
						ctor.instrumentOperation(null)
						events.add(ctor)
					}
				}
				if (marker.isActionActive(TraceFeature.Class, TAClass.Destruction.ordinal)) {
					val destructors = clazz.ownedOperations.filter[
						StereotypeUtil.isApplied(it, Destroy)]
					if (destructors.empty) {
						// create constructor a
						var dtor = clazz.createOwnedOperation("~" + clazz.name, null, null)
						StereotypeUtil.apply(dtor, Destroy)
						BehaviorUtils.createOpaqueBehavior(clazz, dtor)
						dtor.instrumentOperation(null)
						events.add(dtor)
					}
				}
			}

		}
	}

	/**
	 * Instrument an operation taking a prepare hint into account
	 */
	def static instrumentOperation(Operation operation, Port port) {
		if (operation.methods.size > 0) {
			var method = operation.methods.get(0)
			val prepare = operation.prepareHint
			BehaviorUtils.prefixBody(method, '''
			«IF prepare !== null»«prepare»«ENDIF»
			«prefix»_«operation.operationStartsEventName(port)»(BareCTFInit::ctx()«IF operation.needInstanceName», instanceName«ENDIF»
				«IF operation.paramsHint !== null»
					, «operation.paramsHint»
				«ELSEIF (operation.traceOpOrPortAction(port, TAOperation.ParameterValues))»«
					val paramList = operation.inAndInout.flattenParams»
					«FOR param : paramList», «param.castAndName»«ENDFOR»
				«ENDIF»);''')
			if (operation.traceOpOrPortAction(port, TAOperation.MethodEnds) && operation.type === null) {
				BehaviorUtils.appendBody(method, '''
					«prefix»_«operation.operationEndsEventName(port)»(BareCTFInit::ctx()«IF operation.needInstanceName», instanceName«ENDIF»);''')
			}
			val clazz = operation.class_
			clazz.addDependencies
			clazz.addInstanceName
		}
	}

	/**
	 * add (usage) dependency to generated classes BareCTFInit and BareCTF,
	 * if not already existent
	 */
	def static addDependencies(Class clazz) {
		var initCl = ElementUtils.getQualifiedElementFromRS(clazz, ModelQNames.BareCTFInit) as Classifier
		clazz.addDependency(initCl)
		var barectf = ElementUtils.getQualifiedElementFromRS(clazz, ModelQNames.BareCTF) as Classifier
		clazz.addDependency(barectf)
	}

	/**
	 * Add an instance name attribute
	 */
	def static addInstanceName(Class clazz) {
		var instAttrib = clazz.getOwnedAttribute(InstanceNameConfigurator.PROP_INSTANCE_NAME, null)
		if (instAttrib === null) {
			var string = ElementUtils.getQualifiedElementFromRS(clazz, "PrimitiveTypes::String") as Classifier
			if (string === null) {
				PackageUtil.loadPackage(StdUriConstants.UML_PRIM_TYPES_URI, clazz.eResource.resourceSet);
				string = ElementUtils.getQualifiedElementFromRS(clazz, "PrimitiveTypes::String") as Classifier
			}
			if (string !== null) {
				instAttrib = clazz.createOwnedAttribute(InstanceNameConfigurator.PROP_INSTANCE_NAME, string)
				instAttrib.apply(ConfigurationProperty)
				val litString = instAttrib.createDefaultValue(null, null,
					UMLPackage.eINSTANCE.literalString) as LiteralString
				// use class name as default name, if setting via bootloader fails
				litString.value = StringUtils.quote(clazz.name);
			}
		}
		var useIConfig = clazz.applyApp(UseInstanceConfigurator)
		if (useIConfig === null) {
			// load and apply deployment profile
			val depProfile = PackageUtil.loadPackage(DepProfileResource.PROFILE_PATH_URI, clazz.eResource.resourceSet);
			if (depProfile instanceof Profile) {
				PackageUtil.getRootPackage(clazz).applyProfile(depProfile as Profile)
				useIConfig = clazz.applyApp(UseInstanceConfigurator)	
			}
			else {
				throw new RuntimeException("Cannot apply deployment profile");
			}
		}
		// load name instance-configurator from tracing
		PackageUtil.loadPackage(TracingUriConstants.TRACING_LIB_URI, clazz.eResource.resourceSet);
		val iconfigCl = ElementUtils.getQualifiedElementFromRS(clazz,
			"tracing::iconfigurators::InstanceNameConfigurator") as Classifier
		val iconfig = iconfigCl.getStereotypeApplication(InstanceConfigurator)
		useIConfig.configurator = iconfig
	}

	/**
	 * add dependency to a passed target class, if not already existent
	 */
	def static addDependency(Class clazz, Classifier target) {
		if (target !== null) {
			for (dependency : clazz.clientDependencies) {
				if (dependency.suppliers.contains(target)) {
					return
				}
			}
			clazz.createDependency(target /* Copy */ )
		}
	}

	/**
	 * Returns the name, in case of an Enumeration prefixed with a type cast.
	 * 
	 * Background: Enumerations must be cast to uint8 in order to be compatible
	 * with CTF enumeration declarations (which are uints with a CTF mapping
	 * declaration)
	 * @param te a typed element
	 */
	def static castAndName(NameType nt) {
		val type = nt.type
		if (type instanceof Enumeration) {
			return '''(uint8_t) «nt.name»'''
		}
		return nt.name
	}
}
