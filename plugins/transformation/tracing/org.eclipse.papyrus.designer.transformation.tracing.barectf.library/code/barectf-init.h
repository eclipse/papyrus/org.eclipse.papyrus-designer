#include "barectf-platform-fs.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize BareCTF
 */
struct barectf_default_ctx* barectf_papy_init(const char *metadataPath, const char *streamName);

/**
 * Terminate BareCTF
 */
void barectf_papy_fini();

#ifdef __cplusplus
}
#endif
