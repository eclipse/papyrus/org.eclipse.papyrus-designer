#ifndef PKG_TRACING
#define PKG_TRACING

/************************************************************
 Pkg_tracing package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace tracing {

// Types defined within the package
}// of namespace tracing

/************************************************************
 End of Pkg_tracing package header
 ************************************************************/

#endif
