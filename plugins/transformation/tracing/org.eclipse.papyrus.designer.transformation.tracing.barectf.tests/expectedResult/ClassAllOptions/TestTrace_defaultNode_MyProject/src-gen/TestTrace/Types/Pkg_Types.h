#ifndef PKG_TESTTRACE_TYPES
#define PKG_TESTTRACE_TYPES

/************************************************************
 Pkg_Types package header
 ************************************************************/

#include "TestTrace/Pkg_TestTrace.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace TestTrace {
namespace Types {

// Types defined within the package

/**
 * 
 */
enum BooleanType {
	/**
	 * 
	 */
	True,
	/**
	 * 
	 */
	False
};

/**
 * 
 */
typedef int Integer;

/**
 * 
 */
typedef double Double;

/**
 * 
 */
typedef std::string Text;

/**
 * 
 */
enum MyEnumeration {
	/**
	 * 
	 */
	Enum_A,
	/**
	 * 
	 */
	Enum_B,
	/**
	 * 
	 */
	Enum_C,
	/**
	 * 
	 */
	Enum_D
};

} // of namespace Types
} // of namespace TestTrace

/************************************************************
 End of Pkg_Types package header
 ************************************************************/

#endif
