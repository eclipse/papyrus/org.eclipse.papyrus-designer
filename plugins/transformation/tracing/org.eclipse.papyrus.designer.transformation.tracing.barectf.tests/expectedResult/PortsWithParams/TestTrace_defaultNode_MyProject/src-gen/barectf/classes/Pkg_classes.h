#ifndef PKG_BARECTF_CLASSES
#define PKG_BARECTF_CLASSES

/************************************************************
 Pkg_classes package header
 ************************************************************/

#include "barectf/Pkg_barectf.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace barectf {
namespace classes {

// Types defined within the package
}// of namespace classes
} // of namespace barectf

/************************************************************
 End of Pkg_classes package header
 ************************************************************/

#endif
