#ifndef PKG_TESTTRACE_INTERFACES
#define PKG_TESTTRACE_INTERFACES

/************************************************************
 Pkg_Interfaces package header
 ************************************************************/

#include "TestTrace/Pkg_TestTrace.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace TestTrace {
namespace Interfaces {

// Types defined within the package

/**
 * 
 */
typedef class I_MyInterface_1 *I_MyInterface_1_ptr;

/**
 * 
 */
typedef class I_MyInterface_2 *I_MyInterface_2_ptr;
} // of namespace Interfaces
} // of namespace TestTrace

/************************************************************
 End of Pkg_Interfaces package header
 ************************************************************/

#endif
