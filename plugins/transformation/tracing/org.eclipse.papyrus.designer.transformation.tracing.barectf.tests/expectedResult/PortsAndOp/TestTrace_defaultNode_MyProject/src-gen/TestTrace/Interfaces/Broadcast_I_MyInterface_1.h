// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef TESTTRACE_INTERFACES_BROADCAST_I_MYINTERFACE_1_H
#define TESTTRACE_INTERFACES_BROADCAST_I_MYINTERFACE_1_H

/************************************************************
 Broadcast_I_MyInterface_1 class header
 ************************************************************/

#include "TestTrace/Interfaces/Pkg_Interfaces.h"

#include "TestTrace/Interfaces/I_MyInterface_1.h"
#include "TestTrace/Types/Pkg_Types.h"
#include "vector"

// derived using directives
using namespace TestTrace::Interfaces;
using namespace TestTrace::Types;

namespace TestTrace {
namespace Interfaces {

/************************************************************/
/**
 * 
 */
class Broadcast_I_MyInterface_1: public I_MyInterface_1 {
public:

	/**
	 * 
	 */
	std::vector<I_MyInterface_1_ptr> references;

	/**
	 * 
	 * @param ref Reference to provided port 
	 */
	void connect_port(I_MyInterface_1 * /*in*/ref);

	/**
	 * 
	 * @param state 
	 */
	void MyOperation_1(BooleanType /*in*/state);

};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace Interfaces
} // of namespace TestTrace

/************************************************************
 End of Broadcast_I_MyInterface_1 class header
 ************************************************************/

#endif
