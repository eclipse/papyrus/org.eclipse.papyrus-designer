/*******************************************************************************
 * Copyright (c) 2021 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.tests;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.papyrus.designer.languages.common.testutils.RecursiveCopy;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.designer.languages.common.testutils.TransformationTestSupport;
import org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants;
import org.eclipse.papyrus.junit.utils.rules.HouseKeeper;
import org.eclipse.papyrus.junit.utils.rules.PapyrusEditorFixture;
import org.eclipse.papyrus.junit.utils.rules.PluginResource;
import org.eclipse.papyrus.moka.tracepoint.service.TracepointConstants;
import org.eclipse.papyrus.moka.ui.tracepoint.view.ImportTracepointsOperation;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Test of the lazy copier
 */
@SuppressWarnings("nls")
@PluginResource("org.eclipse.papyrus.designer.transformation.tracing.barectf.tests:/models/TraceTest/TraceTest.di")
public class TracingTest {

	private static final String MODELS = "models";

	private static final String TPOINTS_EXT = ".tpoints";

	public static final String TEST_TRACE_DEFAULT_NODE = "TestTrace_defaultNode_MyProject";

	public static final String TEST_TRACE_DEP_PLAN = "TestTrace::MyProject";

	@Rule
	/** The model set fixture. */
	public final PapyrusEditorFixture modelSetFixture = new PapyrusEditorFixture();

	@ClassRule
	public static HouseKeeper.Static houseKeeper = new HouseKeeper.Static();

	@Test
	public void testTraceInstrumentation() throws InterruptedException {

		TransformationTestSupport tts = new TransformationTestSupport(this.getClass(), houseKeeper, modelSetFixture);

		// create generated project. For generation purposes, it is not important that this is not a CDT project
		IProject genProject = houseKeeper.createProject(TEST_TRACE_DEFAULT_NODE);

		// copy "models" folder into target workspace
		IProject modelProject = modelSetFixture.getProject().getProject();
		RecursiveCopy copier = new RecursiveCopy(houseKeeper);
		Bundle srcBundle = FrameworkUtil.getBundle(this.getClass());
		// copy expected results folder to model project
		copier.copy(srcBundle, MODELS, modelProject, MODELS);
		
		IFolder baseFolder = modelProject.getFolder(MODELS).getFolder("TraceTest");
		assertThat("Basefolder must exist", baseFolder.exists());

		// use same (non default) settings as for example
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(org.eclipse.papyrus.designer.languages.cpp.codegen.Activator.PLUGIN_ID);
		prefs.putBoolean(CppCodeGenConstants.P_USING_NAMESPACES_KEY, true);
		prefs.putBoolean(CppCodeGenConstants.P_CPP11_CLASS_ENUM_KEY, false);
		prefs.putBoolean(CppCodeGenConstants.P_FORMAT_CODE_KEY, true);

		String tpointFiles[] = {
				"ClassAllOptions",
				"PortsAndOp",
				"PortsWithParams"
		};
		try {
			for (String tpointFile : tpointFiles) {
				IFile file = baseFolder.getFile("TraceTest." + tpointFile + TPOINTS_EXT);
				assertThat("Tracepoint file must exist", file.exists());

				// change path in tracepoint file, as the automated test creates a copy of this plugin
				// using the name of the test method (and placing the model in the root folder).
				var contentStr = new String(file.getContents().readAllBytes(), StandardCharsets.UTF_8);
				contentStr = contentStr.replaceAll(
					Activator.PLUGIN_ID + "/models/TraceTest/",
					"testTraceInstrumentation/");
				var contentStream = new ByteArrayInputStream(contentStr.getBytes());
				file.setContents(contentStream, true, true, null);

				String fileName = FileLocator.resolve(file.getLocationURI().toURL()).getFile();

				ImportTracepointsOperation importAction = new ImportTracepointsOperation(fileName);
				importAction.run(new NullProgressMonitor());

				tts.runTransformation(TEST_TRACE_DEP_PLAN);

				tts.validateResults(genProject, TestConstants.EXPECTED_RESULT + TestConstants.FILE_SEP +
						tpointFile + TestConstants.FILE_SEP +
						TEST_TRACE_DEFAULT_NODE + TestConstants.FILE_SEP + TestConstants.SRC_GEN, TestConstants.SRC_GEN);
				
				// delete expected folder (will be copied again for subsequent tests)
				IContainer expectedSrcGen = modelProject.getFolder(TestConstants.EXPECTED_RESULT);
				expectedSrcGen.delete(true, null);

				// also delete markers
				modelProject.deleteMarkers(TracepointConstants.tracepointMarker, true, IResource.DEPTH_INFINITE);
			}
		} catch (InvocationTargetException | IOException | CoreException e) {
			e.printStackTrace();
		}
	}

}
