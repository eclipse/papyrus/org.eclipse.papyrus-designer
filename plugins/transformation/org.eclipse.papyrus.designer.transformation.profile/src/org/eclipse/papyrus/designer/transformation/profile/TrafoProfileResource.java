/*****************************************************************************
 * Copyright (c) 2017, 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.profile;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get information on transformation profile resources
 */
public final class TrafoProfileResource {

	public static final String PROFILE_PATHMAP = "pathmap://TRAFO_PROFILE/"; //$NON-NLS-1$

	public static final String PROFILE_PATH = PROFILE_PATHMAP + "Transformation.profile.uml"; //$NON-NLS-1$
	
	public static final URI PROFILE_PATH_URI = URI.createURI(PROFILE_PATH);

	public static final String PROFILE_URI = "http://www.eclipse.org/papyrus/Transformation/1"; //$NON-NLS-1$
}
