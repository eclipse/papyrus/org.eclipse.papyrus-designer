/*****************************************************************************
 * Copyright (c) 2024 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.infra.base;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

/**
 * A simple class that is based on a combination of instance and default scope. It uses the DefaultScope to recover default values
 * The class should behave as the ScopedPreferenceStore in eclipse UI, but we want to avoid a dependency to eclipse.ui for headless
 * operation.
 */
public class ScopedPreferences {

	IEclipsePreferences instancePrefs;
	IEclipsePreferences defaultPrefs;

	/**
	 * Create preferences, based on a combination of instance and default scope with the passed ID
	 *
	 * @param nodeId
	 *            the id of the node to use for instance and default scope
	 */
	public ScopedPreferences(String nodeId) {
		instancePrefs = InstanceScope.INSTANCE.getNode(nodeId);
		defaultPrefs = DefaultScope.INSTANCE.getNode(nodeId);
	}

	/**
	 * @see org.osgi.service.prefs.Preferences#get(java.lang.String, java.lang.String)
	 *
	 * @param key
	 * @param def
	 * @return
	 */
	public String get(String key, String def) {
		return instancePrefs.get(key, defaultPrefs.get(key, def));
	}

	/**
	 * @see org.osgi.service.prefs.Preferences#getInt(java.lang.String, int)
	 *
	 * @param key
	 * @param def
	 * @return
	 */
	public int getInt(String key, int def) {
		return instancePrefs.getInt(key, defaultPrefs.getInt(key, def));
	}

	/**
	 * @see org.osgi.service.prefs.Preferences#getBoolean(java.lang.String, boolean)
	 *
	 * @param key
	 * @param def
	 * @return
	 */
	public boolean getBoolean(String key, boolean def) {
		return instancePrefs.getBoolean(key, defaultPrefs.getBoolean(key, def));
	}

	/**
	 * @see org.osgi.service.prefs.Preferences#getDouble(java.lang.String, double)
	 *
	 * @param key
	 * @param def
	 * @return
	 */
	public double getDouble(String key, double def) {
		return instancePrefs.getDouble(key, defaultPrefs.getDouble(key, def));
	}
}