/*****************************************************************************
 * Copyright (c) 2013, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.infra.base;

/**
 * A collection of common string constants
 */
@SuppressWarnings("nls")
public class StringConstants {
	public static final String EMPTY = "";
	public static final String STAR = "*";
	public static final String DOT = ".";
	public static final String COLON = ":";
	public static final String SPACE = " ";
	public static final String SLASH = "/";
	public static final String BSLASH = "\\";
	public static final String SINGLE_QUOTE = "'";
	public static final String QUOTE = "\"";
	public static final String UNDERSCORE = "_";

	public static final String DOC_START = "/**";
	public static final String COMMENT_START = "/**";
	public static final String COMMENT_END = "*/";

	// EOL = always \n
	public static final String EOL = "\n";
	
	// NL depends on used system
	public static final String NL = System.getProperties().getProperty("line.separator"); //$NON-NLS-1$

	public static final char EOL_C = EOL.charAt(0);
}
