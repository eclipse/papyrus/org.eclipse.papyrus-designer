/*****************************************************************************
 * Copyright (c) 2022 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.infra.ui;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.ui.util.ServiceUtilsForHandlers;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;

/**
 * Utility functions around UI
 */
public class UIUtils {

	/**
	 * Extension of UML models
	 */
	public static final String UML_EXT = "uml"; //$NON-NLS-1$

	/**
	 * Return the top element of the model that is currently edited.
	 * Use this function instead of Utils.getTop (or getModel) if you want to avoid navigating to the
	 * root of an imported model.
	 *
	 * @return the top level package of the model currently loaded into an editor.
	 */
	public static Package getUserModel(ExecutionEvent event) {
		ServiceUtilsForHandlers serviceUtils = ServiceUtilsForHandlers.getInstance();
		try {
			URI uri = serviceUtils.getModelSet(event).getURIWithoutExtension().appendFileExtension(UML_EXT);
			Resource userResource = serviceUtils.getModelSet(event).getResource(uri, false);
			if (userResource != null && userResource.getContents().size() > 0) {
				EObject topEObj = userResource.getContents().get(0);
				if ((topEObj instanceof Package) && (!(topEObj instanceof Profile))) {
					return (Package) topEObj;
				}
			}
		} catch (ServiceException e) {
			Activator.log.error(e);
		}
		return null;
	}
}
