/*******************************************************************************
 * Copyright (c) 2024 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.infra.ui;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


/**
 * This class represents the designer top-level preference page.
 * It is intentionally empty (no field editors)
 */
public class DesignerPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public DesignerPreferencePage() {
		super(GRID);
		setDescription("Papyrus SW Designer preferences"); //$NON-NLS-1$
	}

	
	@Override
	public void init(IWorkbench workbench) {
	}

	/**
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 *
	 */
	@Override
	protected void createFieldEditors() {
		// empty, only used to structure preferences
	}
}
