package org.eclipse.papyrus.designer.components.modellibs.examples;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.components.modellibs.core.messages"; //$NON-NLS-1$
	public static String ConnectorReification_CannotFindMatchingPort;
	public static String ConnectorReification_CANT_FIND_IMPLEMENTATION;
	public static String ConnectorReification_CANT_FIND_IMPLEMENTATION_DIST;
	public static String ConnectorReification_CouldNotBind;
	public static String ConnectorReification_CouldNotConnectPort;
	public static String ConnectorReification_CouldNotConnectPortOfType;
	public static String ConnectorReification_InfoAddConnectorPart;
	public static String ConnectorReification_InfoPortTypes;
	public static String ConnectorReification_RequiresUseOfPorts;
	
	public static String TemplatePort_CANT_CREATE_TEMPLATE_BINDING;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
