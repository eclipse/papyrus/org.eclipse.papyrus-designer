/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.components.modellibs.core.transformations;

public class Constants {
	public static final String CREATE_CONNECTIONS = "createConnections"; //$NON-NLS-1$
	
	public static final String retParamName = "ret"; //$NON-NLS-1$
}
