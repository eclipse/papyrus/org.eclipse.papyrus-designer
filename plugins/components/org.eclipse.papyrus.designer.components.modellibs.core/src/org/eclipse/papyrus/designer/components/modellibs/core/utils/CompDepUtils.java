/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.components.modellibs.core.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.components.FCM.ImplementationGroup;
import org.eclipse.papyrus.designer.components.FCM.InteractionComponent;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.deployment.tools.ImplementationChooser;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CompDepUtils {
	
	public static Class chooseImplementation(Class componentType, EList<InstanceSpecification> nodes, ImplementationChooser chooser) {

		EList<Class> implList = new BasicEList<Class>();
		boolean forDistribution = nodes.size() > 1;
		
		if (StereotypeUtil.isApplied(componentType, ImplementationGroup.class)) {
			for (Property groupAttribute : componentType.getAttributes()) {
				Type implClass = groupAttribute.getType();
				if ((implClass instanceof Class) && DepUtils.isImplEligible((Class) implClass, nodes)) {
					InteractionComponent connImpl = UMLUtil.getStereotypeApplication(implClass, InteractionComponent.class);
					if (connImpl != null) {
						if (connImpl.isForDistribution() == forDistribution) {
							// use distributed interaction component, if distributed
							// non-distributed interaction component, if non
							implList.add((Class) implClass);
						}
					}
				}
			}
		}
		return DepUtils.chooseImplementation(implList, componentType, nodes, chooser);
	}
	
}
