#ifndef PKG_PRODUCERCONSUMER_DEPLOYMENT_MONOPULL2
#define PKG_PRODUCERCONSUMER_DEPLOYMENT_MONOPULL2

/************************************************************
 Pkg_monoPull2 package header
 ************************************************************/

#include "ProducerConsumer/deployment/Pkg_deployment.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace ProducerConsumer {
namespace deployment {
namespace monoPull2 {

// Types defined within the package
}// of namespace monoPull2
} // of namespace deployment
} // of namespace ProducerConsumer

/************************************************************
 End of Pkg_monoPull2 package header
 ************************************************************/

#endif
