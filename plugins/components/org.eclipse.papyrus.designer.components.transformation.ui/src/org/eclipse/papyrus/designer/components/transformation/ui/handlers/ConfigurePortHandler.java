/********
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.transformation.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.papyrus.designer.components.transformation.ui.Messages;
import org.eclipse.papyrus.designer.components.transformation.ui.dialogs.ConfigurePortDialog;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.RunnableWithResult;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Port;

/**
 * Implementation class for ClassAction action
 */
public class ConfigurePortHandler extends CmdHandler {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		if (!(selectedEObject instanceof NamedElement)) {
			return null;
		}

		final NamedElement element = (NamedElement) selectedEObject;
		final Shell shell = Display.getCurrent().getActiveShell();

		// 1. select possible connectors according to port types
		// (only show compatible connectors check-box?)
		// 2. select implementation group according to connector type

		if (element instanceof Class) {
			CommandSupport.exec(element, Messages.ConfigurePortHandler_ConfigurePorts, new RunnableWithResult() {

				@Override
				public IStatus run() {
					ConfigurePortDialog configurePortDialog = new ConfigurePortDialog(shell);
					if (configurePortDialog.init((Class) element)) {
						configurePortDialog.setTitle(Messages.ConfigurePortHandler_ConfigurePorts);
						configurePortDialog.setMessage(
								Messages.ConfigurePortHandler_ConfigurePortsOfComponent + " " + element.getName()); //$NON-NLS-1$
						configurePortDialog.open();
						if (configurePortDialog.getReturnCode() == IDialogConstants.OK_ID) {
							return Status.OK_STATUS;
						}
					}
					return Status.CANCEL_STATUS;
				}
			});
		} else if (element instanceof Port) {
			CommandSupport.exec(element, Messages.ConfigurePortHandler_ConfigurePorts, new RunnableWithResult() {

				@Override
				public IStatus run() {
					ConfigurePortDialog configurePortDialog = new ConfigurePortDialog(shell);
					if (configurePortDialog.init((Port) element)) {
						configurePortDialog.setTitle(Messages.ConfigurePortHandler_ConfigurePorts);
						configurePortDialog
								.setMessage(Messages.ConfigurePortHandler_ConfigurePorts + " " + element.getName()); //$NON-NLS-1$
						configurePortDialog.open();
						if (configurePortDialog.getReturnCode() == IDialogConstants.OK_ID) {
							return Status.OK_STATUS;
						}
					}
					return Status.CANCEL_STATUS;
				}
			});
		}
		return null;
	}
}
