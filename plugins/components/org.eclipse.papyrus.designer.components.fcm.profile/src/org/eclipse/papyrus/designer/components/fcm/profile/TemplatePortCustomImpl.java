/**
 * Copyright (c) 2021 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - Initial API and implementation
 *
 */
package org.eclipse.papyrus.designer.components.fcm.profile;

import org.eclipse.papyrus.designer.components.FCM.PortKind;
import org.eclipse.papyrus.designer.components.FCM.impl.TemplatePortImpl;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;

/**
 * Custom implementation of PortKind stereotype
 */
public class TemplatePortCustomImpl extends TemplatePortImpl {
	/**
	 * Calculate boundType
	 */
	public PortKind basicGetBoundType() {
		if (base_Port == null) {
			return null;
		}
		if (base_Port.isConjugated()) {
			return PortMapUtil.getBoundType(this);
		} else {
			return PortMapUtil.getBoundType(this);
		}

	}
}
