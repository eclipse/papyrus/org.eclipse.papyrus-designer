/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.designer.components.FCM.InteractionComponent;
import org.eclipse.papyrus.designer.components.transformation.templates.ConnectorBinding;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Check whether a connector is connectable, i.e. whether the ports of application parts match with those
 * of the connector types that are chosen.
 *
 * @author ansgar
 *
 */
public class ConnectorConnectable extends AbstractModelConstraint
{

	@Override
	public IStatus validate(IValidationContext ctx)
	{
		Connector connector = (Connector) ctx.getTarget();
		Class class_ = (Class) connector.getOwner();

		org.eclipse.papyrus.designer.components.FCM.Connector fcmConnector = UMLUtil.getStereotypeApplication(connector, org.eclipse.papyrus.designer.components.FCM.Connector.class);
		if (fcmConnector != null) {
			InteractionComponent connectorComp = fcmConnector.getIc();
			if (connectorComp != null) {
				try {
					ConnectorBinding.obtainBinding(class_, connector, connectorComp.getBase_Class(), false);
				} catch (TransformationException e) {
					return ctx.createFailureStatus(String.format("The connector '%s' within composite '%s' cannot be instantiated: %s", //$NON-NLS-1$
							connector.getName(), class_.getName(), e.getMessage()));
				}
			}
		}
		return ctx.createSuccessStatus();
	}
}
