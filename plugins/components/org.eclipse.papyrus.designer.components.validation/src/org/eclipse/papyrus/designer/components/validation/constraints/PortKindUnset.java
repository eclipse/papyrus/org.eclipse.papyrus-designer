/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Check whether the attribute port-kind of the FCM stereotype port is set
 *
 */
public class PortKindUnset extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx)
	{
		Port port = (Port) ctx.getTarget();
		org.eclipse.papyrus.designer.components.FCM.Port fcmPort = UMLUtil.getStereotypeApplication(port, org.eclipse.papyrus.designer.components.FCM.Port.class);
		if (fcmPort != null) {

			if (fcmPort.getKind() == null) {
				return ctx.createFailureStatus(String.format(
						"The stereotype attribute 'portKind' of port '%s' is unset", port.getName())); //$NON-NLS-1$
			}
		}
		return ctx.createSuccessStatus();
	}

}
