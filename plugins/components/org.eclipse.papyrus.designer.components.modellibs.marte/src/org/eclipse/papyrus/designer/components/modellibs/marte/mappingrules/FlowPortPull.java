/*******************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.marte.mappingrules;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.FlowDirectionKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.FlowProperty;
import org.eclipse.papyrus.designer.components.FCM.Port;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * A MARTE FlowPort typed with a flow specification or a datatype. Flow properties for consumption ("in") are obtained via polling,
 * flow properties for production ("out") are distributed via push. In case of a datatype, we assume that
 * the data is produced as well as consumed.
 * Thus, there are two interfaces, one for data consumption that is provided by the port and one for data production
 * that is required by the port.
 */
public class FlowPortPull implements IMappingRule {

	public static final String FLOW_PORT_PULL = "FlowPortPull"; //$NON-NLS-1$

	public static final String IFLOW_PORT_PULL = "IFlowPortPull"; //$NON-NLS-1$

	@Override
	public Type calcDerivedType(Port p, boolean update) {
		Type type = p.getBase_Port().getType();

		if((type instanceof Interface)) {
			Interface intf = (Interface)type;
			int inout = 0;
			for(Property attribute : intf.getOwnedAttributes()) {
				if(StereotypeUtil.isApplied(attribute, FlowProperty.class)) {
					inout++;
				}
			}

			Interface derivedInOutInterface = null;
			if(inout > 0) {
				Class derivedType = PortMapUtil.getDerivedClass(p, FLOW_PORT_PULL, update);
				derivedInOutInterface = PortMapUtil.getDerivedInterface(p, IFLOW_PORT_PULL, update);

				RealizationUtils.addRealization(derivedType, derivedInOutInterface);
				
				if(derivedInOutInterface == null) {
					// may happen, if within template (do not want creation of derived interfaces in template)
					return null;
				}

				// check whether operations already exists. Create, if not
				for(Property attribute : intf.getOwnedAttributes()) {
					if(StereotypeUtil.isApplied(attribute, FlowProperty.class)) {
						FlowProperty fp = UMLUtil.getStereotypeApplication(attribute, FlowProperty.class);
						FlowDirectionKind fdk = fp.getDirection();
						if((fdk == FlowDirectionKind.IN) || (fdk == FlowDirectionKind.INOUT)) {
							FlowPortPush.createOrCheckOp("pull_", derivedInOutInterface, fp.getBase_Property(), ParameterDirectionKind.RETURN_LITERAL);
						}
						if((fdk == FlowDirectionKind.OUT) || (fdk == FlowDirectionKind.INOUT)) {
							FlowPortPush.createOrCheckOp("push_", derivedInOutInterface, fp.getBase_Property(), ParameterDirectionKind.IN_LITERAL);
						}
					}
				}
			}
			return derivedInOutInterface;
		}

		else if(type instanceof DataType) {
			// if typed with data type - handle atomic MARTE flow port with inout (can we always assume a data type?)
			Interface derivedInOutInterface = PortMapUtil.getDerivedInterface(p, IFLOW_PORT_PULL);
			FlowPortPush.createOrCheckOp("push_data", derivedInOutInterface, type, ParameterDirectionKind.IN_LITERAL);
			FlowPortPush.createOrCheckOp("pull_data", derivedInOutInterface, type, ParameterDirectionKind.RETURN_LITERAL);
			return derivedInOutInterface;
		}

		else {
			return null;
		}
	}

	@Override
	public boolean needsUpdate(Port p) {
		return false;
	}
}
