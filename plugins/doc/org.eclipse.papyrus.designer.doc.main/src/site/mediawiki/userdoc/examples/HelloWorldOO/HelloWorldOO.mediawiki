= Object-oriented "Hello World" example =

The Hello World OO example is a simple model from which an OO code can be generated.

== Examine the example model ==

Copy the model into your workspace using File->New->Example. Select the Papyrus example category and the sub-category "Papyrus Designer examples based on direct code generation". Select the SimpleSM example and finalize the wizard. The model will be created within a project of the same name and automatically opened.

When opened for the first time, the user will see the "Welcome" page.

<center>
	[[Image:welcome-page.png|800px]]<br/>
	Welcome page
</center>

Open the contained class diagram in the lower-right part (Notation Views), you'll find the HelloWorld class diagram. Click on it.
Afterwards, you'll see the following screenshot.

<center>
	[[Image:hello-world-behavior.png]]<br/>
	Hello World class diagram with opaque behavior
</center>

In this screenshot, the behavior "run" within class HelloWorld is selected in the model explorer. You see that the UML tab of the property view shows the content of this so-called opaque-behavior. An opaque behavior is a code fragment that is embedded into the model. It consists of a language tag (in this case C++) and a body that contains the actual code. The C++ code generator will use these fragments to create not only a skeleton of your code, but a complete executable.

It is also important to understand the difference between a UML operation and a behavior. The former is a specification of the parameters and return types (a method signature), the latter of the implementation.

<center>
	[[Image:hello-world-main.png|800px]]<br/>
	Hello World class diagram showing <<Include>> stereotype
</center>

In the screenshot above, the class "Main" is selected. Note that the C/C++ section of the property view shows attributes of the "Include" stereotype. The "body" stereotype attribute is used here to add a main function that will instantiate the HelloWorld class and subsequently call the "run" method of this instance. The reason for using a stereotype instead of defining a method is operations belong always to a class (even if defined as static) and can't be used to produce a "main" method without namespace. 


== Generate code ==

Now, let's generate C++ from the model. Right click on the package and choose "Generate Code", as shown in the following screenshot.

<center>
	[[Image:hello-world-codegen.png|800px]]<br/>
	Hello World class diagram with SW Designer context menu
</center>

Answer with "yes" to the question whether a CDT project should be created. Don't change parameters or name in the following CDT
wizard page. Once created, the CDT project can be build and run.
