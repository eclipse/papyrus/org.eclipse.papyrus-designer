= SimpleSM example =

The example model "SimpleSM" is model that contains a simple state-machine. In order to generated code from it, a sequence of transformations is executed.

== Examine the example model ==

Copy the model into your workspace using File->New->Example. Select the Papyrus example category and the sub-category "Papyrus Designer examples based on M2M transformation". Select the SimpleSM example and finalize the wizard. The model will be created within a project of the same name and automatically opened.

When opened for the first time, the user will see the "Welcome" page.
Open the contained state-machine diagram in the lower-right part (Notation Views) by clicking on it.
Afterwards, you'll see the following screenshot.

<center>
	[[Image:SimpleSM-SMdiag.png|1000px]]<br/>
	Simple SM state-machine diagram
</center>

The diagram constains two states, "Flip" and "Flop" and transitions between these states. The "Flop" state is reached, whenever the call-event "add" is emitted (which happens when the add operation gets called) or after 250 milliseconds. The latter is a time event which is specified by using the MARTE VSL (value specification language, basically a combination of value and a unit, see for instance [https://www.omg.org/omgmarte/Documents/tutorial/part2.pdf]). The xtext-based transition editor (invoke py pressing F2 on a selected transition) supports the creation or modification of triggers, guards and effects. In case of the example, the effect consists of an opaque behavior that prints to cout.
In a similar way, the transition from "Flop" to "Flip" happens after 500ms with a similar print-out.

== Generate code ==

Now, let's generate C++ from the model. Right click on the package and choose "Transform and Generate Code", as shown in the following screenshot. Please note, that the stereotype ExecuteTransformationChain is applied to the package. It will assure that code generation via transformation is enable and optionally reference a specific transformation chain. In case of the example, no transformation chain is specified and a default transformation chain (that includes state-machine creation) is chosen.

<center>
	[[Image:SimpleSM-ClassDiag.png|1000px]]<br/>
	Simple SM class diagram
</center>

During the first code generation, a CDT project will be created. Don't change parameters or name in the following CDT
wizard page. Once created, the CDT project can be build and run.
