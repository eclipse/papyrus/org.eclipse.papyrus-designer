== Producer Consumer ==

The producer consumer example consists of two interacting components that are part of a system component. These components communicate in a data flow style in two different variants. In the first, the (data) consumer is invoked from the environment to push new data. In the second, the consumer component asks for new data. Therefore, the two different variants are denoted as Push and Pull-Consumer respectively.

The flow-port characteristic is specified by means of the port-kind attribute within the FCM profile. The port kind is associated with mapping rules that determine which interfaces must be implemented by a port (derived provided) and which interfaces can be called through a port. In case of the data-flow ports, the ports correspond

* Push-producer: required interface with single "push" operation
* Push-consumer: provided interface with single "push" operation (same interface as for push producer)
* Pull-consumer: required interface with pull operation and boolean query whether data is available.

In the push variant, the producer can directly call the push operation that is provided by a consumer.
In case of the pull variant, both components call operations to obtain data. Thus, an additional component is needed in order to implement the interaction. In case of the example, this is a FIFO queue that provides suitable ports for the consumer and producer.
