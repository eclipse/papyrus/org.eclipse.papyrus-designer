= Connector Development =

The definition of a connector is quite similar to that of a component. Thus, its development is
quite similar as well. The major differences are:

* The type of ports are typically generic, i.e. template parameters.
* A connector is often distributed, i.e. the connector itself is an assembly of connector fragments
* It may be required to define additional port kinds

In the following steps, we address the issues above in addition to the normal development of a component.

== Integration into a package template ==

The ports of a connector are often generic, since the connector should connect a large variety of
components. Therefore, the type of the ports often refer to a template parameter. For instance, in 
case of ports providing or requiring an interface, the BasicCalls model library defines a package
template with the template parameter &quot;I&quot; which denotes an arbitrary interface.
When a new connector is defined, the developer should first verify if a suitable package template
already exists.
The use of a package import declares the extension of a existing package.
It is possible to extend this package template by eds to refer to a generic port type - a port whose type adapts that refers to template parameters

In the context of a new interaction component definition, it may be necessary to define new port kinds. A port kind associates a semantics (informally) with a port. It consists of a rule for the provided and required interface in function of the port type.
The rule is currently coded in Java and provided via an (Eclipse) extension point defined by the FCM plugin. This extension point is called fcmPortMappingRule and associates the name of a port kind with a class.&lt;br&gt;
The class must implement the interface FCM.util.IMappingRule, i.e. the operations getProvided, getRequired and needsTransaction. The former two return the provided (required) interface in function of the port type. The latter indicates whether the calculation of one of these (potentially) modifies the model by creating/or updating a derived interface and thus requires a transaction.


An interaction component is the realization of an interaction pattern which is defined by an abstract interaction
component. There are two cases,

* You want to create a new implementation of an existing interaction mechanism
* You want to create an additional interaction mechanism                                                                                   

A particularity of a connector is that it is typically defined in a package template. In the first case, i.e.
the creation of an existing implementation.
Instead of creating a new package template for each new connector, an existing package template should be
extended - provided that the formal parameters are identical. This favors reuse, e.g. the existing package
template for method calls contains the definition of (a)synchronous connector types as well an enumeration
over all the operation within the interface (template parameter).<br>

There is no means in UML of extending an existing package. In SW Designer, a package is extending an existing package
template, if it merges with the existing package template.
