/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.prefs.ui.pages;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.papyrus.designer.monitoring.prefs.MonitoringPreferencesConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

public class MonitoringPreferencesPage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {
	
	public MonitoringPreferencesPage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE, org.eclipse.papyrus.designer.monitoring.prefs.Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("This preferences page allows to customize Papyrus monitoring"); //$NON-NLS-1$
	}

	@Override
	public void createFieldEditors() {
		addField(new BooleanFieldEditor(MonitoringPreferencesConstants.P_ENABLED, "Enable monitoring", getFieldEditorParent())); //$NON-NLS-1$
		addField(new StringFieldEditor(MonitoringPreferencesConstants.P_IP_ADDRESS, "Monitor IP address", getFieldEditorParent()));  //$NON-NLS-1$
		addField(new IntegerFieldEditor(MonitoringPreferencesConstants.P_PORT, "Monitor port", getFieldEditorParent()));  //$NON-NLS-1$
	}

	@Override
	public void init(IWorkbench arg0) {
	}
}
