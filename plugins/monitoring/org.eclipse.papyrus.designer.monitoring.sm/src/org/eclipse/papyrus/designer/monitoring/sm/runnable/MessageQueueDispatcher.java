/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.runnable;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.papyrus.designer.monitoring.sm.data.Message;
import org.eclipse.papyrus.designer.monitoring.sm.data.MessageQueue;
import org.eclipse.papyrus.designer.monitoring.sm.data.StateMachineModel;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;

public class MessageQueueDispatcher implements Runnable {

	private boolean running;
	private final int sleepTime = 10;

	private MessageQueue socketDataMessageQueue;
	private Map<String, MessageQueue> animatorMessageQueues;
	private StateMachineModel stateMachineModel;

	public MessageQueueDispatcher(MessageQueue socketDataMessageQueue, Map<String, MessageQueue> animatorMessageQueueReaders, StateMachineModel stateMachineModel) {
		running = true;

		this.animatorMessageQueues = animatorMessageQueueReaders;
		this.socketDataMessageQueue = socketDataMessageQueue;
		this.stateMachineModel = stateMachineModel;
	}

	@Override
	public void run() {
		while (running) {
			Message message = null;

			if (socketDataMessageQueue != null) {
				message = socketDataMessageQueue.poll();
			}

			if (message != null) {
				String header = message.getHeader();
				String data = message.getData();

				StateMachine containingStateMachine = null;
				String stateMachineQualifiedName = null;

				if (header.equals(Message.TRANSITION_HEADER)) {
					if (stateMachineModel != null) {
						Transition transition = stateMachineModel.getTransitionMap().get(data.trim());
						if (transition != null) {
							containingStateMachine = transition.containingStateMachine();
						}
					}
				}

				if (header.equals(Message.STATE_HEADER)) {
					State state = stateMachineModel.getStateMap().get(data.trim());
					if (state != null) {
						containingStateMachine = state.containingStateMachine();
					}
				}

				if (containingStateMachine != null) {
					stateMachineQualifiedName = containingStateMachine.getQualifiedName();
					if (stateMachineQualifiedName != null && !stateMachineQualifiedName.isEmpty()) {
						MessageQueue animatorMessageQueue = animatorMessageQueues.get(stateMachineQualifiedName);
						if (animatorMessageQueue != null) {
							animatorMessageQueue.getMessages().add(message);
						}
					}
				}

				if (header.equals(AnimatorMessageQueueReader.PAPYRUS_END_SM_MONITORING)) {
					Iterator<Entry<String, MessageQueue>> it = animatorMessageQueues.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<String, MessageQueue> pair = (Map.Entry<String, MessageQueue>) it.next();
						if (pair.getValue() instanceof MessageQueue) {
							MessageQueue animatorMessageQueue = (MessageQueue) pair.getValue();
							animatorMessageQueue.getMessages().add(message);
						}
						it.remove();
					}
					running = false;
				}
			}

			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		running = false;
	}
}
