/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.papyrus.designer.monitoring.sm.animation.Animator;
import org.eclipse.papyrus.designer.monitoring.sm.data.MessageQueue;
import org.eclipse.papyrus.designer.monitoring.sm.data.StateMachineModel;
import org.eclipse.papyrus.designer.monitoring.sm.runnable.DatagramSocketReader;
import org.eclipse.papyrus.designer.monitoring.sm.runnable.MessageQueueDispatcher;
import org.eclipse.papyrus.designer.monitoring.sm.runnable.AnimatorMessageQueueReader;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.StateMachine;

public class StartMonitoringHandler extends CmdHandler {
	public static boolean monitoring = false;
	public static DatagramSocketReader socketReader;
	public static Map<String, MessageQueue> animatorMessageQueues; // There is one message queue per state machine
	public static List<AnimatorMessageQueueReader> animatorMessageQueueReaders; // There is one animator thread per state machine
	public static MessageQueueDispatcher messageQueueDispatcher; // There is one dispatch thread which will dispatch to animatorMessageQueueReaders with their own message queues

	@Override
	public boolean isEnabled() {
		updateSelectedEObject();
		if (selectedEObject instanceof Element) {
			return true;
		}
		return false;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (!(selectedEObject instanceof Element)) {
			return null;
		}

		if (!monitoring) {
			MessageQueue socketDataMessageQueue = new MessageQueue();
			socketReader = new DatagramSocketReader(socketDataMessageQueue);

			if (socketReader.getSocket() != null) { // Socket is null if we cannot bind to port already in use (user must choose another port in the preferences)
				// Common state machine model and animator
				StateMachineModel stateMachineModel = new StateMachineModel(((Element) selectedEObject).getModel());
				Animator animator = new Animator(stateMachineModel);
				
				// The animator threads
				animatorMessageQueues = new HashMap<String, MessageQueue>();
				animatorMessageQueueReaders = new ArrayList<AnimatorMessageQueueReader>();
				for (StateMachine stateMachine : stateMachineModel.getStateMachines()) {
					if (stateMachine.getQualifiedName() != null && !stateMachine.getQualifiedName().isEmpty()) {
						MessageQueue animatorMessageQueue = new MessageQueue();
						AnimatorMessageQueueReader animatorMessageQueueReader = new AnimatorMessageQueueReader(animatorMessageQueue, animator);
						animatorMessageQueues.put(stateMachine.getQualifiedName(), animatorMessageQueue);
						animatorMessageQueueReaders.add(animatorMessageQueueReader);
					}
				}

				// The dispatch thread
				messageQueueDispatcher = new MessageQueueDispatcher(socketDataMessageQueue, animatorMessageQueues, stateMachineModel);

				// Launch
				monitoring = true;
				for (AnimatorMessageQueueReader animatorMessageQueueReader : animatorMessageQueueReaders) {
					Thread animatorMessageQueueReaderThread = new Thread(animatorMessageQueueReader);
					animatorMessageQueueReaderThread.start();
				}
				Thread messageQueueDispatcherThread = new Thread(messageQueueDispatcher);
				messageQueueDispatcherThread.start();
				Thread socketReaderThread = new Thread(socketReader);
				socketReaderThread.start();
			}
		}

		return null;
	}
}
