/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.papyrus.designer.monitoring.sm.runnable.AnimatorMessageQueueReader;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Element;

public class StopMonitoringHandler extends CmdHandler {
	@Override
	public boolean isEnabled() {
		updateSelectedEObject();
		if (selectedEObject instanceof Element) {
			return true;
		}
		return false;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (!(selectedEObject instanceof Element)) {
			return null;
		}
		
		if (StartMonitoringHandler.socketReader != null) {
			StartMonitoringHandler.socketReader.stop();
		}
		
		if (StartMonitoringHandler.messageQueueDispatcher != null) {
			StartMonitoringHandler.messageQueueDispatcher.stop();
		}
		
		if (StartMonitoringHandler.animatorMessageQueueReaders != null) {
			for (AnimatorMessageQueueReader animatorMessageQueueReader : StartMonitoringHandler.animatorMessageQueueReaders) {
				animatorMessageQueueReader.stop();
			}
		}
		
		StartMonitoringHandler.monitoring = false;
		
		return null;
	}
}
