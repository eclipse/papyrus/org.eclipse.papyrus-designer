/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.runnable;

import org.eclipse.papyrus.designer.monitoring.sm.animation.Animator;
import org.eclipse.papyrus.designer.monitoring.sm.data.Message;
import org.eclipse.papyrus.designer.monitoring.sm.data.MessageQueue;

public class AnimatorMessageQueueReader implements Runnable {
	public static final String PAPYRUS_END_SM_MONITORING = "PAPYRUS_END_SM_MONITORING"; //$NON-NLS-1$

	private boolean running;
	private final int sleepTime = 10;

	private MessageQueue messageQueue;
	private Animator animator;

	public AnimatorMessageQueueReader(MessageQueue messageQueue, Animator animator) {
		running = true;

		this.messageQueue = messageQueue;
		this.animator = animator;
	}

	@Override
	public void run() {
		animator.initialize();

		while (running) {
			Message message = null;

			if (messageQueue != null) {
				message = messageQueue.poll();
			}

			if (message != null) {
				String header = message.getHeader();
				String data = message.getData();

				if (header.equals(Message.TRANSITION_HEADER) || header.equals(Message.STATE_HEADER)) {
					animator.animate(header, data);
				}

				if (header.equals(PAPYRUS_END_SM_MONITORING)) {
					running = false;
				}
			}

			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		animator.deleteMarkers();
	}

	public void stop() {
		running = false;
	}
}
