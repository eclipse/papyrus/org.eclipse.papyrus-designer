/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.data;

import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageQueue {
	private ConcurrentLinkedQueue<Message> messages;
	
	public MessageQueue() {
		messages = new ConcurrentLinkedQueue<Message>();
	}

	public synchronized ConcurrentLinkedQueue<Message> getMessages() {
		return messages;
	}

	public synchronized void setMessages(ConcurrentLinkedQueue<Message> messages) {
		this.messages = messages;
	}
	
	public synchronized Message poll() {
		if (messages != null) {
			return messages.poll();
		}
		
		return null;
	}
}
