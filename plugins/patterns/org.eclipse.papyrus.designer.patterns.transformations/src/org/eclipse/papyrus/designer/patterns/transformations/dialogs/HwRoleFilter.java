/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.papyrus.designer.patterns.transformations.Utils;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;


public class HwRoleFilter extends ViewerFilter {

	public static HwRoleFilter getInstance() {
		if(hwRoleFilter == null) {
			hwRoleFilter = new HwRoleFilter();
		}
		return hwRoleFilter;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if(element instanceof Property) {
			Type type = ((Property)element).getType();
			return (type != null) && Utils.isHwComponent(type);
		}
		return false;
	}

	protected static HwRoleFilter hwRoleFilter = null;
}
