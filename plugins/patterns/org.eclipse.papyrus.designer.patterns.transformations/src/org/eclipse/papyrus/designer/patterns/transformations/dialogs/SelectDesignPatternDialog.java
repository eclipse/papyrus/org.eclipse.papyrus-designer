/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.transformations.Apply;
import org.eclipse.papyrus.designer.patterns.transformations.Messages;
import org.eclipse.papyrus.designer.patterns.transformations.Utils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Select a design pattern: show list of available design patterns and provide
 * information about these.
 * 
 * Once a pattern is selected, delegate to application dialog TODO: select
 * specific solution via tree viewer?
 */
public class SelectDesignPatternDialog extends MessageDialog {

	private static final String CLOSE = "close"; //$NON-NLS-1$

	protected Class m_component;

	protected PatternInfoGroup pig;

	protected TableViewer fPatterns;

	protected NamedElement m_context;

	protected Pattern m_pattern;

	protected Package m_model;

	protected Button fApplyButton;

	/**
	 * Select a design pattern. Passed namedElement should be either a
	 * (composite) Class or a Package
	 * 
	 * @param parent
	 *        Parent shell
	 * @param namedElement
	 */
	public SelectDesignPatternDialog(Shell parent, NamedElement namedElement) {
		super(parent, Messages.SelectDesignPatternDialog_SELECT_PATTERN, null,
			    null, MessageDialog.NONE, new String[] { CLOSE }, 0);

		m_model = Utils.getTop(namedElement);
		m_context = namedElement;
		pig = new PatternInfoGroup();
	}

	/**
	 * @see SelectionStatusDialog#computeResult()
	 */
	protected void computeResult() {
		// nothing to do
	}

	public Control createDialogArea(Composite parent) {
		// Composite contents = (Composite)super.createDialogArea(parent);
		Composite contents = parent;
		
		// (parent, "Container rules", "Avail. extensions/interceptors");

		createPatternSelectionGroup(contents);
		pig.createPatternInfoGroup(contents);
		contents.setLayout(new GridLayout(2, false));

		fApplyButton = new Button(contents, SWT.NONE);
		fApplyButton.setText(Messages.SelectDesignPatternDialog_APPLY_PATTERN);
		fApplyButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				ApplyDesignPatternDialog dpd = new ApplyDesignPatternDialog(Display.getCurrent().getActiveShell(), m_model, m_pattern);
				dpd.open();
				if(dpd.getReturnCode() == IDialogConstants.OK_ID) {
					Object result[] = dpd.getResult();
					// receive pattern
					if((result.length == 2) && (result[0] instanceof Map) && (result[1] instanceof Map)) {
						@SuppressWarnings("unchecked")
						final Map<NamedElement, Object> userRoleMap = (Map<NamedElement, Object>)result[0];
						@SuppressWarnings("unchecked")
						final Map<NamedElement, Object> autoRoleMap = (Map<NamedElement, Object>)result[1];
						CommandSupport.exec(m_context, String.format("%s \"%s\"", Messages.SelectDesignPatternDialog_APPLY_PATTERN, //$NON-NLS-1$
								m_pattern.getBase_Package().getName()), new Runnable() {
							
							public void run() {
								Apply apply = new Apply(m_pattern, userRoleMap, autoRoleMap);
								apply.apply(m_context);
							}
						});
					}
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		return contents;
	}

	protected void createPatternSelectionGroup(Composite parent) {
		GridData groupGridData = new GridData();
		int width = 300;
		groupGridData.grabExcessVerticalSpace = true;
		groupGridData.grabExcessHorizontalSpace = true;
		groupGridData.horizontalAlignment = GridData.FILL;
		groupGridData.verticalAlignment = GridData.FILL;
		//
		// --------------- pattern selection -------------------
		//
		Group patternSelGroup = new Group(parent, SWT.BORDER);
		patternSelGroup.setText(Messages.SelectDesignPatternDialog_AVAIL_PATTERNS);
		patternSelGroup.setLayout(new GridLayout(1, false));
		patternSelGroup.setLayoutData(groupGridData);
		patternSelGroup.setSize(width, 300);

		fPatterns = new TableViewer(patternSelGroup, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.heightHint = 150;
		data.widthHint = width;
		fPatterns.getTable().setLayoutData(data);
		fPatterns.setContentProvider(new ArrayContentProvider());
		fPatterns.setLabelProvider(new PatternLabelProvider());
		fPatterns.setInput(getAvailablePatterns().toArray());
		patternSelGroup.pack();

		fPatterns.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				ISelection selection = fPatterns.getSelection();
				if(selection instanceof StructuredSelection) {
					Object[] selected = ((StructuredSelection)selection).toArray();
					if((selected.length == 1) && (selected[0] instanceof Pattern)) {
						selectPattern((Pattern)selected[0]);
					}
				}
			}
		});
		// idea: user selects a pattern (restricted?)
		// afterwards: dialog shows different roles within the pattern and
		// demands for each:
		// create
		// associate
		// HOWTO configure IP-Adresses/port-number of socket?
		//
	}

	/**
	 * return a list of patterns that are available, i.e. available in the
	 * current model or imported
	 */
	protected EList<Pattern> getAvailablePatterns() {
		EList<Pattern> patternList = new UniqueEList<Pattern>();
		EList<Package> visitedPackages = new BasicEList<Package>();
		getAvailablePatterns(m_model, patternList, visitedPackages);
		return patternList;
	}

	private void selectPattern(Pattern pattern) {
		m_pattern = pattern;
		if(pattern != null) {
			pig.updateInfo(pattern);
		}
	}


	protected void getAvailablePatterns(Package pkg, EList<Pattern> patternList, EList<Package> visitedPackages) {
		for(Element el : pkg.getMembers()) {
			if(el instanceof Package) {
				if(!visitedPackages.contains(el)) {
					visitedPackages.add((Package)el);
					Pattern pattern = UMLUtil.getStereotypeApplication(el, Pattern.class);
					if(pattern != null) {
						patternList.add(pattern);
					} else {
						// continue recursively (TODO: don't expect patterns
						// within patterns?)
						getAvailablePatterns((Package)el, patternList, visitedPackages);
					}
				}
			}
		}
	}
}
