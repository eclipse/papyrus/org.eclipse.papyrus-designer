/**
 */
package SafetyPatterns;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HW Comp Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SafetyPatterns.HWCompCapability#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link SafetyPatterns.HWCompCapability#getAvailability <em>Availability</em>}</li>
 *   <li>{@link SafetyPatterns.HWCompCapability#getErrorRate <em>Error Rate</em>}</li>
 *   <li>{@link SafetyPatterns.HWCompCapability#getReliability <em>Reliability</em>}</li>
 * </ul>
 *
 * @see SafetyPatterns.SafetyPatternsPackage#getHWCompCapability()
 * @model
 * @generated
 */
public interface HWCompCapability extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see SafetyPatterns.SafetyPatternsPackage#getHWCompCapability_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link SafetyPatterns.HWCompCapability#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Availability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Availability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Availability</em>' attribute.
	 * @see #setAvailability(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getHWCompCapability_Availability()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getAvailability();

	/**
	 * Sets the value of the '{@link SafetyPatterns.HWCompCapability#getAvailability <em>Availability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Availability</em>' attribute.
	 * @see #getAvailability()
	 * @generated
	 */
	void setAvailability(String value);

	/**
	 * Returns the value of the '<em><b>Error Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Rate</em>' attribute.
	 * @see #setErrorRate(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getHWCompCapability_ErrorRate()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getErrorRate();

	/**
	 * Sets the value of the '{@link SafetyPatterns.HWCompCapability#getErrorRate <em>Error Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Rate</em>' attribute.
	 * @see #getErrorRate()
	 * @generated
	 */
	void setErrorRate(String value);

	/**
	 * Returns the value of the '<em><b>Reliability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reliability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reliability</em>' attribute.
	 * @see #setReliability(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getHWCompCapability_Reliability()
	 * @model dataType="SafetyPatterns.Reliability" required="true" ordered="false"
	 * @generated
	 */
	String getReliability();

	/**
	 * Sets the value of the '{@link SafetyPatterns.HWCompCapability#getReliability <em>Reliability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reliability</em>' attribute.
	 * @see #getReliability()
	 * @generated
	 */
	void setReliability(String value);

} // HWCompCapability
