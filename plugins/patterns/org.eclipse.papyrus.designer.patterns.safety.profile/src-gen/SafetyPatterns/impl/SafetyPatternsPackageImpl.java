/**
 */
package SafetyPatterns.impl;

import SafetyPatterns.HWCompCapability;
import SafetyPatterns.SWCompRequirement;
import SafetyPatterns.SafetyPattern;
import SafetyPatterns.SafetyPatternsFactory;
import SafetyPatterns.SafetyPatternsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SafetyPatternsPackageImpl extends EPackageImpl implements SafetyPatternsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass safetyPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swCompRequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwCompCapabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType safetyPropertiesEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType resourcePropertiesEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType reliabilityEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType availabilityEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see SafetyPatterns.SafetyPatternsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SafetyPatternsPackageImpl() {
		super(eNS_URI, SafetyPatternsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link SafetyPatternsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SafetyPatternsPackage init() {
		if (isInited) return (SafetyPatternsPackage)EPackage.Registry.INSTANCE.getEPackage(SafetyPatternsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSafetyPatternsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SafetyPatternsPackageImpl theSafetyPatternsPackage = registeredSafetyPatternsPackage instanceof SafetyPatternsPackageImpl ? (SafetyPatternsPackageImpl)registeredSafetyPatternsPackage : new SafetyPatternsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DesignPatternsPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		MARTE_PrimitivesTypesPackage.eINSTANCE.eClass();
		MeasurementUnitsPackage.eINSTANCE.eClass();
		GRM_BasicTypesPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();
		MARTE_DataTypesPackage.eINSTANCE.eClass();
		TimeTypesLibraryPackage.eINSTANCE.eClass();
		TimeLibraryPackage.eINSTANCE.eClass();
		RS_LibraryPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		BasicNFP_TypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSafetyPatternsPackage.createPackageContents();

		// Initialize created meta-data
		theSafetyPatternsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSafetyPatternsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SafetyPatternsPackage.eNS_URI, theSafetyPatternsPackage);
		return theSafetyPatternsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSafetyPattern() {
		return safetyPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSafetyPattern_SafetyProperties() {
		return (EAttribute)safetyPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSafetyPattern_ResourceProperties() {
		return (EAttribute)safetyPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSWCompRequirement() {
		return swCompRequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSWCompRequirement_Period() {
		return (EAttribute)swCompRequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSWCompRequirement_Availability() {
		return (EAttribute)swCompRequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSWCompRequirement_ErrorRate() {
		return (EAttribute)swCompRequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWCompRequirement_Base_Class() {
		return (EReference)swCompRequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHWCompCapability() {
		return hwCompCapabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWCompCapability_Base_Class() {
		return (EReference)hwCompCapabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHWCompCapability_Availability() {
		return (EAttribute)hwCompCapabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHWCompCapability_ErrorRate() {
		return (EAttribute)hwCompCapabilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHWCompCapability_Reliability() {
		return (EAttribute)hwCompCapabilityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSafetyProperties() {
		return safetyPropertiesEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getResourceProperties() {
		return resourcePropertiesEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getReliability() {
		return reliabilityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAvailability() {
		return availabilityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyPatternsFactory getSafetyPatternsFactory() {
		return (SafetyPatternsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		safetyPatternEClass = createEClass(SAFETY_PATTERN);
		createEAttribute(safetyPatternEClass, SAFETY_PATTERN__SAFETY_PROPERTIES);
		createEAttribute(safetyPatternEClass, SAFETY_PATTERN__RESOURCE_PROPERTIES);

		swCompRequirementEClass = createEClass(SW_COMP_REQUIREMENT);
		createEAttribute(swCompRequirementEClass, SW_COMP_REQUIREMENT__PERIOD);
		createEAttribute(swCompRequirementEClass, SW_COMP_REQUIREMENT__AVAILABILITY);
		createEAttribute(swCompRequirementEClass, SW_COMP_REQUIREMENT__ERROR_RATE);
		createEReference(swCompRequirementEClass, SW_COMP_REQUIREMENT__BASE_CLASS);

		hwCompCapabilityEClass = createEClass(HW_COMP_CAPABILITY);
		createEReference(hwCompCapabilityEClass, HW_COMP_CAPABILITY__BASE_CLASS);
		createEAttribute(hwCompCapabilityEClass, HW_COMP_CAPABILITY__AVAILABILITY);
		createEAttribute(hwCompCapabilityEClass, HW_COMP_CAPABILITY__ERROR_RATE);
		createEAttribute(hwCompCapabilityEClass, HW_COMP_CAPABILITY__RELIABILITY);

		// Create data types
		safetyPropertiesEDataType = createEDataType(SAFETY_PROPERTIES);
		resourcePropertiesEDataType = createEDataType(RESOURCE_PROPERTIES);
		reliabilityEDataType = createEDataType(RELIABILITY);
		availabilityEDataType = createEDataType(AVAILABILITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DesignPatternsPackage theDesignPatternsPackage = (DesignPatternsPackage)EPackage.Registry.INSTANCE.getEPackage(DesignPatternsPackage.eNS_URI);
		BasicNFP_TypesPackage theBasicNFP_TypesPackage = (BasicNFP_TypesPackage)EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		safetyPatternEClass.getESuperTypes().add(theDesignPatternsPackage.getPattern());

		// Initialize classes, features, and operations; add parameters
		initEClass(safetyPatternEClass, SafetyPattern.class, "SafetyPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSafetyPattern_SafetyProperties(), this.getSafetyProperties(), "safetyProperties", null, 0, -1, SafetyPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSafetyPattern_ResourceProperties(), this.getResourceProperties(), "resourceProperties", null, 0, -1, SafetyPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(swCompRequirementEClass, SWCompRequirement.class, "SWCompRequirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSWCompRequirement_Period(), theBasicNFP_TypesPackage.getNFP_Duration(), "period", null, 1, 1, SWCompRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSWCompRequirement_Availability(), theBasicNFP_TypesPackage.getNFP_Percentage(), "availability", null, 1, 1, SWCompRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSWCompRequirement_ErrorRate(), theBasicNFP_TypesPackage.getNFP_Real(), "errorRate", null, 1, 1, SWCompRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSWCompRequirement_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, SWCompRequirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(hwCompCapabilityEClass, HWCompCapability.class, "HWCompCapability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getHWCompCapability_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, HWCompCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getHWCompCapability_Availability(), theBasicNFP_TypesPackage.getNFP_Percentage(), "availability", null, 1, 1, HWCompCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getHWCompCapability_ErrorRate(), theBasicNFP_TypesPackage.getNFP_Real(), "errorRate", null, 1, 1, HWCompCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getHWCompCapability_Reliability(), this.getReliability(), "reliability", null, 1, 1, HWCompCapability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Initialize data types
		initEDataType(safetyPropertiesEDataType, String.class, "SafetyProperties", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(resourcePropertiesEDataType, String.class, "ResourceProperties", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(reliabilityEDataType, String.class, "Reliability", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(availabilityEDataType, String.class, "Availability", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);
	}

} //SafetyPatternsPackageImpl
