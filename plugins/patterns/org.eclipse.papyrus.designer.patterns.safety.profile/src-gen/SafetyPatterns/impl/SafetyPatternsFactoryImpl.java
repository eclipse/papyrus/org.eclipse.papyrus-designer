/**
 */
package SafetyPatterns.impl;

import SafetyPatterns.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SafetyPatternsFactoryImpl extends EFactoryImpl implements SafetyPatternsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SafetyPatternsFactory init() {
		try {
			SafetyPatternsFactory theSafetyPatternsFactory = (SafetyPatternsFactory)EPackage.Registry.INSTANCE.getEFactory(SafetyPatternsPackage.eNS_URI);
			if (theSafetyPatternsFactory != null) {
				return theSafetyPatternsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SafetyPatternsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyPatternsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SafetyPatternsPackage.SAFETY_PATTERN: return createSafetyPattern();
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT: return createSWCompRequirement();
			case SafetyPatternsPackage.HW_COMP_CAPABILITY: return createHWCompCapability();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SafetyPatternsPackage.SAFETY_PROPERTIES:
				return createSafetyPropertiesFromString(eDataType, initialValue);
			case SafetyPatternsPackage.RESOURCE_PROPERTIES:
				return createResourcePropertiesFromString(eDataType, initialValue);
			case SafetyPatternsPackage.RELIABILITY:
				return createReliabilityFromString(eDataType, initialValue);
			case SafetyPatternsPackage.AVAILABILITY:
				return createAvailabilityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SafetyPatternsPackage.SAFETY_PROPERTIES:
				return convertSafetyPropertiesToString(eDataType, instanceValue);
			case SafetyPatternsPackage.RESOURCE_PROPERTIES:
				return convertResourcePropertiesToString(eDataType, instanceValue);
			case SafetyPatternsPackage.RELIABILITY:
				return convertReliabilityToString(eDataType, instanceValue);
			case SafetyPatternsPackage.AVAILABILITY:
				return convertAvailabilityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyPattern createSafetyPattern() {
		SafetyPatternImpl safetyPattern = new SafetyPatternImpl();
		return safetyPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWCompRequirement createSWCompRequirement() {
		SWCompRequirementImpl swCompRequirement = new SWCompRequirementImpl();
		return swCompRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWCompCapability createHWCompCapability() {
		HWCompCapabilityImpl hwCompCapability = new HWCompCapabilityImpl();
		return hwCompCapability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createSafetyPropertiesFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSafetyPropertiesToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createResourcePropertiesFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResourcePropertiesToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createReliabilityFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertReliabilityToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createAvailabilityFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAvailabilityToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyPatternsPackage getSafetyPatternsPackage() {
		return (SafetyPatternsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SafetyPatternsPackage getPackage() {
		return SafetyPatternsPackage.eINSTANCE;
	}

} //SafetyPatternsFactoryImpl
