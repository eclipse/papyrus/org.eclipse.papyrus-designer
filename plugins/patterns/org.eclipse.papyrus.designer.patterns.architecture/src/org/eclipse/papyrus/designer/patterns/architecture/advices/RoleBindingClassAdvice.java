/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.patterns.architecture.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper advice for a class called "CollaborationRoles". It will have a specific name and automatically
 * add the CollaborationUse binding relationship
 */
public class RoleBindingClassAdvice extends AbstractEditHelperAdvice {

	public static final String ROLE_BINDING = "RoleBinding"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Class)) {
			return super.getAfterConfigureCommand(request);
		}
		CompositeCommand compositeCommand = new CompositeCommand("name role binding and set collaborationUse command"); //$NON-NLS-1$

		IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(newElement);

		SetRequest setType = new SetRequest(newElement, UMLPackage.eINSTANCE.getNamedElement_Name(), ROLE_BINDING);
		compositeCommand.add(commandProvider.getEditCommand(setType));

		CollaborationUse cu = UMLFactory.eINSTANCE.createCollaborationUse();
		SetRequest addCU = new SetRequest(newElement, UMLPackage.eINSTANCE.getClassifier_CollaborationUse(), cu);
		compositeCommand.add(commandProvider.getEditCommand(addCU));

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	protected EObject source;
	protected EObject target;
}
