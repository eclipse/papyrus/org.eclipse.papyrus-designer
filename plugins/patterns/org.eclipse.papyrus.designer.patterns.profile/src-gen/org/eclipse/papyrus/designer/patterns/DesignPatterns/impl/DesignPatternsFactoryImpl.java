/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsFactoryImpl extends EFactoryImpl implements DesignPatternsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DesignPatternsFactory init() {
		try {
			DesignPatternsFactory theDesignPatternsFactory = (DesignPatternsFactory)EPackage.Registry.INSTANCE.getEFactory(DesignPatternsPackage.eNS_URI);
			if (theDesignPatternsFactory != null) {
				return theDesignPatternsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DesignPatternsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DesignPatternsPackage.CONTEXT: return createContext();
			case DesignPatternsPackage.APPLICABILITY: return createApplicability();
			case DesignPatternsPackage.INTENT: return createIntent();
			case DesignPatternsPackage.SOLUTION_DESC: return createSolutionDesc();
			case DesignPatternsPackage.PROBLEM: return createProblem();
			case DesignPatternsPackage.PROBLEM_KIND: return createProblemKind();
			case DesignPatternsPackage.SOLUTION: return createSolution();
			case DesignPatternsPackage.CONSEQUENCE: return createConsequence();
			case DesignPatternsPackage.SAMPLE_CODE: return createSampleCode();
			case DesignPatternsPackage.PATTERN: return createPattern();
			case DesignPatternsPackage.KNOWN_USE: return createKnownUse();
			case DesignPatternsPackage.REPLICATION: return createReplication();
			case DesignPatternsPackage.AUTOMATIC_BINDING: return createAutomaticBinding();
			case DesignPatternsPackage.PATTERN_APPLICATION: return createPatternApplication();
			case DesignPatternsPackage.PATTERN_SYSTEM: return createPatternSystem();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DesignPatternsPackage.CONSEQUENCE_KIND:
				return createConsequenceKindFromString(eDataType, initialValue);
			case DesignPatternsPackage.SAFETY_LEVEL:
				return createSafetyLevelFromString(eDataType, initialValue);
			case DesignPatternsPackage.CLASSIFICATION:
				return createClassificationFromString(eDataType, initialValue);
			case DesignPatternsPackage.REFERENCE_LEVEL:
				return createReferenceLevelFromString(eDataType, initialValue);
			case DesignPatternsPackage.REPLICATION_KIND:
				return createReplicationKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DesignPatternsPackage.CONSEQUENCE_KIND:
				return convertConsequenceKindToString(eDataType, instanceValue);
			case DesignPatternsPackage.SAFETY_LEVEL:
				return convertSafetyLevelToString(eDataType, instanceValue);
			case DesignPatternsPackage.CLASSIFICATION:
				return convertClassificationToString(eDataType, instanceValue);
			case DesignPatternsPackage.REFERENCE_LEVEL:
				return convertReferenceLevelToString(eDataType, instanceValue);
			case DesignPatternsPackage.REPLICATION_KIND:
				return convertReplicationKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Context createContext() {
		ContextImpl context = new ContextImpl();
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Applicability createApplicability() {
		ApplicabilityImpl applicability = new ApplicabilityImpl();
		return applicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Intent createIntent() {
		IntentImpl intent = new IntentImpl();
		return intent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionDesc createSolutionDesc() {
		SolutionDescImpl solutionDesc = new SolutionDescImpl();
		return solutionDesc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Problem createProblem() {
		ProblemImpl problem = new ProblemImpl();
		return problem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProblemKind createProblemKind() {
		ProblemKindImpl problemKind = new ProblemKindImpl();
		return problemKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Solution createSolution() {
		SolutionImpl solution = new SolutionImpl();
		return solution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Consequence createConsequence() {
		ConsequenceImpl consequence = new ConsequenceImpl();
		return consequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SampleCode createSampleCode() {
		SampleCodeImpl sampleCode = new SampleCodeImpl();
		return sampleCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Pattern createPattern() {
		PatternImpl pattern = new PatternImpl();
		return pattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KnownUse createKnownUse() {
		KnownUseImpl knownUse = new KnownUseImpl();
		return knownUse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Replication createReplication() {
		ReplicationImpl replication = new ReplicationImpl();
		return replication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomaticBinding createAutomaticBinding() {
		AutomaticBindingImpl automaticBinding = new AutomaticBindingImpl();
		return automaticBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternApplication createPatternApplication() {
		PatternApplicationImpl patternApplication = new PatternApplicationImpl();
		return patternApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternSystem createPatternSystem() {
		PatternSystemImpl patternSystem = new PatternSystemImpl();
		return patternSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConsequenceKind createConsequenceKindFromString(EDataType eDataType, String initialValue) {
		ConsequenceKind result = ConsequenceKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConsequenceKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyLevel createSafetyLevelFromString(EDataType eDataType, String initialValue) {
		SafetyLevel result = SafetyLevel.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSafetyLevelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classification createClassificationFromString(EDataType eDataType, String initialValue) {
		Classification result = Classification.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertClassificationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceLevel createReferenceLevelFromString(EDataType eDataType, String initialValue) {
		ReferenceLevel result = ReferenceLevel.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertReferenceLevelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReplicationKind createReplicationKindFromString(EDataType eDataType, String initialValue) {
		ReplicationKind result = ReplicationKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertReplicationKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsPackage getDesignPatternsPackage() {
		return (DesignPatternsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DesignPatternsPackage getPackage() {
		return DesignPatternsPackage.eINSTANCE;
	}

} //DesignPatternsFactoryImpl
