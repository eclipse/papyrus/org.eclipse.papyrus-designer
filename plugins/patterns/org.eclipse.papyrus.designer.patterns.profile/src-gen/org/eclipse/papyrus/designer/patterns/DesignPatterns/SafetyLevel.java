/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Safety Level</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSafetyLevel()
 * @model
 * @generated
 */
public enum SafetyLevel implements Enumerator {
	/**
	 * The '<em><b>SIL0</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIL0_VALUE
	 * @generated
	 * @ordered
	 */
	SIL0(0, "SIL0", "SIL0"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SIL1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIL1_VALUE
	 * @generated
	 * @ordered
	 */
	SIL1(1, "SIL1", "SIL1"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SIL2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIL2_VALUE
	 * @generated
	 * @ordered
	 */
	SIL2(2, "SIL2", "SIL2"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SIL3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIL3_VALUE
	 * @generated
	 * @ordered
	 */
	SIL3(3, "SIL3", "SIL3"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SIL4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIL4_VALUE
	 * @generated
	 * @ordered
	 */
	SIL4(4, "SIL4", "SIL4"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>SIL0</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIL0</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIL0
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIL0_VALUE = 0;

	/**
	 * The '<em><b>SIL1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIL1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIL1
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIL1_VALUE = 1;

	/**
	 * The '<em><b>SIL2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIL2</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIL2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIL2_VALUE = 2;

	/**
	 * The '<em><b>SIL3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIL3</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIL3
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIL3_VALUE = 3;

	/**
	 * The '<em><b>SIL4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIL4</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIL4
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIL4_VALUE = 4;

	/**
	 * An array of all the '<em><b>Safety Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SafetyLevel[] VALUES_ARRAY =
		new SafetyLevel[] {
			SIL0,
			SIL1,
			SIL2,
			SIL3,
			SIL4,
		};

	/**
	 * A public read-only list of all the '<em><b>Safety Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SafetyLevel> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Safety Level</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SafetyLevel get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SafetyLevel result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Safety Level</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SafetyLevel getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SafetyLevel result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Safety Level</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SafetyLevel get(int value) {
		switch (value) {
			case SIL0_VALUE: return SIL0;
			case SIL1_VALUE: return SIL1;
			case SIL2_VALUE: return SIL2;
			case SIL3_VALUE: return SIL3;
			case SIL4_VALUE: return SIL4;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SafetyLevel(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SafetyLevel
