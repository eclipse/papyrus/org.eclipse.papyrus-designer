/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Replication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Constraint <em>Base Constraint</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getInitialNumberOfReplicas <em>Initial Number Of Replicas</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getMinNumberOfReplicas <em>Min Number Of Replicas</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Property <em>Base Property</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication()
 * @model
 * @generated
 */
public interface Replication extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Constraint</em>' reference.
	 * @see #setBase_Constraint(Constraint)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication_Base_Constraint()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Constraint getBase_Constraint();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Constraint <em>Base Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Constraint</em>' reference.
	 * @see #getBase_Constraint()
	 * @generated
	 */
	void setBase_Constraint(Constraint value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind
	 * @see #setKind(ReplicationKind)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication_Kind()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ReplicationKind getKind();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(ReplicationKind value);

	/**
	 * Returns the value of the '<em><b>Initial Number Of Replicas</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Number Of Replicas</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Number Of Replicas</em>' attribute.
	 * @see #setInitialNumberOfReplicas(int)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication_InitialNumberOfReplicas()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getInitialNumberOfReplicas();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getInitialNumberOfReplicas <em>Initial Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Number Of Replicas</em>' attribute.
	 * @see #getInitialNumberOfReplicas()
	 * @generated
	 */
	void setInitialNumberOfReplicas(int value);

	/**
	 * Returns the value of the '<em><b>Min Number Of Replicas</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Number Of Replicas</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Number Of Replicas</em>' attribute.
	 * @see #setMinNumberOfReplicas(int)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication_MinNumberOfReplicas()
	 * @model dataType="org.eclipse.uml2.types.Integer" required="true" ordered="false"
	 * @generated
	 */
	int getMinNumberOfReplicas();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getMinNumberOfReplicas <em>Min Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Number Of Replicas</em>' attribute.
	 * @see #getMinNumberOfReplicas()
	 * @generated
	 */
	void setMinNumberOfReplicas(int value);

	/**
	 * Returns the value of the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Property</em>' reference.
	 * @see #setBase_Property(Property)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplication_Base_Property()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Property getBase_Property();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Property <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Property</em>' reference.
	 * @see #getBase_Property()
	 * @generated
	 */
	void setBase_Property(Property value);

} // Replication
