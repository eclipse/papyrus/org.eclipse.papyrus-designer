/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.CollaborationUse;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_CollaborationUse <em>Base Collaboration Use</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getUser <em>User</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_Class <em>Base Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getPatternApplication()
 * @model
 * @generated
 */
public interface PatternApplication extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Collaboration Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Collaboration Use</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Collaboration Use</em>' reference.
	 * @see #setBase_CollaborationUse(CollaborationUse)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getPatternApplication_Base_CollaborationUse()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CollaborationUse getBase_CollaborationUse();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_CollaborationUse <em>Base Collaboration Use</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Collaboration Use</em>' reference.
	 * @see #getBase_CollaborationUse()
	 * @generated
	 */
	void setBase_CollaborationUse(CollaborationUse value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(String)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getPatternApplication_Date()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getDate();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(String value);

	/**
	 * Returns the value of the '<em><b>User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' attribute.
	 * @see #setUser(String)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getPatternApplication_User()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getUser();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getUser <em>User</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' attribute.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(String value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getPatternApplication_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

} // PatternApplication
