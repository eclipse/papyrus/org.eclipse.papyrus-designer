/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.profile.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;


public class PatternUtils {

	public static URI DP_URI = URI.createURI("http://www.eclipse.org/papyrus/DP/1"); //$NON-NLS-1$
	
	public static URI DP_PATH = URI.createURI("pathmap://DPATTERN_PROFILE/DesignPatterns.profile.uml"); //$NON-NLS-1$
	
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getOwnedCommentWithStereotype(Package owner, java.lang.Class<T> clazz) {
		if (owner != null) {
			for (Comment comment : owner.getOwnedComments()) {
				for (EObject eObj : comment.getStereotypeApplications()) {
					// check whether the stereotype is an instance of the passed parameter clazz
					if (clazz.isInstance(eObj)) {
						return (T) eObj;
					}
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T extends EObject> EList<T> getOwnedCommentsWithStereotype(Package owner, java.lang.Class<T> clazz) {
		// TODO: The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		EList<T> list = new BasicEList<T>();
		for (Comment comment : owner.getOwnedComments()) {
			for (EObject eObj : comment.getStereotypeApplications()) {
				// check whether the stereotype is an instance of the passed parameter clazz
				if (clazz.isInstance(eObj)) {
					list.add((T) eObj);
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getPackagedElementWithStereotype(Package owner, java.lang.Class<T> clazz) {
		for (PackageableElement element : owner.getPackagedElements()) {
			for (EObject eObj : element.getStereotypeApplications()) {
				// check whether the stereotype is an instance of the passed parameter clazz
				if (clazz.isInstance(eObj)) {
					return (T) eObj;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T extends EObject> EList<T> getPackagedElementsWithStereotype(Package owner, java.lang.Class<T> clazz) {
		EList<T> list = new BasicEList<T>();
		for (PackageableElement element : owner.getPackagedElements()) {
			for (EObject eObj : element.getStereotypeApplications()) {
				// check whether the stereotype is an instance of the passed parameter clazz
				if (clazz.isInstance(eObj)) {
					list.add((T) eObj);
				}
			}
		}
		return list;
	}

	public static Collaboration getCollaboration(Pattern pattern) {
		for (Element el : pattern.getBase_Package().getPackagedElements()) {
			if (el instanceof Collaboration) {
				return (Collaboration) el;
			}
		}
		return null;
	}

	public static EList<ConnectableElement> getRoles(Pattern pattern) {
		Collaboration collaboration = getCollaboration(pattern);
		if (collaboration != null) {
			return collaboration.getRoles();
		}
		return null;
	}
	
	public static Classifier getCUClassifier(Solution solution) {
		for (Element el : solution.getBase_Package().getPackagedElements()) {
			if (el instanceof Classifier) {
				Classifier cl = (Classifier) el;
				if (cl.getCollaborationUses().size() > 0) {
					return cl;
				}
			}
		}
		return null;
	}
	
	public static EList<NamedElement> getSolutionElements(Solution solution) {
		Classifier cl = getCUClassifier(solution);
		if (cl != null) {
			// if CU classifier != null, it will contain at least one collaboration use
			CollaborationUse cu = cl.getCollaborationUses().iterator().next();
			return getSolutionElements(cu);
		}
		return new BasicEList<NamedElement>();
	}
	
	public static EList<NamedElement> getSolutionElements(CollaborationUse cu) {
		EList<NamedElement> elements = new BasicEList<NamedElement>();
		for (Dependency d : cu.getRoleBindings()) {
			NamedElement client = d.getClients().iterator().next();
			if (client != null) {
				elements.add(client);
			}
		}
		return elements;
	}
}
