/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaStep;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StepSaStepOpaqueActionAdvice extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				OpaqueAction element = (OpaqueAction) request.getElementToConfigure();
				element.setName(element.getName().replaceAll("SaStep", "Step"));
				element.setName(element.getName().replaceAll("OpaqueAction", "Step"));
				SaStep saStep = UMLUtil.getStereotypeApplication(element, SaStep.class);
				if (saStep != null) {
					for (ActivityPartition activityPartition : element.getInPartitions()) {
						SaStep saStepActivityPartition = UMLUtil.getStereotypeApplication(activityPartition, SaStep.class);
						if (saStepActivityPartition != null) {
							if (saStepActivityPartition.getConcurRes() != null) {
								saStep.setConcurRes(saStepActivityPartition.getConcurRes());
							}
							if (saStepActivityPartition.getHost() != null) {
								saStep.setHost(saStepActivityPartition.getHost());
							}
							if (saStepActivityPartition.getPriority() != null) {
								saStep.setPriority(saStepActivityPartition.getPriority());
							}
							break;
						}
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}
}
