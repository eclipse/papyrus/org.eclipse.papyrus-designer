/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import java.util.Collections;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaExecHost;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaStep;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaStep;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SwSchedulableResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.SchedulableResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Scheduler;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class TaskSaStepActivityPartitionAdvice extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				ActivityPartition element = (ActivityPartition) request.getElementToConfigure();
				element.setName(element.getName().replaceAll("SaStep", "Task"));
				element.setName(element.getName().replaceAll("ActivityPartition", "Task"));
				SaStep saStep = UMLUtil.getStereotypeApplication(element, SaStep.class);
				if (saStep != null) {
					saStep.setPriority("1");
					Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
					if (platformResourcesPackage != null) {
						org.eclipse.uml2.uml.Class taskResource = platformResourcesPackage.createOwnedClass(element.getName() + "_" + element.hashCode(), false);
						if (taskResource != null) {
							StereotypeUtil.apply(taskResource, SwSchedulableResource.class);
							SwSchedulableResource swSchedulableResource = UMLUtil.getStereotypeApplication(taskResource, SwSchedulableResource.class);
							if (swSchedulableResource != null) {
								saStep.setConcurRes(swSchedulableResource);
								Element owner = element.getOwner();
								if (owner instanceof ActivityPartition) {
									GaStep gaStep = UMLUtil.getStereotypeApplication(owner, GaStep.class);
									if (gaStep != null) {
										GaExecHost gaExecHost = gaStep.getHost();
										if (gaExecHost != null) {
											saStep.setHost(gaExecHost);
											Scheduler scheduler = gaExecHost.getMainScheduler();
											if (scheduler != null) {
												swSchedulableResource.setHost(gaExecHost);
												scheduler.getSchedulableResources().add(swSchedulableResource);
												Property priorityProperty = taskResource.createOwnedAttribute("priority", null);
												if (priorityProperty != null) {
													priorityProperty.setIntegerDefaultValue(1);
													swSchedulableResource.getPriorityElements().add(priorityProperty);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}

	@Override
	protected ICommand getBeforeDestroyDependentsCommand(DestroyDependentsRequest request) {
		ActivityPartition element = (ActivityPartition) request.getElementToDestroy();
		Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
		SaStep saStep = UMLUtil.getStereotypeApplication(element, SaStep.class);
		if (saStep != null) {
			SchedulableResource schedulableResource = saStep.getConcurRes();
			if (schedulableResource != null) {
				Classifier baseSchedulableResourceClassifier = schedulableResource.getBase_Classifier();				
				if (platformResourcesPackage != null) {
					ICommand configureCommand = new AbstractTransactionalCommand(request.getEditingDomain(), "Destroy host and its main scheduler", Collections.EMPTY_LIST) {
						@Override
						protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
							for (Element ownedElement : platformResourcesPackage.getOwnedElements()) {
								if (baseSchedulableResourceClassifier != null && ownedElement == baseSchedulableResourceClassifier) {
									baseSchedulableResourceClassifier.destroy();
									break;
								}
							}
							return CommandResult.newOKCommandResult(element);
						}
					};
					return CompositeCommand.compose(configureCommand, super.getBeforeDestroyDependentsCommand(request));
				}
			}
		}
		return super.getBeforeDestroyDependentsCommand(request);
	}
}
