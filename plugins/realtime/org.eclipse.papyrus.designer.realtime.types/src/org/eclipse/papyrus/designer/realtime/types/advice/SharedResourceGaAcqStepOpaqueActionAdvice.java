/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaAcqStep;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.ConcurrentAccessProtocolKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SwMutualExclusionResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class SharedResourceGaAcqStepOpaqueActionAdvice extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				OpaqueAction element = (OpaqueAction) request.getElementToConfigure();
				element.setName(element.getName().replaceAll("GaAcqStep", "SharedResource"));
				element.setName(element.getName().replaceAll("OpaqueAction", "SharedResource"));
				GaAcqStep gaAcqStep = UMLUtil.getStereotypeApplication(element, GaAcqStep.class);
				Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
				if (platformResourcesPackage != null) {
					org.eclipse.uml2.uml.Class sharedResource = platformResourcesPackage.createOwnedClass(element.getName() + "_" + element.hashCode(), false);
					if (sharedResource != null) {
						StereotypeUtil.apply(sharedResource, SwMutualExclusionResource.class);
						SwMutualExclusionResource swMutualExclusionResource = UMLUtil.getStereotypeApplication(sharedResource, SwMutualExclusionResource.class);
						if (swMutualExclusionResource != null) {
							swMutualExclusionResource.setConcurrentAccessProtocol(ConcurrentAccessProtocolKind.PIP);
							if (gaAcqStep != null) {
								gaAcqStep.setAcqRes(swMutualExclusionResource);
							}
						}
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}

	@Override
	protected ICommand getBeforeDestroyDependentsCommand(DestroyDependentsRequest request) {
		OpaqueAction element = (OpaqueAction) request.getElementToDestroy();
		Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
		GaAcqStep gaAcqStep = UMLUtil.getStereotypeApplication(element, GaAcqStep.class);
		if (gaAcqStep != null) {
			List<Resource> usedResources = new ArrayList<Resource>();
			for (Resource resource : gaAcqStep.getUsedResources()) {
				usedResources.add(resource);
			}
			if (usedResources != null && !usedResources.isEmpty()) {
				ICommand configureCommand = new AbstractTransactionalCommand(request.getEditingDomain(), "Destroy used resource", Collections.EMPTY_LIST) {
					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
						for (Resource resource : usedResources) {
							if (resource instanceof SwMutualExclusionResource) {
								Classifier baseResourceClassifier = resource.getBase_Classifier();
								if (platformResourcesPackage != null) {
									for (Element ownedElement : platformResourcesPackage.getOwnedElements()) {
										if (baseResourceClassifier != null && ownedElement == baseResourceClassifier) {
											baseResourceClassifier.destroy();
											break;
										}
									}
								}
							}
						}
						return CommandResult.newOKCommandResult(element);
					}
				};
				return CompositeCommand.compose(configureCommand, super.getBeforeDestroyDependentsCommand(request));
			}
		}
		return super.getBeforeDestroyDependentsCommand(request);
	}
}
