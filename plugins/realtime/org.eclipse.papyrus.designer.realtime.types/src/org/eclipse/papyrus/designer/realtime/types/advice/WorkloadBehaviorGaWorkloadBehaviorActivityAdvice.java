/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaWorkloadBehavior;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaAnalysisContext;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorkloadBehaviorGaWorkloadBehaviorActivityAdvice extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				Activity element = (Activity) request.getElementToConfigure();
				element.setName(element.getName().replaceAll("GaWorkloadBehavior", "WorkloadBehavior"));
				element.setName(element.getName().replaceAll("Activity", "WorkloadBehavior"));
				GaWorkloadBehavior gaWorkloadBehavior = UMLUtil.getStereotypeApplication(element, GaWorkloadBehavior.class);
				if (gaWorkloadBehavior != null) {
					Element owner = element.getOwner();
					if (owner instanceof Package) {
						SaAnalysisContext saAnalysisContextStep = UMLUtil.getStereotypeApplication(owner, SaAnalysisContext.class);
						if (saAnalysisContextStep != null) {
							saAnalysisContextStep.getWorkload().add(gaWorkloadBehavior);
						}
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}
}
