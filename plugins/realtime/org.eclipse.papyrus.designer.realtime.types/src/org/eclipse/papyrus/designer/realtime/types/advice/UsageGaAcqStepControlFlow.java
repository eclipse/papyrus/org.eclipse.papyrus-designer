/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import java.util.Collections;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaAcqStep;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaStep;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.util.UMLUtil;

public class UsageGaAcqStepControlFlow extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				ControlFlow element = (ControlFlow) request.getElementToConfigure();
				ActivityNode sourceNode = element.getSource();
				ActivityNode targetNode = element.getTarget();
				if (sourceNode != null && targetNode != null) {
					SaStep saStep = UMLUtil.getStereotypeApplication(sourceNode, SaStep.class); 
					GaAcqStep gaAcqStep = UMLUtil.getStereotypeApplication(targetNode, GaAcqStep.class);
					if (saStep != null && gaAcqStep != null && !saStep.getSubUsage().contains(gaAcqStep)) {
						saStep.getSubUsage().add(gaAcqStep);
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}

	@Override
	protected ICommand getBeforeDestroyDependentsCommand(DestroyDependentsRequest request) {
		ControlFlow element = (ControlFlow) request.getElementToDestroy();
		ActivityNode sourceNode = element.getSource();
		ActivityNode targetNode = element.getTarget();
		if (sourceNode != null && targetNode != null) {
			SaStep saStep = UMLUtil.getStereotypeApplication(sourceNode, SaStep.class); 
			GaAcqStep gaAcqStep = UMLUtil.getStereotypeApplication(targetNode, GaAcqStep.class);
			if (saStep != null && gaAcqStep != null && saStep.getSubUsage().contains(gaAcqStep)) {
				ICommand configureCommand = new AbstractTransactionalCommand(request.getEditingDomain(), "Destroy subusage", Collections.EMPTY_LIST) {
					@Override
					protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
						boolean isRelated = false;
						for (ActivityEdge outgoingEdge : sourceNode.getOutgoings()) {
							if (outgoingEdge != element && outgoingEdge.getTarget() == targetNode && UMLUtil.getStereotypeApplication(outgoingEdge, GaAcqStep.class) != null) {
								isRelated = true;
								break;
							}
						}
						if (!isRelated) {
							saStep.getSubUsage().remove(gaAcqStep);
						}
						return CommandResult.newOKCommandResult(element);
					}
				};
				return CompositeCommand.compose(configureCommand, super.getBeforeDestroyDependentsCommand(request));
			}
		}
		return super.getBeforeDestroyDependentsCommand(request);
	}
}
