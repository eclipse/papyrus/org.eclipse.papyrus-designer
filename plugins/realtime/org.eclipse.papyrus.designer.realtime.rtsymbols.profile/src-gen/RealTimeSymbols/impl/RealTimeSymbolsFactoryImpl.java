/**
 */
package RealTimeSymbols.impl;

import RealTimeSymbols.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RealTimeSymbolsFactoryImpl extends EFactoryImpl implements RealTimeSymbolsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RealTimeSymbolsFactory init() {
		try {
			RealTimeSymbolsFactory theRealTimeSymbolsFactory = (RealTimeSymbolsFactory)EPackage.Registry.INSTANCE.getEFactory(RealTimeSymbolsPackage.eNS_URI);
			if (theRealTimeSymbolsFactory != null) {
				return theRealTimeSymbolsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RealTimeSymbolsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeSymbolsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RealTimeSymbolsPackage.SYMBOL_COMPUTING_RESOURCE: return createSymbolComputingResource();
			case RealTimeSymbolsPackage.SYMBOL_SCHEDULABLE_RESOURCE: return createSymbolSchedulableResource();
			case RealTimeSymbolsPackage.SYMBOL_DELAY: return createSymbolDelay();
			case RealTimeSymbolsPackage.SYMBOL_WORKLOAD_EVENT: return createSymbolWorkloadEvent();
			case RealTimeSymbolsPackage.SYMBOL_END_TO_END_FLOW: return createSymbolEndToEndFlow();
			case RealTimeSymbolsPackage.SYMBOL_MUTUAL_EXCLUSION_RESOURCE: return createSymbolMutualExclusionResource();
			case RealTimeSymbolsPackage.SYMBOL_REAL_TIME_OBSERVATION: return createSymbolRealTimeObservation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolComputingResource createSymbolComputingResource() {
		SymbolComputingResourceImpl symbolComputingResource = new SymbolComputingResourceImpl();
		return symbolComputingResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolSchedulableResource createSymbolSchedulableResource() {
		SymbolSchedulableResourceImpl symbolSchedulableResource = new SymbolSchedulableResourceImpl();
		return symbolSchedulableResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolDelay createSymbolDelay() {
		SymbolDelayImpl symbolDelay = new SymbolDelayImpl();
		return symbolDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolWorkloadEvent createSymbolWorkloadEvent() {
		SymbolWorkloadEventImpl symbolWorkloadEvent = new SymbolWorkloadEventImpl();
		return symbolWorkloadEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolEndToEndFlow createSymbolEndToEndFlow() {
		SymbolEndToEndFlowImpl symbolEndToEndFlow = new SymbolEndToEndFlowImpl();
		return symbolEndToEndFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolMutualExclusionResource createSymbolMutualExclusionResource() {
		SymbolMutualExclusionResourceImpl symbolMutualExclusionResource = new SymbolMutualExclusionResourceImpl();
		return symbolMutualExclusionResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SymbolRealTimeObservation createSymbolRealTimeObservation() {
		SymbolRealTimeObservationImpl symbolRealTimeObservation = new SymbolRealTimeObservationImpl();
		return symbolRealTimeObservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeSymbolsPackage getRealTimeSymbolsPackage() {
		return (RealTimeSymbolsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RealTimeSymbolsPackage getPackage() {
		return RealTimeSymbolsPackage.eINSTANCE;
	}

} //RealTimeSymbolsFactoryImpl
