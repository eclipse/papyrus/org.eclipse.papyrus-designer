/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Connectables Ordered Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConnectablesOrderedSetTest extends PyCPAElementTest {

	/**
	 * Constructs a new Connectables Ordered Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectablesOrderedSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Connectables Ordered Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ConnectablesOrderedSet getFixture() {
		return (ConnectablesOrderedSet)fixture;
	}

} //ConnectablesOrderedSetTest
