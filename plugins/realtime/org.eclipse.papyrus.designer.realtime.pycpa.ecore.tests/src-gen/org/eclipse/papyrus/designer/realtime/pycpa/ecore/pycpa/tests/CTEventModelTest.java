/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import junit.textui.TestRunner;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CT Event Model</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CTEventModelTest extends EventModelTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CTEventModelTest.class);
	}

	/**
	 * Constructs a new CT Event Model test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CTEventModelTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CT Event Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CTEventModel getFixture() {
		return (CTEventModel)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(PyCPAFactory.eINSTANCE.createCTEventModel());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CTEventModelTest
