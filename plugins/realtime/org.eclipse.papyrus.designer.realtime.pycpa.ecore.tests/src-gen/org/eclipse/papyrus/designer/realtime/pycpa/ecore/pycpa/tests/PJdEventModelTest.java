/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import junit.textui.TestRunner;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>PJd Event Model</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PJdEventModelTest extends EventModelTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PJdEventModelTest.class);
	}

	/**
	 * Constructs a new PJd Event Model test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PJdEventModelTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this PJd Event Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PJdEventModel getFixture() {
		return (PJdEventModel)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(PyCPAFactory.eINSTANCE.createPJdEventModel());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PJdEventModelTest
