/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Connectable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConnectableElementTest extends PyCPAElementTest {

	/**
	 * Constructs a new Connectable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Connectable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ConnectableElement getFixture() {
		return (ConnectableElement)fixture;
	}

} //ConnectableElementTest
