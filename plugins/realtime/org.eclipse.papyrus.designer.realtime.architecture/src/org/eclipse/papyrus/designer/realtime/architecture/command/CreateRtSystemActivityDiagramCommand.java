/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.designer.realtime.architecture.command;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaWorkloadBehavior;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaAnalysisContext;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.DiagramPrototype;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramUtils;
import org.eclipse.papyrus.uml.diagram.activity.CreateActivityDiagramCommand;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CreateRtSystemActivityDiagramCommand extends CreateActivityDiagramCommand {
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Diagram doCreateDiagram(Resource diagramResource, EObject owner, EObject element, DiagramPrototype prototype, String name) {
		Diagram diagram = null;
		if (element instanceof org.eclipse.uml2.uml.Package) {
			diagram = ViewService.createDiagram(element, getDiagramNotationID(), getPreferenceHint());
		} else if (element instanceof BehavioredClassifier) {
			diagram = ViewService.createDiagram(((BehavioredClassifier) element).getNearestPackage(), getDiagramNotationID(), getPreferenceHint());
		}
		// create diagram
		if (diagram != null) {
			if (element instanceof Activity) {
				Activity activity = (Activity) element;
				StereotypeUtil.apply(activity, GaWorkloadBehavior.class);
				activity.setName(activity.getName().replaceAll("GaWorkloadBehavior", "WorkloadBehavior"));
				activity.setName(activity.getName().replaceAll("Activity", "WorkloadBehavior"));
				activity.setName(activity.getName().replaceAll("SaAnalysisContext", "WorkloadBehavior"));
				GaWorkloadBehavior gaWorkloadBehavior = UMLUtil.getStereotypeApplication(activity, GaWorkloadBehavior.class);
				if (gaWorkloadBehavior != null) {
					Element ownerPackage = activity.getOwner();
					if (ownerPackage instanceof Package) {
						SaAnalysisContext saAnalysisContextStep = UMLUtil.getStereotypeApplication(ownerPackage, SaAnalysisContext.class);
						if (saAnalysisContextStep != null) {
							saAnalysisContextStep.getWorkload().add(gaWorkloadBehavior);
						}
					}
				}
			}

			setName(name);
			diagram.setElement(element);
			DiagramUtils.setOwner(diagram, owner);
			DiagramUtils.setPrototype(diagram, prototype);
			initializeModel(element);
			initializeDiagram(diagram);
			diagramResource.getContents().add(diagram);
		}
		return diagram;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDefaultDiagramName() {
		return "Real-time system diagram"; //$NON-NLS-1$
	}
}
