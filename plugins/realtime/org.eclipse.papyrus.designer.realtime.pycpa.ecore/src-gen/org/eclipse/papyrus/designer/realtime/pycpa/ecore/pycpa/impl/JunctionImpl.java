/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Junction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl#getStrategy <em>Strategy</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl#getPrev_tasks <em>Prev tasks</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl#getNext_task <em>Next task</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JunctionImpl extends ConnectableElementImpl implements Junction {
	/**
	 * The default value of the '{@link #getStrategy() <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategy()
	 * @generated
	 * @ordered
	 */
	protected static final JunctionStrategyKind STRATEGY_EDEFAULT = JunctionStrategyKind.AND;

	/**
	 * The cached value of the '{@link #getStrategy() <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategy()
	 * @generated
	 * @ordered
	 */
	protected JunctionStrategyKind strategy = STRATEGY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPrev_tasks() <em>Prev tasks</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrev_tasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> prev_tasks;

	/**
	 * The cached value of the '{@link #getNext_task() <em>Next task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNext_task()
	 * @generated
	 * @ordered
	 */
	protected Task next_task;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.JUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JunctionStrategyKind getStrategy() {
		return strategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategy(JunctionStrategyKind newStrategy) {
		JunctionStrategyKind oldStrategy = strategy;
		strategy = newStrategy == null ? STRATEGY_EDEFAULT : newStrategy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.JUNCTION__STRATEGY, oldStrategy, strategy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getPrev_tasks() {
		if (prev_tasks == null) {
			prev_tasks = new EObjectResolvingEList<Task>(Task.class, this, PyCPAPackage.JUNCTION__PREV_TASKS);
		}
		return prev_tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Task getNext_task() {
		if (next_task != null && next_task.eIsProxy()) {
			InternalEObject oldNext_task = (InternalEObject)next_task;
			next_task = (Task)eResolveProxy(oldNext_task);
			if (next_task != oldNext_task) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PyCPAPackage.JUNCTION__NEXT_TASK, oldNext_task, next_task));
			}
		}
		return next_task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Task basicGetNext_task() {
		return next_task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNext_task(Task newNext_task) {
		Task oldNext_task = next_task;
		next_task = newNext_task;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.JUNCTION__NEXT_TASK, oldNext_task, next_task));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PyCPAPackage.JUNCTION__STRATEGY:
				return getStrategy();
			case PyCPAPackage.JUNCTION__PREV_TASKS:
				return getPrev_tasks();
			case PyCPAPackage.JUNCTION__NEXT_TASK:
				if (resolve) return getNext_task();
				return basicGetNext_task();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PyCPAPackage.JUNCTION__STRATEGY:
				setStrategy((JunctionStrategyKind)newValue);
				return;
			case PyCPAPackage.JUNCTION__PREV_TASKS:
				getPrev_tasks().clear();
				getPrev_tasks().addAll((Collection<? extends Task>)newValue);
				return;
			case PyCPAPackage.JUNCTION__NEXT_TASK:
				setNext_task((Task)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PyCPAPackage.JUNCTION__STRATEGY:
				setStrategy(STRATEGY_EDEFAULT);
				return;
			case PyCPAPackage.JUNCTION__PREV_TASKS:
				getPrev_tasks().clear();
				return;
			case PyCPAPackage.JUNCTION__NEXT_TASK:
				setNext_task((Task)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PyCPAPackage.JUNCTION__STRATEGY:
				return strategy != STRATEGY_EDEFAULT;
			case PyCPAPackage.JUNCTION__PREV_TASKS:
				return prev_tasks != null && !prev_tasks.isEmpty();
			case PyCPAPackage.JUNCTION__NEXT_TASK:
				return next_task != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (strategy: ");
		result.append(strategy);
		result.append(')');
		return result.toString();
	}

} //JunctionImpl
