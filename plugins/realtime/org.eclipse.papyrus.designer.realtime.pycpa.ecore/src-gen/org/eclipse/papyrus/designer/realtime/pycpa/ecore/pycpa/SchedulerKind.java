/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Scheduler Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSchedulerKind()
 * @model
 * @generated
 */
public enum SchedulerKind implements Enumerator {
	/**
	 * The '<em><b>ROUND ROBIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROUND_ROBIN_VALUE
	 * @generated
	 * @ordered
	 */
	ROUND_ROBIN(0, "ROUND_ROBIN", "ROUND_ROBIN"),

	/**
	 * The '<em><b>SP NP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SP_NP_VALUE
	 * @generated
	 * @ordered
	 */
	SP_NP(1, "SP_NP", "SP_NP"),

	/**
	 * The '<em><b>SP P</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SP_P_VALUE
	 * @generated
	 * @ordered
	 */
	SP_P(2, "SP_P", "SP_P"),

	/**
	 * The '<em><b>SP PACTIVOFFSETS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SP_PACTIVOFFSETS_VALUE
	 * @generated
	 * @ordered
	 */
	SP_PACTIVOFFSETS(3, "SP_P_ACTIVOFFSETS", "SP_P_ACTIVOFFSETS");

	/**
	 * The '<em><b>ROUND ROBIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ROUND ROBIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROUND_ROBIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROUND_ROBIN_VALUE = 0;

	/**
	 * The '<em><b>SP NP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SP NP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SP_NP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SP_NP_VALUE = 1;

	/**
	 * The '<em><b>SP P</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SP P</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SP_P
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SP_P_VALUE = 2;

	/**
	 * The '<em><b>SP PACTIVOFFSETS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SP PACTIVOFFSETS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SP_PACTIVOFFSETS
	 * @model name="SP_P_ACTIVOFFSETS"
	 * @generated
	 * @ordered
	 */
	public static final int SP_PACTIVOFFSETS_VALUE = 3;

	/**
	 * An array of all the '<em><b>Scheduler Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SchedulerKind[] VALUES_ARRAY =
		new SchedulerKind[] {
			ROUND_ROBIN,
			SP_NP,
			SP_P,
			SP_PACTIVOFFSETS,
		};

	/**
	 * A public read-only list of all the '<em><b>Scheduler Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SchedulerKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Scheduler Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SchedulerKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SchedulerKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Scheduler Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SchedulerKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SchedulerKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Scheduler Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SchedulerKind get(int value) {
		switch (value) {
			case ROUND_ROBIN_VALUE: return ROUND_ROBIN;
			case SP_NP_VALUE: return SP_NP;
			case SP_P_VALUE: return SP_P;
			case SP_PACTIVOFFSETS_VALUE: return SP_PACTIVOFFSETS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SchedulerKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SchedulerKind
