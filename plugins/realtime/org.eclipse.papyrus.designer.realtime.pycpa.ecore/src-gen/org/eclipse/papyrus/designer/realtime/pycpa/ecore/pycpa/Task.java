/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getWcet <em>Wcet</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getBcet <em>Bcet</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getScheduling_parameter <em>Scheduling parameter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getIn_eventmodel <em>In eventmodel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getPrecedence_deps <em>Precedence deps</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getData_deps <em>Data deps</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends ConnectableElement {
	/**
	 * Returns the value of the '<em><b>Wcet</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wcet</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wcet</em>' attribute.
	 * @see #setWcet(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Wcet()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getWcet();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getWcet <em>Wcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wcet</em>' attribute.
	 * @see #getWcet()
	 * @generated
	 */
	void setWcet(int value);

	/**
	 * Returns the value of the '<em><b>Bcet</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bcet</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bcet</em>' attribute.
	 * @see #setBcet(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Bcet()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getBcet();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getBcet <em>Bcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bcet</em>' attribute.
	 * @see #getBcet()
	 * @generated
	 */
	void setBcet(int value);

	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Deadline()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getDeadline();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(int value);

	/**
	 * Returns the value of the '<em><b>Scheduling parameter</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling parameter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling parameter</em>' attribute.
	 * @see #setScheduling_parameter(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Scheduling_parameter()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getScheduling_parameter();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getScheduling_parameter <em>Scheduling parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduling parameter</em>' attribute.
	 * @see #getScheduling_parameter()
	 * @generated
	 */
	void setScheduling_parameter(int value);

	/**
	 * Returns the value of the '<em><b>In eventmodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In eventmodel</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In eventmodel</em>' containment reference.
	 * @see #setIn_eventmodel(EventModel)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_In_eventmodel()
	 * @model containment="true"
	 * @generated
	 */
	EventModel getIn_eventmodel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getIn_eventmodel <em>In eventmodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In eventmodel</em>' containment reference.
	 * @see #getIn_eventmodel()
	 * @generated
	 */
	void setIn_eventmodel(EventModel value);

	/**
	 * Returns the value of the '<em><b>Precedence deps</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precedence deps</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precedence deps</em>' reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Precedence_deps()
	 * @model
	 * @generated
	 */
	EList<ConnectableElement> getPrecedence_deps();

	/**
	 * Returns the value of the '<em><b>Data deps</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data deps</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data deps</em>' reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getTask_Data_deps()
	 * @model
	 * @generated
	 */
	EList<ConnectableElement> getData_deps();

} // Task
