/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Effect Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getEffectChain()
 * @model
 * @generated
 */
public interface EffectChain extends ConnectablesOrderedSet {

} // EffectChain
