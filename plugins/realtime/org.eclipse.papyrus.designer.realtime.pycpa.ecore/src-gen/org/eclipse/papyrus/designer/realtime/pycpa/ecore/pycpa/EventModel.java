/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getEventModel()
 * @model abstract="true"
 * @generated
 */
public interface EventModel extends PyCPAElement {
} // EventModel
