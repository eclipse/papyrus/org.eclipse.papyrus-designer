/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Junction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getStrategy <em>Strategy</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getPrev_tasks <em>Prev tasks</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getNext_task <em>Next task</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getJunction()
 * @model
 * @generated
 */
public interface Junction extends ConnectableElement {
	/**
	 * Returns the value of the '<em><b>Strategy</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategy</em>' attribute.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind
	 * @see #setStrategy(JunctionStrategyKind)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getJunction_Strategy()
	 * @model required="true"
	 * @generated
	 */
	JunctionStrategyKind getStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getStrategy <em>Strategy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategy</em>' attribute.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind
	 * @see #getStrategy()
	 * @generated
	 */
	void setStrategy(JunctionStrategyKind value);

	/**
	 * Returns the value of the '<em><b>Prev tasks</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prev tasks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prev tasks</em>' reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getJunction_Prev_tasks()
	 * @model required="true"
	 * @generated
	 */
	EList<Task> getPrev_tasks();

	/**
	 * Returns the value of the '<em><b>Next task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next task</em>' reference.
	 * @see #setNext_task(Task)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getJunction_Next_task()
	 * @model required="true"
	 * @generated
	 */
	Task getNext_task();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getNext_task <em>Next task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next task</em>' reference.
	 * @see #getNext_task()
	 * @generated
	 */
	void setNext_task(Task value);

} // Junction
