package org.eclipse.papyrus.designer.realtime.pycpa.m2m.executors;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.Log;
import org.eclipse.m2m.qvt.oml.util.WriterLog;

import org.eclipse.uml2.uml.Activity;
import org.eclipse.papyrus.designer.realtime.pycpa.m2m.Activator;

public class RTMLToPyCPATransformExecutor {

	public static void run(Activity sb) throws ExecutionException
	{
		// create a string to represent the base path of model files involved in the transformation
        URI resBasePathName = sb.getModel().eResource().getURI().trimSegments(1);

		// Refer to an existing transformation via URI
		URI transformationURI = URI.createURI("platform:/plugin/org.eclipse.papyrus.designer.realtime.pycpa.m2m/transforms/RTML2PyCPA.qvto");

		// create executor for the given transformation
		TransformationExecutor executor = new TransformationExecutor(transformationURI);

		// define the transformation input
		EList<EObject> semanticObjects = new BasicEList<EObject>();
		semanticObjects.add(sb);

		// create the input extent with its initial contents
		ModelExtent input = new BasicModelExtent(semanticObjects);
		// create an empty extent to catch the output
		ModelExtent output = new BasicModelExtent();

		// setup the execution environment details -> 
		// configuration properties, logger, monitor object etc.
		OutputStreamWriter outStream = new OutputStreamWriter(System.out);
		Log log = new WriterLog(outStream);
		ExecutionContextImpl context = new ExecutionContextImpl();
		context.setLog(log);

		// run the transformation assigned to the executor with the given 
		// input and output and execution context
		// Remark: variable arguments count is supported
		ExecutionDiagnostic result = executor.execute(context, input, output);

		// check the result for success
		if(result.getSeverity() == Diagnostic.OK)
		{
			// the output objects got captured in the output extent
			List<EObject> outObjects = output.getContents();
			// let's persist them using a resource
			ResourceSet resourceSet2 = new ResourceSetImpl();
			Resource outResource = resourceSet2.createResource(resBasePathName.appendSegment(sb.getName()).appendFileExtension("pycpa"));
			outResource.getContents().addAll(outObjects);
			try
			{
				outResource.save(Collections.emptyMap());
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			// turn the result diagnostic into status and send it to error log			
			IStatus status = BasicDiagnostic.toIStatus(result);
			Activator.getDefault().getLog().log(status);
			throw new ExecutionException(status.getMessage());
		}
	}

}
