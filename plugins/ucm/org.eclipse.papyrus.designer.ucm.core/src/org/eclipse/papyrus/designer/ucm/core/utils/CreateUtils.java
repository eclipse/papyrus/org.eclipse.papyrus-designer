/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.ucm.core.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.UMLPackage;

public class CreateUtils {
	/**
	 * Navigate to the nearest package having a given stereotype. Examines the nearest package first and will
	 * step-up to the owning packages. It Will return null, if none exists
	 * 
	 * @param element
	 *            an element for which we want to obtain the nearest package
	 * @param stereoApplication
	 *            the applied stereotype (in form of a class) we expect on package.
	 * @return the package with the passed stereotype application or null
	 */
	public static Package nearestPackageWithStereo(Element element, Class<? extends EObject> stereoApplication) {
		if (element != null) {
			Package nearest = element.getNearestPackage();
			if (StereotypeUtil.isApplied(nearest, stereoApplication)) {
				return nearest;
			}
			return nearestPackageWithStereo(nearest.getOwner(), stereoApplication);
		}
		return null;
	}

	/**
	 * Return an existing sub-package or create a new one, if none exists yet
	 * (will do nothing and return null, if a sub-element with the name exist, but is not a package)
	 *
	 * @param pkg
	 *            a package in which we want to create a sub-package
	 * @param subPkgName
	 *            the name of the sub-package
	 * @return the sub-package with the given name or null
	 */
	public static Package subPackage(Package pkg, String subPkgName) {
		PackageableElement subPkg = pkg.getPackagedElement(subPkgName);
		if (subPkg == null) {
			subPkg = pkg.createPackagedElement(subPkgName, UMLPackage.eINSTANCE.getPackage());
		}
		if (subPkg instanceof Package) {
			return (Package) subPkg;
		}
		return null;
	}
}
