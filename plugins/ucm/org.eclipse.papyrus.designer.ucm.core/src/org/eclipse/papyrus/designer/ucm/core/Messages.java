package org.eclipse.papyrus.designer.ucm.core;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.ucm.core.messages"; //$NON-NLS-1$
	public static String AffectPolicyEditHelperAdvice_ChoosePorts;
	public static String AffectPolicyEditHelperAdvice_ConfigureAffectPolicy;
	public static String AssemblyPartEditHelperAdvice_AssemblyPartConfig;
	public static String AssemblyPartEditHelperAdvice_ChoosePartType;
	public static String ComponentImplementationEditHelperAdvice_ChooseComponentType;
	public static String ComponentImplementationEditHelperAdvice_ImplementationConfig;
	public static String CreateUCMModelCommand_InitModel;
	public static String CreateUCMModelCommand_ProfileNotFound;
	public static String CreateUCMModelCommand_ProfileSuppNotFound;
	public static String CreateUCMModelCommand_ProfileTypesNotFound;
	public static String ElementTypeUtils_NoMatcher;
	public static String MenuHelper_ExistingTypes;
	public static String MenuHelper_SelectExistingTypes;
	public static String ConnectionEditHelperAdvice_ChooseConnectorDef;
	public static String ConnectionEditHelperAdvice_ConnectionConfigCommand;
	public static String PortEditHelperAdvice_ChoosePortType;
	public static String PortEditHelperAdvice_PortConfigCommand;
	public static String PortElementEditHelperAdvice_1;
	public static String PortElementEditHelperAdvice_ChooseValue;
	public static String TechnicalPolicyDefinitionEditHelperAdvice_ChooseAspect;
	public static String TechnicalPolicyDefinitionEditHelperAdvice_ConfigureTechPolicy;
	public static String TechnicalPolicyEditHelperAdvice_ChooseDefinition;
	public static String TechnicalPolicyEditHelperAdvice_ConfigureTechPolicy;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
