/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (inspired from SysML variant of this file)
 *  	- Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.designer.ucm.profile.util;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations on UCM resources
 *
 */
public final class UCMResource {

	public static final String PROFILE_PATHMAP = "pathmap://UCM_PROFILE/"; //$NON-NLS-1$

	public static final String PROFILE_PATH = PROFILE_PATHMAP + "ucm.profile.uml"; //$NON-NLS-1$

	public static final URI PROFILE_PATH_URI = URI.createURI(PROFILE_PATH);

	public static final String PROFILE_URI = "http://www.omg.org/ucm/0.9"; //$NON-NLS-1$
}
