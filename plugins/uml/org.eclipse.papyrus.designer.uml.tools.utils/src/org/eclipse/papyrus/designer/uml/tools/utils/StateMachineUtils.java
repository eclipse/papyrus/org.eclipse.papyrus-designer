/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.uml.tools.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;

/**
 * Some utility functions for state machines
 *
 */
public class StateMachineUtils {
	/**
	 * @param state
	 *            a composite state return the first region of a state
	 */
	public static Region region(State state) {
		EList<Region> regions = state.getRegions();
		if (regions.size() > 0) {
			return regions.get(0);
		}
		return null;
	}

	/**
	 * @param stateMachine
	 *            a state machine return the first region of that state
	 *            machine
	 */
	public static Region region(StateMachine stateMachine) {
		EList<Region> regions = stateMachine.getRegions();
		if (regions.size() > 0) {
			return regions.get(0);
		}
		return null;
	}

	/**
	 * @param state
	 *            a composite state
	 * @return the list of substates in the first region of a composite state
	 */
	public static EList<State> subStates(State state) {
		Region region = region(state);
		return (region != null) ? states(region) : new BasicEList<>();
	}

	public static EList<State> states(StateMachine stateMachine) {
		Region region = region(stateMachine);
		return (region != null) ? states(region) : new BasicEList<>();
	}

	/**
	 * @param region
	 *            a region
	 * @return all states (not pseudo states) of the pased region
	 */
	public static EList<State> states(Region region) {
		EList<State> states = new BasicEList<>();
		for (org.eclipse.uml2.uml.Vertex vertex : region.getSubvertices()) {
			if (vertex instanceof State) {
				states.add((State) vertex);
			}
		}
		return states;
	}

	public static EList<Pseudostate> entryPoints(State state) {
		EList<Pseudostate> pseudoStates = new BasicEList<>();
		for (Pseudostate pseudoState : state.getConnectionPoints()) {
			if (pseudoState.getKind() == PseudostateKind.ENTRY_POINT_LITERAL) {
				pseudoStates.add(pseudoState);
			}
		}
		return pseudoStates;
	}

	public static EList<Pseudostate> exitPoints(State state) {
		EList<Pseudostate> pseudoStates = new BasicEList<>();
		for (Pseudostate pseudoState : state.getConnectionPoints()) {
			if (pseudoState.getKind() == PseudostateKind.EXIT_POINT_LITERAL) {
				pseudoStates.add(pseudoState);
			}
		}
		return pseudoStates;
	}

	public static EList<Pseudostate> junctionPoints(Region region) {
		return filteredPseudoStates(region, PseudostateKind.JUNCTION_LITERAL);
	}

	public static EList<Pseudostate> choicePoints(Region region) {
		return filteredPseudoStates(region, PseudostateKind.CHOICE_LITERAL);
	}

	public static EList<Pseudostate> filteredPseudoStates(Region region, PseudostateKind kind) {
		EList<Pseudostate> pseudoStates = new BasicEList<>();
		for (Vertex vertex : region.getSubvertices()) {
			if (vertex instanceof Pseudostate) {
				Pseudostate pseudoState = (Pseudostate) vertex;
				if (pseudoState.getKind() == kind) {
					pseudoStates.add(pseudoState);
				}
			}
		}
		return pseudoStates;
	}

	public static Pseudostate firstPseudoState(Region region, PseudostateKind kind) {
		for (Vertex vertex : region.getSubvertices()) {
			if (vertex instanceof Pseudostate) {
				Pseudostate pseudoState = (Pseudostate) vertex;
				if (pseudoState.getKind() == kind) {
					return pseudoState;
				}
			}
		}
		return null;
	}

	/**
	 * @return The set of all transitions ending indirectly on the state. This is,
	 *         those which end on one of the state's entry points.
	 */
	public static Collection<Transition> indirectIncomings(State state) {
		EList<Transition> indirectIncoming = new BasicEList<>();
		for (Pseudostate p : entryPoints(state)) {
			for (Transition t : p.getIncomings()) {
				indirectIncoming.add(t);
			}
		}
		return indirectIncoming;
	}

	/**
	 * @return The set of all transitions ending indirectly on the state. This is,
	 *         those which end on one of the state's entry points.
	 */
	public static Collection<Transition> indirectOutgoings(State state) {
		EList<Transition> indirectIncoming = new BasicEList<>();
		for (Pseudostate p : entryPoints(state)) {
			for (Transition t : p.getIncomings()) {
				indirectIncoming.add(t);
			}
		}
		return indirectIncoming;
	}

	/**
	 * @return The set of all direct and indirect incoming transitions to the state.
	 */
	public static Collection<Transition> allIncomings(State state) {
		EList<Transition> allIncoming = new BasicEList<>();
		for (Transition t : state.getIncomings()) {
			allIncoming.add(t);
		}
		allIncoming.addAll(indirectIncomings(state));
		return allIncoming;
	}

	/**
	 * @return The set of all direct and indirect incoming transitions to the state.
	 */
	public static Collection<Transition> allOutgoings(State state) {
		EList<Transition> allIncoming = new BasicEList<>();
		for (Transition t : state.getIncomings()) {
			allIncoming.add(t);
		}
		allIncoming.addAll(indirectOutgoings(state));
		return allIncoming;
	}

	/**
	 * Adds all the inner elements of a composite state to self, except for the
	 * initial, entry, exit and history pseudo-states.
	 *
	 * This operation moves those elements, as it removes them from their original
	 * container and sets their owner to the recipient.
	 */
	public static void moveContents(Region targetRegion, State source) {
		Region sourceRegion = region(source);
		EList<Vertex> vertices = new BasicEList<>(sourceRegion.getSubvertices());
		for (Vertex v : vertices) {
			// move states and pseudostates
			targetRegion.getSubvertices().add(v);
		}
		EList<Transition> transitions = new BasicEList<>(sourceRegion.getTransitions());
		for (Transition t : transitions) {
			targetRegion.getTransitions().add(t);
		}
	}

	public static Pseudostate initialState(Region region) {
		return firstPseudoState(region, PseudostateKind.INITIAL_LITERAL);
	}

	public static Pseudostate deepHistory(Region region) {
		return firstPseudoState(region, PseudostateKind.DEEP_HISTORY_LITERAL);
	}

	public static State createState(Region region, String name) {
		return (State) region.createSubvertex(name, UMLPackage.eINSTANCE.getState());
	}

	public static Pseudostate createPseudostate(Region region, String name) {
		return (Pseudostate) region.createSubvertex(name, UMLPackage.eINSTANCE.getPseudostate());
	}

	/**
	 * @param r
	 *            a region
	 * @return the state that is reached from the initial state (via the first transition) or null
	 */
	public static State findInitialState(Region r) {
		Pseudostate pseudoDefault = firstPseudoState(r, PseudostateKind.INITIAL_LITERAL);
		if (pseudoDefault != null) {
			return (State) pseudoDefault.getOutgoings().get(0).getTarget();
		}
		return null;
	}

	/**
	 * @param r
	 *            a region
	 * @return the code of the effect that is associated with the first transition from the initial state
	 */
	public static String getInitialEffect(Region r) {
		Pseudostate pseudoDefault = firstPseudoState(r, PseudostateKind.INITIAL_LITERAL);
		if (pseudoDefault != null) {
			Transition t = pseudoDefault.getOutgoings().get(0);
			return BehaviorUtils.body(t.getEffect(), null);
		}
		return "//no initial effect is defined"; //$NON-NLS-1$
	}

	/**
	 * @param t
	 *            a transition
	 * @return the code of the effect of a transition, if any (and an opaque
	 *         behavior), return null, otherwise
	 */
	public static String getTransitionEffect(Transition t) {
		String body = BehaviorUtils.body(t.getEffect(), null);
		if (body != null) {
			return body;
		}
		return StringConstants.EMPTY;
	}

	/**
	 * @param parent
	 *            a parent state which is eventually a composite state
	 * @return
	 */
	public static List<State> transitiveSubStates(State parent) {
		List<State> ret = new ArrayList<>();
		if (parent.isComposite()) {
			for (Region r : parent.getRegions()) {
				for (State state : states(r)) {
					ret.add(state);
					ret.addAll(transitiveSubStates(state));
				}
			}
		}
		return ret;
	}

	public static List<State> transitiveSubStates(Region parent) {
		List<State> ret = new ArrayList<>();
		for (State state : states(parent)) {
			if (!(state instanceof FinalState)) {
				ret.add(state);
				ret.addAll(transitiveSubStates(state));
			}
		}
		return ret;
	}

	/**
	 * Return the name of an event (remove non-ASCII characters)
	 */
	public static String eventName(Event event) {
		var name = StringConstants.EMPTY;
		var repeat = false;
		for (char c : event.getName().toCharArray()) {
			if (Character.isAlphabetic(c) || Character.isDigit(c)) {
				name += c;
				repeat = false;
			} else if (!repeat) {
				name += StringConstants.UNDERSCORE;
				repeat = true;
			}
		}
		return name;
	}

	/**
	 * @param event
	 *            an event
	 * @return the ID of an event (used in connection with code generation)
	 */
	public static String eventID(Event event) {
		return eventName(event).toUpperCase() + "_ID"; //$NON-NLS-1$
	}

	/**
	 * @param r
	 *            a region
	 * @return true, if the region contains a history or deep-history state
	 */
	public static boolean isSavehistory(Region r) {
		for (Vertex v : r.getSubvertices()) {
			if ((v instanceof Pseudostate) && ((Pseudostate) v).getKind() == PseudostateKind.SHALLOW_HISTORY_LITERAL) {
				return true;
			}
		}
		return isSaveDeepHistory(r);
	}

	/**
	 * @param r
	 *            a region
	 * @return true, if the region (or a containing region in upper direction) contains a history or deep-history state
	 */
	public static boolean isSaveDeepHistory(Region r) {
		for (Vertex v : r.getSubvertices()) {
			if ((v instanceof Pseudostate) && ((Pseudostate) v).getKind() == PseudostateKind.DEEP_HISTORY_LITERAL) {
				return true;
			}
		}
		if (r.getState() != null) {
			var nextRegion = r.getState().getContainer();
			return isSaveDeepHistory(nextRegion);
		}
		return false;
	}

	/**
	 * @param b
	 *            an behavior
	 * @return true, if the behavior is an opaque behavior with associated code
	 */
	public static boolean isBehaviorExist(Behavior b) {
		return BehaviorUtils.body(b, null) != null;
	}

	/**
	 * @param s
	 *            a state
	 * @return true, if the passed state has an outgoing transition without trigger
	 */
	public static boolean hasTriggerlessTransition(State s) {
		for (Transition t : s.getOutgoings()) {
			if (t.getTriggers().isEmpty()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param sm
	 *            a state machine
	 * @return true, if at least one state of the first region has an outgoing transition without trigger
	 */
	public static boolean hasTriggerlessTransition(StateMachine sm) {
		Region r = region(sm);
		if (r != null) {
			for (State state : states(r)) {
				if (hasTriggerlessTransition(state)) {
					return true;
				}
			}
		}
		return false;
	}

}
