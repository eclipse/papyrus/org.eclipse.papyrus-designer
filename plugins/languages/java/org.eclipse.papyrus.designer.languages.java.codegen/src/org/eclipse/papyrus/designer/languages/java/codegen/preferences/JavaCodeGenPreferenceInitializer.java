/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.papyrus.designer.languages.java.codegen.Activator;


public class JavaCodeGenPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences store = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		store.put(JavaCodeGenConstants.P_JAVA_SUFFIX_KEY, JavaCodeGenConstants.P_JAVA_SUFFIX_DVAL);
		store.put(JavaCodeGenConstants.P_PROJECT_PREFIX_KEY, JavaCodeGenConstants.P_PROJECT_PREFIX_DVAL);
		store.put(JavaCodeGenConstants.P_COMMENT_HEADER_KEY, JavaCodeGenConstants.P_COMMENT_HEADER_DVAL);
	}
}
