/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.transformation;

import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.base.HierarchyLocationStrategy;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.Project;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.NamedElement;

public class JavaLocationStrategy extends HierarchyLocationStrategy {
	/**
	 * Return the filename for a given named element.
	 *
	 * @param element
	 *            a named element
	 * @return filename for this element
	 */
	public String getFileName(NamedElement element) {
		String fileQualifiedName = super.getFileName(element);
		
		org.eclipse.uml2.uml.Package root = PackageUtil.getRootPackage(element);
		if (fileQualifiedName != null && root != null && GenUtils.hasStereotype(root, Project.class)) {
			if (fileQualifiedName.startsWith(root.getName()) || fileQualifiedName.startsWith("/" + root.getName())) { //$NON-NLS-1$
				String fileQualifiedNameWithoutRoot = fileQualifiedName.replaceFirst(root.getName() + "/", ""); //$NON-NLS-1$ //$NON-NLS-2$
				return fileQualifiedNameWithoutRoot;
			}
			
			return fileQualifiedName;
		}
		
		return fileQualifiedName;
	}
}
