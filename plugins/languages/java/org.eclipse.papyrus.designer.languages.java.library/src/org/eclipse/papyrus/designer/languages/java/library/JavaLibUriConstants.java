/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.java.library;

import org.eclipse.emf.common.util.URI;
import org.eclipse.uml2.uml.resource.UMLResource;

/**
 * Utility class to get informations on JAVA library resources
 */
public final class JavaLibUriConstants {

	public static final String LIBRARY_PATHMAP = "pathmap://PapyrusJava_LIBRARIES/"; //$NON-NLS-1$

	public static final String LIBRARY_PATH = LIBRARY_PATHMAP + "JavaLibrary.uml"; //$NON-NLS-1$

	public static final URI LIBRARY_PATH_URI = URI.createURI(LIBRARY_PATH);

	/**
	 * Java primitive types, as part of the UML library
	 */
	public static final URI PT_LIBRARY_PATH_URI = URI.createURI(UMLResource.JAVA_PRIMITIVE_TYPES_LIBRARY_URI);

}
