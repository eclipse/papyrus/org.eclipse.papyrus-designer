/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.java.reverse.umlparser;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Instance of this class denote a qualified name of a type.
 *
 * @author dumoulin
 *
 */
@SuppressWarnings("serial")
public class QualifiedName extends ArrayList<String> {

	/**
     *
     */
	public QualifiedName() {
	}

	/**
	 * @param initialCapacity
	 */
	public QualifiedName(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * @param c
	 */
	public QualifiedName(Collection<String> c) {
		super(c);
	}

	/**
	 * Return the last name of the qualified name.
	 * This is usually the short name.
	 *
	 * @return last segment of name
	 */
	public String getName() {

		return get(size() - 1);
	}
}
