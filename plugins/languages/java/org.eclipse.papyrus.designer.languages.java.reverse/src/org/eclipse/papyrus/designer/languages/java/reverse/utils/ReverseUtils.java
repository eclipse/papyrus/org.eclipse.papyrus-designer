/*****************************************************************************
 * Copyright (c) 2025 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.java.reverse.utils;

/**
 * Utility functions for reverse engineering
 */
public class ReverseUtils {
	/**
	 * Assure that some characters that are problematic for UML get replaced. This is notably the case for some \\u00xy characters in bodies
	 * or default values (TODO: rather fix translation to single character in parser)
	 *
	 * @param in
	 *            the input string
	 * @return the string containing Java compliant escape for the string
	 */
	public static String escapeChars(String in) {
		// replace 0x1a and 0x00 chars with proper sequence
		return in.replaceAll("\u001a", "\\\\u001a").replaceAll("\u0000", "\\\\u0000");
	}

	/**
	 * Decrease the indentation in the passed string by 4 characters (for opaque-behaviors)
	 * 
	 * @param in
	 *            the input string
	 * @return a string with reduced characters
	 */
	public static String trimIndent(String in) {
		// remove first 4 spaces after newline (parser translates tabs into spaces)
		return escapeChars(in).replaceAll("\n    ", "\n").strip();
	}
}
