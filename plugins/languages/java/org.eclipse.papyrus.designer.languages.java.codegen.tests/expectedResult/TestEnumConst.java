// --------------------------------------------------------
// Code generated by Papyrus Java
// --------------------------------------------------------

package JavaCodegenTest;

import JavaCodegenTest.Package1.TestEnum;

// Manual imports
// non required import (since already added automatically)
import JavaCodegenTest.Package1.TestEnum;
// End of manual imports

/************************************************************/
/**
 * 
 */
public abstract class TestEnumConst {
	/**
	 * 
	 */
	public int externalProp;
	/**
	 * 
	 */
	public TestEnum enumAttr = TestEnum.TestLiteral;
}
