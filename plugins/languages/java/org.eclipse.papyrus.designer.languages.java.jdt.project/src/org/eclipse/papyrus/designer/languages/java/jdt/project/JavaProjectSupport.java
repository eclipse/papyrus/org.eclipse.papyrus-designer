/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.jdt.project;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.AbstractSettings;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Package;

/**
 * Supports the creation and configuration of JDT projects (without UI)
 */
public class JavaProjectSupport implements ILangProjectSupport {

	private static final String ORG_ECLIPSE_JDT_LAUNCHING_JRE_CONTAINER = "org.eclipse.jdt.launching.JRE_CONTAINER"; //$NON-NLS-1$

	/**
	 * Create a Java project. Caller should test before calling, whether the project
	 * exists already.
	 *
	 * @param projectName
	 * @return the created project or null
	 */
	@Override
	public IProject createProject(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IProject project = root.getProject(projectName);
		IProgressMonitor monitor = new NullProgressMonitor();
		try {
			if (!project.exists()) {
				project.create(monitor);
				project.open(monitor);
			}

			// add project natures
			IProjectDescription description = project.getDescription();
			description.setNatureIds(new String[] { JavaCore.NATURE_ID });
			project.setDescription(description, null);

			// now create Java Project
			IJavaProject javaProject = JavaCore.create(project);

			// add bin folder
			IFolder binFolder = project.getFolder("bin");
			if (!binFolder.exists()) {
				binFolder.create(false, true, null);
			}
			javaProject.setOutputLocation(binFolder.getFullPath(), null);

			IFolder sourceFolder = project.getFolder("src");
			if (!sourceFolder.exists()) {
				sourceFolder.create(false, true, null);
			}

			IClasspathEntry[] entries = new IClasspathEntry[2];
			// ignore existing entry which configures the project root as source folder
			// (this would cause a "nesting" exception when src is added later)

			// add standard container to project class path
			entries[0] = JavaCore.newContainerEntry(new Path(ORG_ECLIPSE_JDT_LAUNCHING_JRE_CONTAINER));
			// add src folder to class path (LocateJavaProject takes care of adding src-gen folder)
			entries[1] = JavaCore.newSourceEntry(javaProject.getPackageFragmentRoot(sourceFolder).getPath());

			javaProject.setRawClasspath(entries, null);
			
			project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		} catch (CoreException e) {
			Activator.getDefault().getLog().error(Messages.JavaProjectSupport_COULD_NOT_CREATE, e);
		}
		if ((project == null) || !project.exists()) {
			throw new RuntimeException(Messages.JavaProjectSupport_COULD_NOT_CREATE);
		}
		return project;
	}

	@Override
	public IProject createProject(String projectName, Package modelRoot) {
		return createProject(projectName);
	}

	@Override
	public void setSettings(IProject project, AbstractSettings settings) {
	}

	@Override
	public AbstractSettings initialConfigurationData() {
		return null;
	}

	@Override
	public void gatherConfigData(Classifier implementation, AbstractSettings settings) {
	}
}