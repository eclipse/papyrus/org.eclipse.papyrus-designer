from .superclass3 import SuperClass3
from .superclass2 import SuperClass2
from .superclass1 import SuperClass1

class DaughterClass(SuperClass3, SuperClass2, SuperClass1):

	def __init__(self):
		pass

