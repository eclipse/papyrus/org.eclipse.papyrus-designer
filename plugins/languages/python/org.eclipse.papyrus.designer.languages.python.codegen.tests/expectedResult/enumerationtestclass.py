from .enumerationtest1 import EnumerationTest1
from .enumerationtest2 import EnumerationTest2

class EnumerationTestClass:

	def __init__(self):
		self.var1: EnumerationTest1 = EnumerationTest1.EnumerationLiteral1
		self.var2: EnumerationTest2 = EnumerationTest2.EnumerationLiteral4

