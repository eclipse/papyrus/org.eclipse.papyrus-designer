/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.transformation;

import org.eclipse.papyrus.designer.languages.common.base.HierarchyLocationStrategy;
import org.eclipse.uml2.uml.NamedElement;

public class PythonLocStrategy extends HierarchyLocationStrategy {
	/**
	 * Return the filename for a given named element. In case of Python,
	 * a lower case name is used by default.
	 *
	 * @param element
	 *            a named element
	 * @return filename for this element
	 */
	public String getFileName(NamedElement element) {
		return super.getFileName(element).toLowerCase();
	}
}
