/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.uml2.uml.DataType

class PyDatatype {
	def static genDatatype(DataType datatype)'''
		class «datatype.name»:
		
			def __init__(self):
				«IF datatype.getAllAttributes.empty»
					pass
				«ELSE»
					«FOR att : datatype.getAllAttributes»
						self.«att.name»
					«ENDFOR»
				«ENDIF»

	'''
}