/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 * 
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Property
import org.eclipse.papyrus.designer.infra.base.StringConstants

class PyAttributes {
	/**
	 * Assign the value of the attribute depending on its multiplicity. If the attribute has an infinite multiplicity
	 * the function returns an empty list. If the multiplicity number is specified, a list of the same size will be
	 * created containing all the attributes
	 */
	def static assignAttributeValue(Property property) {
		if (!property.multivalued) {
			return ''' = «getAttributeValue(property)»'''
		} else {
			if (property.upperBound == -1) {
				return ''' = []'''
			} else {
				// repeat same value several times (TODO: useful?)
				return ''' = [«FOR i : 0..property.upper SEPARATOR (', ')»«getAttributeValue(property)»()«ENDFOR»]'''
			}
		}
	}

	/**
	 * Same as the previous method, but it treats composite attributes. For multiple instances, the constructor
	 * of the attribute will be called multiple times in a list
	 */
	def static assignCompositeAttributes(Property property) {
		if (!property.isMultivalued) {
			return ''' = «property.type.name»()'''
		} else {
			if (property.upperBound == -1) {
				return ''' = []'''
			} else {
				// repeat same value several times (TODO: useful?)
				return ''' = [«FOR i : 0..property.upper SEPARATOR (', ')»«property.type.name»()«ENDFOR»]'''
			}
		}
	}

	/**
	 * A simple method that adds a type definition for an attribute
	 */
	def static assignType(Property property) {
		if (property.type === null) {
			return " : None"
		}
		switch (property.type.name) {
			case (property.type.name == "Integer"): return ": int"
			case (property.type.name == "Boolean"): return ": bool"
			case (property.type.name == "Real"): return ": float"
			case (property.type.name == "String"): return ": str"
			default: return ''': «property.type.name»'''
		}
	}

	/**
	 * A method that assigns the value of the attribute, depending on if the value is assigned in the model or not.
	 */
	def static getAttributeValue(Property property) {
		if (property.defaultValue !== null && property.type !== null) {
			switch (property.type.name) {
				case (property.type.name == "bool" || property.type.name == "Boolean"): return property.defaultValue.
					stringValue.toFirstUpper
				case "byte": return "bytes(" + property.defaultValue.stringValue + ")"
				case property.type instanceof Enumeration: return property.type.name + '.' +
					property.defaultValue.stringValue
				// respect user specified default value in most cases
				default: return property.defaultValue.stringValue
			}
		} else {
			if (property.type === null) {
				return "None"
			}
			switch (property.type.name) {
				case (property.type.name == "int" || property.type.name == "Integer"): return "0"
				case (property.type.name == "bool" || property.type.name == "Boolean"): return "False"
				case (property.type.name == "float" || property.type.name == "Real"): return "0.0"
				case (property.type.name == "str" || property.type.name == "String"): return "''"
				case "complex": return "0+0j"
				case "byte": return "0x00"
				case "tuple": return "()"
				case "set": return "set()"
				case "list": return "[]"
				case "dict": return "{}"
				case "range": return "rang(0)"
				default: return "None"
			}
		}
	}

	/**
	 * A method that write the visibility of the attribute.
	 * __ for private, _ for protected and nothing for public
	 */
	def static writeAttributeVisibility(Property attribute) {
		switch (attribute.visibility.toString) {
			case "private": return "__"
			case "protected": return "_"
			case "package": return "_"
			default: return StringConstants.EMPTY
		}
	}
}
