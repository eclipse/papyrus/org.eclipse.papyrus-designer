/**
 */
package org.eclipse.papyrus.designer.languages.python.profile.python;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Specialisation of External stereotype from common code generation profile without additional attributes
 * <!-- end-model-doc -->
 *
 *
 * @see org.eclipse.papyrus.designer.languages.python.profile.python.PythonPackage#getExternal()
 * @model
 * @generated
 */
public interface External extends org.eclipse.papyrus.designer.languages.common.profile.Codegen.External {
} // External
