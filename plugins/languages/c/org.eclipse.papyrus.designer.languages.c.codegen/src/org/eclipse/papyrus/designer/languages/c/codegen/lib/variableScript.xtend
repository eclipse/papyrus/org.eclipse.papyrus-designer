/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.StorageClass
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.FinalState
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.TypedElement

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.MultiplicityScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.TypeScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ValueSpecificationScript.*
import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.*
import static extension org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication

class variableScript {
	def static genVariableDeclaration(Property property) '''
		«FOR comment : property.ownedComments»
			«comment.genComment»
		«ENDFOR»
		«property.genDeclarator»«
			property.genType»«property.genPointer» «property.genName»«property.genMultiplicity»«
			IF (property.defaultValue !== null && property.isStatic)» «property.genDefaultValue()» «ENDIF»;
	'''

	def static genStateVariableDeclaration(Class clazz) '''

		enum StateIDEnum activeStateID;
	'''

	def static genStateArrayDeclaration(StateMachine sm) '''

		State_t states[«sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)].size»];
	'''

	def static genDefaultValue(Property property) '''
		«IF (property.defaultValue !== null)»
		  = «IF (property.upper != 1)»{ «property.defaultValue.ValueSpecificationTemplate()» }
			«ELSE»«property.defaultValue.ValueSpecificationTemplate()»
			«ENDIF»
		«ENDIF»
	'''


	def static genType(TypedElement typedElement) { '''
		«IF (typedElement.type === null)»
			void *
		«ELSE»
			«typedElement.type.genCTypeName»
		«ENDIF»
		'''.toString.trim()
	}


	def static genCTypeName(Type type) '''
		«IF (type === null || type.name === null)»
			undefined
		«ELSEIF (type.name.toLowerCase == 'boolean') || (type.name.toLowerCase == 'bool')»
			«type.genBooleanType»
		«ELSEIF (type.name.toLowerCase == 'integer')»
			«type.genIntegerType»
		«ELSEIF (type.name.toLowerCase == 'string')»
			«type.genStringType»
		«ELSE»
			«type.name»
		«ENDIF»'''


	def static genDeclarator(TypedElement typedElement) '''
		«typedElement.genStorageClass()» «typedElement.genConstant»
	'''


	def static genStorageClass(TypedElement typedElement) '''
		«IF (typedElement.hasStereotype(StorageClass))»
			«typedElement.getStereotypeApplication(StorageClass).storageClass.toString»
		«ENDIF»
		
	'''


	def static genConstant(TypedElement typedElement) '''
		«IF (typedElement.hasStereotype(Const))»const «ENDIF»
	'''

	def static genPointer(TypedElement typedElement) '''
		«IF (typedElement.hasStereotype(Ptr))»* «ENDIF»
	'''
	
	def static genInitDefaultValue(Property property) '''
		«IF (property.upper == 1)»
			self->«property.genName» «property.genDefaultValue»;
		«ELSE»
			«property.genType» tmp_«property.genName»«property.genMultiplicity» «property.genDefaultValue»;
			self->«property.genName»«'«'»0«'»'» = tmp_«property.genName()»;
		«ENDIF»
	'''
}
