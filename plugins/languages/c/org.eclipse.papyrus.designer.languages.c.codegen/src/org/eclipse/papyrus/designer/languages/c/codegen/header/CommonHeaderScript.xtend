/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.header

import org.eclipse.uml2.uml.NamedElement
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*

class CommonHeaderScript {
	def static genHeadingHeader(NamedElement namedElement) '''
		#ifndef «namedElement.genName.toString.toUpperCase»_H_
		#define «namedElement.genName.toString.toUpperCase»_H_
	'''


	def static genEndHeader(NamedElement namedElement) '''
		#endif /*«namedElement.genName.toString.toUpperCase»_H_*/
	'''
}
