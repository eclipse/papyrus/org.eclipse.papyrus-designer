/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen
import org.eclipse.uml2.common.util.UML2Util
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.InterfaceRealization

import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.hasStereotypeTree

class InterfaceScript {
	def static getInterfaceRealizationClass(Classifier classifier) {
		UML2Util.getInverseReferences(classifier).filter[it instanceof InterfaceRealization && !(it as Element).hasStereotypeTree(NoCodeGen)].map[(it as InterfaceRealization).clients]
	}
}
