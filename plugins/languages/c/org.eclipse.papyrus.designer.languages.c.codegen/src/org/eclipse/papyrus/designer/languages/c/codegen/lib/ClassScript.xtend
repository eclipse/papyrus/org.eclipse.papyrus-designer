/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.FinalState
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.VisibilityKind
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.profile.standard.Destroy

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.*
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.DefaultConstructor
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.FunctionScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*

class ClassScript {
	def static genDynamicInstanciationOperationPrototypes(Class clazz) '''
		«clazz.partComment('Default constructor & destructor prototypes')»
		«clazz.genConstructorPrototype»
		«clazz.genDestructorPrototype»
	'''

	// generate a constructor prototype, if there is none 
	def static genConstructorPrototype(Class clazz) '''
		«IF clazz.ownedOperations.filter[hasStereotype(Create)].size == 0 && clazz.hasStereotype(DefaultConstructor)»
			/**
			 * Dynamic instantiation of a «clazz.genName()» structure
			 * @return «clazz.genName()» instance pointer
			 */
			«clazz.genName»* «clazz.genName()»_create();
		«ENDIF»
		
	'''

	// generate a destructor prototype, if there is none 
	def static genDestructorPrototype(Class clazz) '''
		«IF (clazz.ownedOperations.filter[hasStereotype(Destroy)].size == 0) && clazz.hasStereotype(DefaultConstructor)»
			/**
			 * Dynamic destruction of a «clazz.genName()» structure instantiation
			 * @param Pointer to an instance of the «clazz.genName()» structure
			 * @return void
			 */
			void «clazz.genName»_destroy(«clazz.genName»*);
		«ENDIF»
	'''

	def static genDynamicInstanciationOperations(Class clazz) '''
		«clazz.partComment('Default constructor & destructor implementations')»
		«clazz.genConstructor»
		«clazz.genDestructor»
	'''

	// generate a constructor, if there is none
	def static genConstructor(Class clazz) '''
		«IF clazz.ownedOperations.filter[hasStereotype(Create)].size == 0 && clazz.hasStereotype(DefaultConstructor)»
			«clazz.genName()»* «clazz.genName()»_create() {
				«clazz.genName()»* self = («clazz.genName()»*) malloc(sizeof(«clazz.genName()»));
				«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty || clazz.getAllAttributes.filter[defaultValue !== null].size > 0)»
					«clazz.genName()»_init(self);
				«ENDIF»
				return self;
			}
		«ENDIF»
	'''

	// generate a destructor implementation, if there is none
	def static genDestructor(Class clazz) '''
		«IF clazz.ownedOperations.filter[hasStereotype(Destroy)].size == 0 && clazz.hasStereotype(DefaultConstructor)»
			// default destructor
			void «clazz.genName()»_destroy(«clazz.genName()»* self) {
				free(self);
			}
			
		«ENDIF»
	'''

	def static genDefaultInitialisationProtoype(Class clazz) '''
		«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty || clazz.getAllAttributes.filter[defaultValue !== null].size > 0)»
			«clazz.partComment('Default value initialization prototypes')»
			/**
			 * Default value initialization
			 * @param «clazz.genName()» structure instance pointer
			 * @return void
			 */
			void «clazz.genName()»_init(«clazz.genName()»* self);

		«ENDIF»
	'''

	def static genDefaultIntialisationOperation(Class clazz) '''
		«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty || clazz.getAllAttributes.filter[defaultValue !== null].size > 0)»
		
		void «clazz.genName()»_init(«clazz.genName()»* self) {
		
			«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
				//Default value initialization implementation
				«FOR attribute : clazz.getAllAttributes»
					«IF (attribute.defaultValue !== null && !attribute.isStatic && attribute.type !== null)»
						«attribute.genInitDefaultValue»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
		
			«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
				«var sm = clazz.ownedBehaviors.filter(StateMachine).head»
				«var states =sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)]»
				«IF !states.isEmpty»
					// initialization of state internal behaviors if exists
					«FOR s:states»
						«IF s.entry.isBehaviorExist()»
							self->states[«s.name»].entry= &«s.name»_entry;
						«ELSE»
							self->states[«s.name»].entry= NULL;
						«ENDIF»
						«IF s.exit.isBehaviorExist()»
							self->states[«s.name»].exit= &«s.name»_exit;
						«ELSE»
							self->states[«s.name»].exit= NULL;
						«ENDIF»
						«IF s.doActivity.isBehaviorExist()»
							self->states[«s.name»].doActivity= &«s.name»_doActivity;
						«ELSE»
							self->states[«s.name»].doActivity= NULL;
						«ENDIF»
					«ENDFOR»

					«var initialState = clazz.ownedBehaviors.filter(StateMachine).head.regions.head.findInitialState()»
					// set the activeStateID and call initial state entry and do Activity
					self->activeStateID = «initialState.name»;
					if (self->states[«initialState.name»].entry != NULL) {
						self->states[«initialState.name»].entry();
					}
					if (self->states[«initialState.name»].doActivity != NULL) {
						self->states[«initialState.name»].doActivity();
					}
				«ENDIF»
			«ENDIF»
		}
		«ENDIF»
	'''

	def static defaultIntialisationCall(Class clazz) '''
		«IF (clazz.getAllAttributes.filter[defaultValue !== null].size > 0)»
			/* Default value initialization */
			«clazz.genName()»_init(&«clazz.genName()»);
		«ENDIF»
	'''

	def static genClassStructDeclarations(Class clazz) '''
		/* Class Macro definition */
		#define «clazz.genName()»(OBJ) ((«clazz.genName()»*)OBJ)
		
		«FOR comment : clazz.ownedComments»
			«comment.genComment»
		«ENDFOR»
		typedef struct «clazz.genName» «clazz.genName»;
		
		struct «clazz.genName» {
			«FOR attribute : clazz.getClassStructAttributes»
				«IF (!attribute.isStatic)»
					«attribute.genVariableDeclaration»«ENDIF»
			«ENDFOR»
			«IF (!clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
			
							«clazz.genStateVariableDeclaration»
							
							«clazz.ownedBehaviors.filter(StateMachine).head.genStateArrayDeclaration»
			«ENDIF»
			
		};
	'''

	def static getClassStructAttributes(Class clazz) {
		clazz.getAllAttributes.clone + clazz.interfaceRealizations.map[contract].map[ownedAttributes].flatten
	}
	
	def static genNonStaticFunctionDeclarations(Class clazz) '''
		«IF (clazz.ownedOperations.filter[!isStatic && visibility == VisibilityKind.PUBLIC_LITERAL].size() > 0)»
			«clazz.partComment('Function Declarations')»
		«ENDIF»
		«FOR operation : clazz.getAllOperations»
			«IF (!operation.isStatic && operation.visibility == VisibilityKind.PUBLIC_LITERAL)»
				«operation.genFunctionPrototype»
			«ENDIF»
		«ENDFOR»
	'''

	def static genReceptionDeclarations(Class clazz) '''
		«IF (clazz.ownedReceptions.filter[signal !== null && !isStatic && visibility == VisibilityKind.PUBLIC_LITERAL].size() > 0)»
			«clazz.partComment('Reception Declarations')»
		«ENDIF»
		«FOR reception : clazz.ownedReceptions»
			«IF (!reception.isStatic && reception.visibility == VisibilityKind.PUBLIC_LITERAL)»
				«reception.genFunctionPrototype»
			«ENDIF»
		«ENDFOR»
	'''
}
