/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.uml2.uml.Element;

public class UmlCommentServices {

	static final int lengthComment = 60;
	
	/**
	 * Retrieves a comment like this:
	 * "/*"
	 * " * --------- <%body> ---------------"
	 * " *"/"
	 * The number of "-" symbol depends on the body length and
	 * the lengthComment (constant)
	 */
	@SuppressWarnings("nls")
	public static String partComment(Element elt, String body) {

		
		String symbol = "-";
		String symbols = StringConstants.EMPTY;
		int lengthBody = body.length();
		int symbolNumber = (lengthComment - lengthBody) / 2;
		for (int i = 0; i < symbolNumber; i++) {
			symbols = symbols + symbol;
		}
		boolean uneven = (lengthComment - lengthBody) % 2 == 1;
		return
			"/*\n" +
			" * " + symbol + symbols + StringConstants.SPACE + body +
				StringConstants.SPACE + symbols + (uneven ? symbol : StringConstants.EMPTY) + "\n" +
			" */";
	}

}
