/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import java.util.ArrayList
import java.util.List
import org.eclipse.emf.common.util.EList
import org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenUtils
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.CppRoot
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.ExternLibrary
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.External
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Template
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.PrimitiveType
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import org.eclipse.papyrus.designer.languages.c.codegen.utils.CClassUtils

class ImportScript {
	def static genHeaderIncludes(Classifier classifier) '''
		«classifier.partComment('Includes and declares')»
		««« Derived includes from required classifiers that are not pointers
		// Derived includes
		«FOR path : CClassAllIncludesHeader(classifier).sort»
			«includeDirective(path)»
		«ENDFOR»
		// End of Derived includes
		
		««« Derived includes from required classifiers that are pointers
		««« We do *not* include the headers to avoid circular declarations:
		««« e.g. class A has an operation with a pointer parameter of class B and vice versa 
		// Derived declarations
		«FOR path : CClassAllDeclares(classifier).sort» 
			«declareDirective(path)»
		«ENDFOR»
		// End of Derived declares
		
		««« Includes from <<Include>> stereotype's header attribute
		«classifier.CIncludeHeader()»
		
		««« Includes stdio and stdlib
		// Std headers
		«genStdInclude()» 
		// End of Std headers
	'''

	def static genBodyIncludes(Classifier classifier) '''
		««« Includes from <<Include>> stereotype's preBody attribute
		«CIncludePreBody(classifier)»
		««« Include self header
		// Include self header
		«includeDirective(classifier.name + '.' + CCodeGenUtils.getHeaderSuffix())»
		««« Derived includes from required classifiers that are dependencies and pointers
		// Derived includes
		«FOR path : CClassAllIncludesBody(classifier).sort» 
			«includeDirective(path)»
		«ENDFOR»
		««« Includes from <<Include>> stereotype's body attribute
		«CIncludeBody(classifier)»
	'''

	// TODO: why do need stdlib in this case?
	// TODO: why do we need this at all? 
	def static genStdInclude() '''
		#include <stdio.h>
		#include <stdlib.h>
		#include <stdint.h>
	'''

	static def includeDirective(String path) {
		if ((path !== null) && (path.length > 0))
			return '''#include ''' + '"' + path + '"'
	}
	
	static def CClassAllIncludesHeader(Classifier clazz) {
		cClassAllIncludes(clazz, CClassUtils.includedClassifiers(clazz))
	}

	static def CClassAllIncludesBody(Classifier classifier) {
		cClassAllIncludes(classifier, CClassUtils.includedImplementationClassifiers(classifier))
	}

	static def cClassAllIncludes(Classifier classifier, EList<Classifier> list) {
		var List<String> newList = new ArrayList<String>()
		for (cl : list) {
			if (cl != classifier && !GenUtils.hasStereotype(cl, NoCodeGen) || GenUtils.hasStereotype(cl, External)) {
				if ((cl instanceof Enumeration || cl instanceof PrimitiveType) &&
					!GenUtils.hasStereotype(cl, External) &&
					!GenUtils.hasStereotypeTree(cl, ExternLibrary)) {
					if (cl.owner instanceof Package && cl.owner != classifier.owner) {
						/*
						 * No additional include is required, if enum and primitive types are in
						 * the same package. The latter is always included.
						 */
						// TODO how is it done in C compared to C++? Following is from C++ generator.
						//var includePath = (cl.owner as Package).cOwnerPackageIncludePath
						//if (!newList.contains(includePath)) newList.add(includePath)
					} else {
						// str = null
					}
				} else {
					for (includePath : ImportScript.cClassIncludes(cl)) {
						if (!newList.contains(includePath)) newList.add(includePath)
					}
				}
			} else {
				//str = null
			}
		}
		return newList.filter[str | str !== null]
	}

 	static def cOwnerPackageIncludePath(Package pkg) {
		if ((pkg !== null) && (!GenUtils.hasStereotype(pkg, CppRoot))) {
			return GenUtils.getFullPath(pkg) + '/Pkg_' + pkg.name + '.h'
		} else {
			return null
		}
	}

	static def cClassIncludes(NamedElement ne) {
		var List<String> result = new ArrayList<String>()
		if (GenUtils.hasStereotypeTree(ne, ExternLibrary)) {
			// If a class is in an external library, use #include
			// directives defined there
			result.addAll(GenUtils.getApplicationTree(ne, ExternLibrary).includes)
			// No individual includes are produced for members
			// unless the stereotype "External" defines one explicitly
			if (GenUtils.hasStereotype(ne, External)) {
				val incPath = includeName(ne)
				if (incPath !== null) {
					result.add(incPath)
				}
				// else: if unset, assume that no additional include directive is required
			}
		} else {
			result.add(includeName(ne))
		}
		return result
	}

	static def includeName(NamedElement ne) {
		if (GenUtils.hasStereotypeTree(ne, Template)) {
			return UMLUtil.getStereotypeApplication(ne, Template).declaration
		} else {
			if (GenUtils.hasStereotypeTree(ne, External)) {
				return UMLUtil.getStereotypeApplication(ne, External).incPath
			} else {
				// standard case (no stereotypes are applied)
				
				var path = GenUtils.getFullPath(ne.nearestPackage)
				if (path.equals(ne.model.name)) {
					path = ""
				} else if (path.startsWith(ne.model.name + "/")) {
					path = GenUtils.getFullPath(ne.nearestPackage).replace(ne.model.name + "/", "") + "/"
				}

				return path + ne.name + '.' + CCodeGenUtils.getHeaderSuffix()
			}
		}
	}

	static def declareDirective(String path) {
		if ((path !== null) && (path.length > 0)) {
			return path
		}	
	}

	static def CClassAllDeclares(Classifier clazz) {
		ImportScript.cClassAllDeclares(clazz, CClassUtils.declaredClassifiers(clazz), CClassUtils.includedClassifiers(clazz))
	}

	static def cClassAllDeclares(Classifier classifier, EList<Classifier> declaredClassifiers, EList<Classifier> includedClassifiers) {
		var List<String> newList = new ArrayList<String>()
		
		if (declaredClassifiers !== null) {
			if (includedClassifiers !== null) {
				declaredClassifiers.removeAll(includedClassifiers)
			}
			
			for (cl : declaredClassifiers) {
				if (cl != classifier && !GenUtils.hasStereotype(cl, NoCodeGen) || GenUtils.hasStereotype(cl, External)) {
					var declaration = "";
					
					if (!(cl instanceof Enumeration) && !(cl instanceof PrimitiveType)) {
						declaration = '''typedef struct «cl.name» «cl.name»;'''
					}
					
					if (declaration != "") {
						if (!newList.contains(declaration)) {
							newList.add(declaration)
						}
					}
				}
			}
		}
		
		return newList.filter[str | str !== null]
	}
	
	static def CIncludeHeader(NamedElement ne) {
		if (GenUtils.hasStereotype(ne, Include)) {
			UMLUtil.getStereotypeApplication(ne, Include)
			var header = UMLUtil.getStereotypeApplication(ne, Include).header
			if ((header !== null) && (header.length > 0)) {
				var includeHeader = constIncludeHeaderStart + GenUtils.cleanCR(header) + '\n' +
					constIncludeHeaderEnd
				return includeHeader
			}
		}
	}

	static def constIncludeHeaderStart() '''
		// Include from Include stereotype (header)
	'''

	static def constIncludeHeaderEnd() '''
		// End of Include stereotype (header)
	'''

	static def CIncludePreBody(NamedElement ne) {
		if (GenUtils.hasStereotype(ne, Include)) {
			var String preBody = UMLUtil.getStereotypeApplication(ne, Include).preBody
			if ((preBody !== null) && (preBody.length > 0)) {
				var includePreBody = constIncludePreBodyStart + GenUtils.cleanCR(preBody) + '\n' +
					constIncludePreBodyEnd
				return includePreBody
			}
		}
	}

	static def constIncludePreBodyStart() '''
		// Include from Include stereotype (preBody)
	'''

	static def constIncludePreBodyEnd() '''
		// End of Include from Include stereotype (preBody)
	'''

	static def CIncludeBody(NamedElement ne) {
		if (GenUtils.hasStereotype(ne, Include)) {
			var String body = UMLUtil.getStereotypeApplication(ne, Include).body
			if ((body !== null) && (body.length > 0)) {
				var includeBody = constIncludeBodyStart + GenUtils.cleanCR(body) + '\n' +
					constIncludeBodyEnd
				return includeBody
			}
		}
	}
	
	static def constIncludeBodyStart() '''
		// Include from Include stereotype (body)
	'''

	static def constIncludeBodyEnd() '''
		// End of Include from Include stereotype (body)
	'''
}
