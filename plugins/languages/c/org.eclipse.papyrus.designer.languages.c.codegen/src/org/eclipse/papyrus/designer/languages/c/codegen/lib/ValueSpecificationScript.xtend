/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.uml2.uml.InstanceValue
import org.eclipse.uml2.uml.ValueSpecification
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.uml2.uml.Expression

class ValueSpecificationScript {
	def static ValueSpecificationTemplate(ValueSpecification valueSpecification) '''
		«valueSpecification.stringValue()»
	'''

	def static ValueSpecificationTemplate(InstanceValue instanceValue) '''
		«IF (instanceValue.instance instanceof EnumerationLiteral)»
			«FOR slot : instanceValue.instance.slots»
				«FOR value : slot.values» «value.ValueSpecificationTemplate»
				«ENDFOR»
			«ENDFOR»{
			«FOR slot : instanceValue.instance.slots»
				«slot.definingFeature.name.toUpperCase()» = «FOR value : slot.values» «value.ValueSpecificationTemplate» «ENDFOR»;
			«ENDFOR»
			}
		«ELSE»
			«instanceValue.instance.name»
		«ENDIF»
	'''

	def static ValueSpecificationTemplate(Expression expression) '''
		
	'''
}
