/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;

public class UmlClassServices {

	/**
	 * Retrieves all the attributes of this class object, including those
	 * inherited from its parents Duplicates named attributes are removed
	 */
	public EList<Property> getAllAttributes(Class elt) {
		EList<Property> tmp = new BasicEList<Property>();
		Iterator<Property> it = elt.getAllAttributes().iterator();
		while(it.hasNext()) {
			Property p = it.next();
			Iterator<Property> tmp_it = tmp.iterator();
			boolean found = false;
			while(tmp_it.hasNext()) {
				if(tmp_it.next().getName().equalsIgnoreCase(p.getName())) {
					found = true;
				}
			}
			if(!found) {
				tmp.add(p);
			}
		}
		return tmp;
	}

	/**
	 * Retrieves all the operations of this class object, including those
	 * inherited from its parents and those owned by the interface realization
	 */
	public EList<Operation> getAllOperations(Class elt) {
		EList<Operation> unduplicatedOperations = new BasicEList<Operation>();

		/* Retrieves all operations of all interface realizations */
		EList<Operation> interfaceOperations = new BasicEList<Operation>();
		Iterator<InterfaceRealization> interfaceIt = elt.getInterfaceRealizations().iterator();
		while(interfaceIt.hasNext()) {
			InterfaceRealization i = interfaceIt.next();
			Iterator<Operation> interfaceOperations_it = i.getContract().getAllOperations().iterator();
			while(interfaceOperations_it.hasNext()) {
				interfaceOperations.add(interfaceOperations_it.next());
			}
		}

		/* All classifier and parents' classifiers' operation list concatenation with interface operation list */
		EList<Operation> allOperations = new BasicEList<Operation>();
		allOperations.addAll(elt.getAllOperations());
		allOperations.addAll(interfaceOperations);

		/* Making unduplicated list */
		Iterator<Operation> it = allOperations.iterator();
		while(it.hasNext()) {
			Operation p = it.next();
			Iterator<Operation> tmp_it = unduplicatedOperations.iterator();
			boolean found = false;
			while(tmp_it.hasNext()) {
				if(tmp_it.next().getName().equalsIgnoreCase(p.getName())) {
					found = true;
				}
			}
			if(!found) {
				unduplicatedOperations.add(p);
			}
		}

		return unduplicatedOperations;
	}
}
