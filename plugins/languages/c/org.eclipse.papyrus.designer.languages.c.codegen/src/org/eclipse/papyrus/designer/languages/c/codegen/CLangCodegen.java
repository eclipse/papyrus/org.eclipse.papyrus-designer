/*******************************************************************************
 * Copyright (c) 2006 - 2021 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenUtils;
import org.eclipse.papyrus.designer.languages.c.codegen.transformation.CModelElementsCreator;
import org.eclipse.papyrus.designer.languages.c.codegen.utils.LocateCProject;
import org.eclipse.papyrus.designer.languages.common.base.CommonLangCodegen;
import org.eclipse.papyrus.designer.languages.common.base.ModelElementsCreator;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.MethodInfo;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.SyncInformation;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * C language support
 *
 */
public class CLangCodegen extends CommonLangCodegen implements ILangCodegen {

	@Override
	public String getDescription() {
		return "General purpose C code generator for object-oriented models. Uses the CEA C/C++ profile."; //$NON-NLS-1$
	}

	/**
	 * Check whether the code generator is able to produce code for the passed element:
	 * it must be a classifier or package. We dropped the requirement that the C++ profile is applied, since
	 * the user might want to generate code without profile details.
	 */
	public boolean isEligible(Element modelElement) {
		if (modelElement instanceof Classifier || modelElement instanceof org.eclipse.uml2.uml.Package) {
			return true;
		}
		return false;
	}

	@Override
	public String getSuffix(FILE_KIND fileKind) {
		if (fileKind == FILE_KIND.BODY) {
			return CCodeGenUtils.getBodySuffix();
		}
		else {
			return CCodeGenUtils.getHeaderSuffix();
		}
	}
	
	@Override
	public IProject getTargetProject(PackageableElement pe, boolean createIfMissing) {
		return LocateCProject.getTargetProject(pe, createIfMissing);
	}
	
	// TODO: provide a suitable implementation
	@Override
	public SyncInformation getSyncInformation(String methodName, String body) {
		return null;
	}

	// TODO: provide a suitable implementation
	@Override
	public MethodInfo getMethodInfo(NamedElement operationOrBehavior) {
		return null;
	}

	@Override
	protected ModelElementsCreator newCreator(IProject project, PackageableElement pe) {
		return new CModelElementsCreator(project);
	}
}
