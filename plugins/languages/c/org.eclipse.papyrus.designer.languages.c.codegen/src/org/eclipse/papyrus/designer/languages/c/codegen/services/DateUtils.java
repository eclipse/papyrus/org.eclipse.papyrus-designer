/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.services;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.eclipse.uml2.uml.Element;

public class DateUtils {

	/**
	 * Get the date in a short format : January 12, 1952
	 */
	public static String getLongDate(Element elt) {
		Date date = new Date(); // to get the date
		Locale locale = Locale.getDefault();// to get the language of the system
		DateFormat dateFormatShort = DateFormat.getDateInstance(
				DateFormat.LONG, locale);
		return dateFormatShort.format(date);
	}

	/**
	 * Get the date in a short format : 06/08/07
	 */
	public static String getShortDate(Element elt) {
		Date date = new Date(); // to get the date
		Locale locale = Locale.getDefault();// to get the language of the system
		DateFormat dateFormatShort = DateFormat.getDateInstance(
				DateFormat.SHORT, locale);
		return dateFormatShort.format(date);
	}

	/**
	 * Get the time of the system
	 */
	public static String getTime(Element elt) {
		Date date = new Date();
		DateFormat timeFormat = DateFormat.getTimeInstance();
		return timeFormat.format(date);

	}
}