/*******************************************************************************
 * Copyright (c) 2006 - 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.utils;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.papyrus.designer.infra.base.ScopedPreferences;
import org.eclipse.papyrus.designer.languages.c.codegen.Activator;
import org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants;
import org.eclipse.papyrus.designer.languages.common.base.AutoLanguageProjectSupport;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

public class LocateCProject {
	public static final String LANGUAGE_NAME = "C"; //$NON-NLS-1$

	/**
	 * Locate and return the target project for the given packageable element. Return null if
	 * no target project can be found.
	 *
	 * Ensures that the target project is correctly setup to contain generated C/C++ code. Does
	 * not create a new project, but may modify existing ones.
	 *
	 * @param pe
	 *            a packageable element within a model
	 * @param createIfMissing
	 *            if true, ask the user to apply the C++ nature if required.
	 * @return the associated project, if the C++ nature is applied.
	 */
	public static IProject getTargetProject(PackageableElement pe, boolean createIfMissing) {
		// URI uri = pe.eResource().getURI();
		Package rootPkg = PackageUtil.getRootPackage(pe);

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		ScopedPreferences prefs = new ScopedPreferences(Activator.PLUGIN_ID);

		String prefix = prefs != null ? prefs.get(CCodeGenConstants.P_PROJECT_PREFIX_KEY, CCodeGenConstants.P_PROJECT_PREFIX_DVAL) : "bad preferences"; //$NON-NLS-1$
		String projectName = prefix + rootPkg.getName();
		IProject modelProject = root.getProject(projectName);
		if (!modelProject.exists()) {
			String languageName = LANGUAGE_NAME;
			if (createIfMissing) {
				ILangProjectSupport langSupport = AutoLanguageProjectSupport
						.getProjectSupport(languageName);
				if (langSupport != null) {
					modelProject = langSupport.createProject(projectName);
					langSupport.setSettings(modelProject, langSupport.initialConfigurationData());
					if (modelProject == null) {
						return null;
					}
				} else {
					return null;
				}
			}
		}

		// Make sure the target project is open. If it was just created, it is
		// open. However if it is an old project then it could have been closed.
		if (!modelProject.isOpen()) {
			try {
				modelProject.open(null);
			} catch (CoreException e) {
				return null;
			}
		}

		return modelProject;
	}
}
