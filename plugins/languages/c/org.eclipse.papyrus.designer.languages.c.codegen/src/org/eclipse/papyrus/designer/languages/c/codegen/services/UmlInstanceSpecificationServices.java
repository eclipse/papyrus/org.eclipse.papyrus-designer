/*******************************************************************************
 * Copyright (c) 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;

public class UmlInstanceSpecificationServices {

	public Classifier getClassifier(InstanceSpecification is, int index) {
		return is.getClassifiers().get(index);
	}

	public boolean IS_ClassifierhasStereotype(InstanceSpecification is,
			String stereotype, int index) {
		if (is.getClassifiers().size() != 0) {
			Classifier elt = is.getClassifiers().get(index);
			if (elt != null) {
				if (elt.hasKeyword(stereotype)) {
					return true;
				}
			}
		}
		return false;
	}
}
