/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import java.util.regex.Pattern
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.EStorageClass
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.StorageClass
import org.eclipse.uml2.uml.CallEvent
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Reception
import org.eclipse.uml2.uml.SignalEvent
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.Trigger
import org.eclipse.uml2.uml.profile.standard.Create

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.InterfaceScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.StateMachineScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlOperationServices.*
import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.*
import static extension org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication

class FunctionScript {
	final public static Pattern CLANGUAGE = Pattern.compile("C|C\\/C\\+\\+|c")

	def static genFunctionPrototype(Operation operation) '''
		«FOR comment : operation.ownedComments»
			«comment.genComment» 
		«ENDFOR»
		«operation.genDeclarator»«operation.genReturnArgumentType(true)»«operation.genFunctionDeclarator»(«operation.genArgumentDeclarator(true)»);
		
	'''

	def static genFunctionPrototype(Reception reception) '''
		«FOR comment : reception.ownedComments»
			«comment.genComment» 
		«ENDFOR»
		void «reception.genFunctionDeclarator»(«reception.genArgumentDeclarator»);
		
	'''

	def static genFunctionImplementation(Operation operation) '''
		«CommonScript.genComment(operation)»
		«AcslScript.genPrePostConditions(operation)»
		«operation.genDeclarator()»«operation.genReturnArgumentType(false)»«operation.genFunctionDeclarator()»(«operation.genArgumentDeclarator(false)») {
			«FOR callevent : operation.model.allOwnedElements.filter(CallEvent)»
				«IF (callevent.operation== operation)»
					/* it is an operation of a call event : only one event associated with this operation*/
						«callevent.operation.genDeclarator»«callevent.genCallEventOperationDeclarator»(«callevent.operation.genCallArgumentDeclarator»);		
					/* original code*/			
				«ENDIF»
			«ENDFOR»
			«operation.getBody(CLANGUAGE)»
		}
		
	'''

	def static genFunctionImplementation(Reception reception) {
		var body = ""

		if (reception.owner instanceof Class && (reception.owner as Class).classifierBehavior instanceof StateMachine) {
			val clazz = reception.owner as Class
			var SignalEvent signalEvent = null
			var i = 0;
			var triggers = clazz.classifierBehavior.allOwnedElements.filter(Trigger)

			while (signalEvent === null && i < triggers.size) {
				val trigger = triggers.get(i)
				if (trigger.event instanceof SignalEvent &&
					(trigger.event as SignalEvent).signal === reception.signal) {
					signalEvent = trigger.event as SignalEvent
				}
				i++
			}

			if (signalEvent !== null) {
				body = '''
					«signalEvent.genSignalEventOperationDeclarator»(self, signal);
				'''
			}
		}

		'''
			«FOR comment : reception.ownedComments»
				«comment.genComment()»
			«ENDFOR»
			void «reception.genFunctionDeclarator»(«reception.genArgumentDeclarator») {
				«body»
			}
			
		'''
	}

	def static genCallEventProcessFunctionImplementation(CallEvent callevent, Class clazz) '''
		«FOR comment : callevent.ownedComments»
			«comment.genComment()»
		«ENDFOR»
		«callevent.operation.genDeclarator()»«callevent.operation.genReturnArgumentType(false)»«callevent.genCallEventOperationDeclarator»(«callevent.operation.genArgumentDeclarator(false)») {
			
			«callevent.genProcessMethodBody(clazz)»
		}
		
	'''

	def static genProcessCompletionEventFunctionImplementation(Class clazz) '''
		
		void ProcessCompletionEvent(«clazz.genName()»* self){
			
			«genProcessCompletionEventMethodBody(clazz)»
		}
		
	'''

	def static genSignalEventProcessFunctionImplementation(SignalEvent signalEvent, Class clazz) {
		var triggersClassTransition = false;
		var i = 0;
		val triggers = clazz.allOwnedElements().filter(Trigger)
		while (!triggersClassTransition && i < triggers.size) {
			if (triggers.get(i).event !== null && triggers.get(i).event === signalEvent) {
				triggersClassTransition = true;
			}
			i++
		}

		if (triggersClassTransition) {
			'''
				«FOR comment : signalEvent.ownedComments»
					«comment.genComment()»
				«ENDFOR»
				void «signalEvent.genSignalEventOperationDeclarator»(«clazz.genName()»* self, «signalEvent.signal.genName» *sig) {
					
					«signalEvent.genProcessMethodBody(clazz)»
				}
				
			'''
		}

	}

	def static genFunctionPrototype(CallEvent callEvent) '''
		«FOR comment : callEvent.ownedComments»
			«comment.genComment» 
		«ENDFOR»
		«callEvent.operation.genDeclarator»«callEvent.operation.genReturnArgumentType(true)»«callEvent.genCallEventOperationDeclarator»(«callEvent.operation.genArgumentDeclarator(true)»);
		
	'''

	def static genFunctionPrototype(SignalEvent signalEvent, Class clazz) {
		var triggersClassTransition = false;
		var i = 0;
		val triggers = clazz.allOwnedElements().filter(Trigger)
		while (!triggersClassTransition && i < triggers.size) {
			if (triggers.get(i).event !== null && triggers.get(i).event === signalEvent) {
				triggersClassTransition = true;
			}
			i++
		}

		if (triggersClassTransition) {
			'''
				«FOR comment : signalEvent.ownedComments»
					«comment.genComment» 
				«ENDFOR»
				void «signalEvent.genSignalEventOperationDeclarator»(«clazz.genName()»* self, struct «signalEvent.signal.name» *sig);
				
			'''
		}
	}

	def static genDeclarator(Operation operation) {
		'''«operation.genStorageClass»«operation.genVolatile»«operation.genConstant»'''
	}

	def static genPointer(Operation operation) {
		'''«IF operation.hasStereotype(Ptr)»* «ENDIF»'''
	}

	def static genStorageClass(Operation operation) '''
		«IF operation.hasStereotype(StorageClass)»
			«operation.getStereotypeApplication(StorageClass).storageClass» 
		«ENDIF»
	'''

	def static genConstant(Operation operation) '''
		«IF (operation.hasStereotype(Const))»const «ENDIF»
	'''

	def static genVolatile(Operation operation) '''
		«IF operation.hasStereotype(StorageClass)»
			«IF (operation.getStereotypeApplication(StorageClass).storageClass == EStorageClass.VOLATILE)»volatile «ENDIF»
		«ENDIF»
	'''

	def static genArgumentDeclarator(Operation operation, boolean isHeader) {
		'''
			«IF !operation.isStatic && !operation.hasStereotype(Create)»
				«operation.genClassArgument»«IF operation.getArguments.size > 0», «ENDIF»
			«ENDIF»
			«operation.genArguments(isHeader)»
		'''.toString.trim
	}

	def static genArgumentDeclarator(Reception reception) {
		'''
			«IF (reception.isStatic)»
				«reception.genArguments»
			«ELSE»
				«reception.genClassArgument», «reception.genArguments»
			«ENDIF»
		'''.toString.trim
	}

	def static genCallArgumentDeclarator(Operation operation) {
		'''
			«IF !operation.isStatic && !operation.hasStereotype(Create)»
				self«IF (operation.getArguments.size > 0)», «ENDIF»
			«ENDIF»«operation.genCallArguments»
		'''.toString.trim
	}

	def static genFunctionDeclarator(Operation operation) {
		'''
			«IF operation.hasStereotype(Ptr)»
				(*«operation.genName»)
			«ELSE»
				«operation.genName»
			«ENDIF»
		'''.toString.trim
	}

	def static genFunctionDeclarator(Reception reception) {
		'''
			«IF reception.hasStereotype(Ptr)»
				(*«reception.genName»)
			«ELSE»
				«reception.genName»
			«ENDIF»
		'''.toString.trim
	}

	def static genCallEventOperationDeclarator(CallEvent callevent) {
		'''
			
				«"ProcessCE_" + callevent.operation.genName»
			
		'''.toString.trim
	}

	def static genEntryPrototype(State state) {

		var String name = state.name + "_entry"
		'''
			
			«BehaviorScript.behaviorDeclaration(name,state.entry)»
		'''.toString.trim
	}

	def static genExitPrototype(State state) {
		var String name = state.name + "_exit"
		'''	
			
				«BehaviorScript.behaviorDeclaration(name,state.exit)»	
		'''.toString.trim
	}

	def static genDoActivityPrototype(State state) {
		var String name = state.name + "_doActivity"
		'''	
			
				«BehaviorScript.behaviorDeclaration(name,state.doActivity)»		
		'''.toString.trim
	}

	def static genEntryImplementation(State state) {

		var String name = state.name + "_entry"
		'''
			«BehaviorScript.behaviorImplementation(name, state.entry as OpaqueBehavior)»
		'''.toString.trim
	}

	def static genExitImplementation(State state) {

		var String name = state.name + "_exit"
		'''	
			
				«BehaviorScript.behaviorImplementation(name, state.exit as OpaqueBehavior)»	
		'''.toString.trim
	}

	def static genDoActivityImplementation(State state) {

		var String name = state.name + "_doActivity"
		'''	
			
				«BehaviorScript.behaviorImplementation(name, state.doActivity as OpaqueBehavior)»		
		'''.toString.trim
	}

	def static genSignalEventOperationDeclarator(SignalEvent signalEvent) {
		'''
			
				«"ProcessSE_"+signalEvent.signal.genName»
			
		'''.toString.trim
	}

	def static genClassArgument(Operation operation) {
		'''
			«IF (operation.namespace instanceof Interface)»
				«operation.interface.getInterfaceRealizationClass.map[(it as NamedElement).genName]» *self
			«ELSE»
				«operation.namespace.genName()» *self
			«ENDIF»
		'''.toString.trim
	}

	def static genClassArgument(Reception reception) {
		'''
			«reception.namespace.genName()» *self
		'''.toString.trim
	}

	def static genArguments(Operation operation, boolean isHeader) {
		'''
			«FOR parameter : operation.getArguments SEPARATOR (', ')» «parameter.genType()» «parameter.genPointer()»«parameter.genName()» «ENDFOR»
		'''.toString.trim
	}

	def static genArguments(Reception reception) {
		'''
			struct «reception.signal.genName» *signal
		'''.toString.trim
	}

	def static genCallArguments(Operation operation) {
		'''
			«FOR parameter : operation.getArguments SEPARATOR (', ')»«parameter.genName()» «ENDFOR»
		'''.toString.trim
	}

	/**
	 * Produce return type for function declaration. In case of a constructor, return a pointer to the class struct,
	 * unless otherwise specified
	 *
	 * @param operation a UML operation
	 * @param isHeader if true, generate code for a header file
	 */
	def static genReturnArgumentType(Operation operation, boolean isHeader) '''
		«IF (operation.getReturnResult !== null)»
			«operation.getReturnResult.genType»«operation.getReturnResult.genPointer»
		«ELSEIF operation.hasStereotype(Create)»
			«operation.class_.genName»*
		«ELSE»
			void
		«ENDIF»
	'''

	def static genName(Operation operation) '''
		«IF (operation.isStatic)»
			«operation.genVisibility()»«operation.genCoreName()»
		«ELSE»
			«IF (operation.namespace instanceof Interface)»
				«operation.genVisibility()»«operation.interface.getInterfaceRealizationClass.map[(it as NamedElement).genName]»_«operation.genCoreName()»
			«ELSE»
				«operation.genVisibility()»«operation.namespace.genName()»_«operation.genCoreName()»
			«ENDIF»
		«ENDIF»
	'''
}
