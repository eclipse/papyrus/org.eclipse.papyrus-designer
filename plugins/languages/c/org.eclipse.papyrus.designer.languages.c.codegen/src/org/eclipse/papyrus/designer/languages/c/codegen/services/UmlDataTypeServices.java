/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Property;

public class UmlDataTypeServices {
	/**
	 * Retrieves all the attributes of this datatype object, including those
	 * inherited from its parents. Duplicates named attributes are removed.
	 */
	public EList<Property> getAllAttributes(DataType elt) {
		EList<Property> tmp = new BasicEList<Property>();
		Iterator<Property> it = elt.getAllAttributes().iterator();
		while (it.hasNext()) {
			Property p = it.next();
			Iterator<Property> tmp_it = tmp.iterator();
			boolean found = false;
			while (tmp_it.hasNext() && !found) {
				if (tmp_it.next().getName().equalsIgnoreCase(p.getName())) {
					found = true;
				}
			}
			if (!found) {
				tmp.add(p);
			}
		}
		return tmp;		
	}
}
