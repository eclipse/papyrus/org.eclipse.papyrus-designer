/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.module

import org.eclipse.uml2.uml.CallEvent
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.FinalState
import org.eclipse.uml2.uml.SignalEvent
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.VisibilityKind

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.FunctionScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ClassScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ImportScript.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*

class ClassModuleScript {
	def static classModuleScript(Class clazz) '''
		««« This template is called by the main module file 
		«clazz.genHeading()»
		
		«clazz.genBodyIncludes»

		«clazz.genModuleDeclarationBody()»
		«clazz.genModuleImplementationBody()»
	'''

	/**
	 * Generate the declaration for the body. This includes only private and protected functions
	 * (the header file contains only the public declarations)
	 */
	def static genModuleDeclarationBody(Class clazz) '''
		«IF (clazz.visibility != VisibilityKind.PUBLIC_LITERAL)»
			«clazz.partComment('Private Class Description')»
			// Structure 
			«clazz.genClassStructDeclarations()»
			
			// Constructor and destructor declarations 
			«clazz.genDynamicInstanciationOperationPrototypes()»
			
			// Property initialization declarations
			«clazz.genDefaultInitialisationProtoype()»
			
			// Class method declarations
			«clazz.genNonStaticFunctionDeclarations()»
		«ENDIF»
		
««« ---------------------- private and protected global (static) variable declaration -------------------
		«IF (clazz.ownedAttributes.filter[isStatic && visibility != VisibilityKind.PUBLIC_LITERAL && type !== null].size > 0)»
			«clazz.partComment('private/protected global variable declarations')»
		«ENDIF»
		«FOR attribute : clazz.ownedAttributes»
			«IF (attribute.isStatic && attribute.visibility != VisibilityKind.PUBLIC_LITERAL && attribute.type !== null)»
				// global variable declaration
				«attribute.genVariableDeclaration()»
			«ENDIF»
		«ENDFOR»
		
««« ---------------------- private and protected class function prototypes ------------------------------
		«IF (clazz.ownedOperations.filter[!isStatic && namespace.visibility == VisibilityKind.PUBLIC_LITERAL && visibility != VisibilityKind.PUBLIC_LITERAL].size > 0)»
			«clazz.partComment('Private/protected function declarations')»
		«ENDIF»
		«FOR operation : clazz.ownedOperations»
			«IF (!operation.isStatic && operation.namespace.visibility == VisibilityKind.PUBLIC_LITERAL && operation.visibility != VisibilityKind.PUBLIC_LITERAL)»
				«operation.genFunctionPrototype()»
			«ENDIF»
		«ENDFOR»
		
««« ---------------------- private and protected global (static) function prototypes --------------------
		«IF (clazz.getAllOperations.filter[isStatic && visibility != VisibilityKind.PUBLIC_LITERAL].size > 0)»
			«clazz.partComment('Private/Protected global (static) function declarations')»
		«ENDIF»
		«FOR operation : clazz.getAllOperations»
			«IF (operation.isStatic &&  operation.visibility != VisibilityKind.PUBLIC_LITERAL)»
				«operation.genFunctionPrototype()»
			«ENDIF»
		«ENDFOR»
	'''

	def static genModuleImplementationBody(Class clazz) '''
		
««« ---------------------- implementation of the class constructor and destructor -----------------------
		«clazz.genDynamicInstanciationOperations()»
		
««« ---------------------- initialization function implementations --------------------------------------
		«clazz.genDefaultIntialisationOperation()»
		
««« ---------------------- functions implementations ----------------------------------------------------
		«FOR operation : clazz.operations»
			«operation.genFunctionImplementation()»
		«ENDFOR»

««« ---------------------- receptions implementation ----------------------------------------------------
		«FOR reception : clazz.ownedReceptions»
			«IF reception.signal !== null»«reception.genFunctionImplementation()»«ENDIF»
		«ENDFOR»
		
««« ---------------------- signal event process function implementations --------------------------------
		«IF (clazz.classifierBehavior !== null)»
			«FOR signalEvent : clazz.model.allOwnedElements.filter(SignalEvent)»		
				«signalEvent.genSignalEventProcessFunctionImplementation(clazz)»
			«ENDFOR»
		«ENDIF»
		
««« ---------------------- call event process function implementations ----------------------------------
		«IF (clazz.classifierBehavior !== null)»
			«FOR callevent : clazz.model.allOwnedElements.filter(CallEvent)»
				«IF (callevent.operation !== null && clazz.operations.contains(callevent.operation))»
					«callevent.genCallEventProcessFunctionImplementation(clazz)»
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		
		«IF (clazz.classifierBehavior !== null && !clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
			«var sm = clazz.ownedBehaviors.filter(StateMachine).head»
««« ---------------------- entry, exit and doActivity implementations for each state --------------------
			«IF !sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)].isEmpty»
					«var states = clazz.ownedBehaviors.filter(StateMachine).head.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)]»
							«FOR state : states»		
								«IF (state.entry.isBehaviorExist())»
									«state.genEntryImplementation»
								«ENDIF»
								«IF (state.exit.isBehaviorExist())»
									«state.genExitImplementation»
								«ENDIF»
								«IF (state.doActivity.isBehaviorExist())»
									«state.genDoActivityImplementation»
								«ENDIF»
								«ENDFOR»
									
			«ENDIF»
			«IF(sm.hasTriggerlessTransition())»
««« ----------------------- process completion event for completion transition ---------------------------
				«clazz.genProcessCompletionEventFunctionImplementation»
			«ENDIF»
		«ENDIF»
	'''
}
