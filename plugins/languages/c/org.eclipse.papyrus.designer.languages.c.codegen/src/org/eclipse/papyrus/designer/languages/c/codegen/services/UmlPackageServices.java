/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.c.codegen.Activator;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;


public class UmlPackageServices {

	/**
	 * Minimize an attribute list with type criteria.
	 * 
	 * @param attributeList the list of attributes.
	 * @return ArrayList Minimized list.
	 */
	public ArrayList<Property> getMinimizeAttributeOnType(EList<Property> attributeList)
	{
		if(attributeList == null) {
			return null;
		}
		ArrayList<Property> resultList = new ArrayList<Property>();
		boolean notExistInList = true;
		for(Property attribute : attributeList) {
			for(Property resultAttribute : resultList) {
				if(attribute.getType() == resultAttribute.getType()) {
					notExistInList = false;
				}
			}
			if(notExistInList) {
				resultList.add(attribute);
			}
		}
		return resultList;
	}

	/**
	 * Get minimized list of parameters with type criteria.
	 * 
	 * @param parameterList
	 *        the parameter list.
	 * @return Arraylist minimized parameter list.
	 */
	public ArrayList<Parameter> getMinimizeParameterOnType(EList<Parameter> parameterList)
	{
		if(parameterList == null) {
			return null;
		}
		ArrayList<Parameter> resultList = new ArrayList<Parameter>();
		boolean notExistInList = true;
		for(Parameter parameter : parameterList) {
			for(Parameter resultParameter : resultList) {
				if(parameter.getType() == resultParameter.getType()) {
					notExistInList = false;
				}
			}
			if(notExistInList) {
				resultList.add(parameter);
			}
		}
		return resultList;
	}

	/**
	 * Get the package of the attribute.
	 * 
	 * @param anAttribute
	 *        the attribute.
	 * @return Package the package where is the attribute.
	 */
	public Package getPackageAttribute(Property anAttribute)
	{
		Package resultPackage = null;
		if(anAttribute.getType() == null) {
			return resultPackage;
		}
		if(anAttribute.getClass_().getPackage() != anAttribute.getType()
			.getPackage()) {
			if(!(anAttribute.getType().getPackage() instanceof Model)) {
				resultPackage = anAttribute.getType().getPackage();
			}
		}
		return resultPackage;
	}

	/**
	 * Get the package of the parameter type if it is out of class package.
	 * 
	 * @param aParameter
	 *        a parameter.
	 * @return Package The package of the parameter type.
	 */
	public Package getPackageMethod(Parameter aParameter)
	{
		Package resultPackage = null;
		if(aParameter.getType() == null) {
			return (resultPackage);
		}
		if(aParameter.getOperation().getClass_().getPackage() != aParameter
			.getType().getPackage()) {
			if(!(aParameter.getType().getPackage() instanceof Model)) {
				resultPackage = aParameter.getType().getPackage();
			}
		}
		return resultPackage;
	}

	/**
	 * Minimize an classifier list.
	 * 
	 * @param classifierList
	 *        the classifier list.
	 * @param aClassifier
	 *        the reference classifier.
	 * @return ArrayList ret element is not in classifier package.
	 */
	public ArrayList<Classifier> minimizeClassNoInPackage(EList<Classifier> classifierList,
		Classifier aClassifier) {
		Namespace namespaceClass = aClassifier.getNamespace();// getPackage();
		if(classifierList == null) {
			Activator.log.debug("Classifier list null"); //$NON-NLS-1$
			return null;
		}
		ArrayList<Classifier> resultList = new ArrayList<Classifier>();
		for(EObject classifier : classifierList) {
			Classifier classTarget = (Classifier)classifier;
			if(classTarget.getNamespace() != namespaceClass) {
				resultList.add(classTarget);
			}
		}
		// System.out.println("Liste :" + resultList);
		return resultList;
	}
}
