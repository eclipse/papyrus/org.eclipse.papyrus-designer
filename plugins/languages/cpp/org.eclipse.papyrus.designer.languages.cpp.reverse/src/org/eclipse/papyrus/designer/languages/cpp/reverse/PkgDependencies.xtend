package org.eclipse.papyrus.designer.languages.cpp.reverse

import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.Usage
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ReverseUtils.*

/**
 * Create package level dependencies, i.e. from nearest package to the other nearest package.
 * Place these dependencies in a package "dependencies"
 */
class PkgDependencies {

	static String depencyPkgName = "dependencies"

	/**
	 * create a dependency (Usage) from the classifier that owns the examined operation to the passed supplier. The relationship
	 * is created in the nearest package.
	 * @param the target of the usage.
	 */
	static def void createDependency(NamedElement client, NamedElement supplier) {
		// val dependencyPkg = dependencyPkg
		if (client !== null && supplier !== null) {
			val from = client.nearestPackage
			val to = supplier.nearestPackage
			if (from != to) {
				// todo: not too slow?
				var usages = from.ownedElements.filter(typeof(Usage)).toList
				var usage = usages.filter[it.suppliers.contains(to)].head
				if (usage === null) {
					usage = from.createUsage(to)
					usage.setXmlID
					// dependencyPkg.packagedElements.add(usage)
				}
			}
		} else {
			System.err.println("should not happen")
		}
	}

	static def getDependencyPkg() {
		var dependencyPkg = ReverseData.current.dependencyPkg;
		if (dependencyPkg === null) {
			val root = ReverseData.current.models.get(0)
			dependencyPkg = root.getMember(depencyPkgName) as Package
			if (dependencyPkg === null) {
				dependencyPkg = root.createNestedPackage(depencyPkgName)
				ReverseData.current.dependencyPkg = dependencyPkg
			}
		}
		return dependencyPkg
	}
}
