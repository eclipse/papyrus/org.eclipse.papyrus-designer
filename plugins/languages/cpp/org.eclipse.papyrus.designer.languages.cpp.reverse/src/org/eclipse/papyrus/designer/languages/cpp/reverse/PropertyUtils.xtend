/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   Van Cam Pham (CEA LIST) <vancam.pham@cea.fr> - Reverse implementation
 *   Ansgar Radermacher (CEA LIST) - Larger refactoring
 *******************************************************************************/
 
 package org.eclipse.papyrus.designer.languages.cpp.reverse

import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration
import org.eclipse.cdt.core.model.IField
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Mutable
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Volatile
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.TypeOperationsEnhanced
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.AggregationKind
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.PackageableElement
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.UMLFactory
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ASTUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ReverseUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.CommentUtils.*
import org.eclipse.uml2.uml.NamedElement

/**
 * functions to create and update properties
 */
class PropertyUtils {
	static def createProperty(IField field, Classifier classifier) {
		var prop = UMLFactory.eINSTANCE.createProperty => [
			it.name = field.elementName
		]
		if (classifier instanceof DataType) {
			(classifier as DataType).ownedAttributes += prop
		} else if (classifier instanceof Class) {
			(classifier as Class).ownedAttributes += prop
		}
		prop.setXmlID
		updateProperty(field, prop)
	}

	static def updateProperty(IField field, Property prop) {
		prop.unapplyAllStereotypes()

		var Type type = null
		var doNotAnalyzeDeclaration = false

		try {
			// Check the field's rawSignature to see if it's an anonymous function pointer
			var fieldNode = field.findEnclosingNode
			if (fieldNode !== null) {
				var String rawSignature = fieldNode.rawSignature.replaceAll("\\n", "").replaceAll("\\r", "").
					replaceAll(";", "").replaceAll("\\s+", " ").trim
				// Unescaped regex: (\()(\s*)(\*)(.*)(\))(\s*)(\()(.*)(\))
				var Pattern pattern = Pattern.compile("(\\()(\\s*)(\\*)(.*)(\\))(\\s*)(\\()(.*)(\\))");
				var Matcher matcher = pattern.matcher(rawSignature);
				if (matcher.find()) {
					var String typeName = rawSignature.replaceFirst(Pattern.quote(field.elementName), "typeName").
						replaceFirst("typedef", "");

					var PackageableElement packageable = null
					if (prop.owner instanceof Class) {
						packageable = prop.owner as Class
					} else {
						packageable = prop.nearestPackage
					}
					val String ownerName = packageable.name

					var primitiveType = UMLFactory.eINSTANCE.createPrimitiveType => [
						it.name = ownerName + "_" + field.elementName + "_funcptr"
					]

					if (packageable instanceof Package) {
						(packageable as Package).ownedTypes += primitiveType
					} else {
						(packageable as Class).nestedClassifiers += primitiveType
					}

					StereotypeUtil.apply(primitiveType, Typedef)
					UMLUtil.getStereotypeApplication(primitiveType, Typedef).definition = typeName

					type = primitiveType
					doNotAnalyzeDeclaration = true
				}
			}

			// Was not an anon function pointer, check if it's an anon struct or enum
			if (type === null) {
				if (field.typeName.equals("enum") || field.typeName.equals("struct") ||
					field.typeName.equals("class")) {
					// TODO Bug: rawSignature is only name of field for anonymouse struct and class
					// This is because field.source returns only the name of the field
					// For an anonymous enum, it returns the enum as well...
					var rawSignature = fieldNode.rawSignature
					var trimmedRawSignature = rawSignature.replaceAll("\\n", "").replaceAll("\\r", "").
						replaceAll(";", "").replaceAll("\\s+", " ").trim

					// Unescaped pattern: ({)(.*)(})
					var Pattern pattern = Pattern.compile("(\\{)(.*)(\\})");
					var Matcher matcher = pattern.matcher(trimmedRawSignature);
					if (matcher.find()) {
						var String[] tokens = rawSignature.split("}")
						if (tokens.size > 0) {
							var String lastToken = tokens.get(tokens.size - 1)
							if (lastToken.contains(field.elementName)) {
								lastToken = lastToken.replaceFirst(Pattern.quote(field.elementName), "typeName")
							}

							var String typeName = ""
							for (i : 0 ..< tokens.size - 1) {
								typeName += tokens.get(i)
							}
							typeName += lastToken

							var PackageableElement packageable = null
							if (prop.owner instanceof Class) {
								packageable = prop.owner as Class
							} else {
								packageable = prop.nearestPackage
							}
							val String ownerName = packageable.name

							var primitiveType = UMLFactory.eINSTANCE.createPrimitiveType => [
								it.name = ownerName + "_" + field.elementName + "_anon_" + field.typeName
							]

							if (packageable instanceof Package) {
								(packageable as Package).ownedTypes += primitiveType
							} else {
								(packageable as Class).nestedClassifiers += primitiveType
							}

							StereotypeUtil.apply(primitiveType, Typedef)
							UMLUtil.getStereotypeApplication(primitiveType, Typedef).definition = typeName

							type = primitiveType
							doNotAnalyzeDeclaration = true
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace
		}

		if (type === null) {
			type = field.qualifiedName.getUMLType(field)
		}

		prop.type = type
		prop.name = field.elementName
		if (!doNotAnalyzeDeclaration) {
			var node = getSelector(field.translationUnit).findEnclosingNode(field.sourceRange.startPos,
				field.sourceRange.length)
			if (node instanceof IASTSimpleDeclaration) {
				node.declarators.analyzeDeclaration(prop.type, prop, Cpp_LangID)
			}
			if (prop.owner instanceof NamedElement) {
				PkgDependencies.createDependency(prop.owner as NamedElement, type)
			}
			prop.addComment(node)
		}

		prop.isStatic = field.isStatic
		prop.visibility = field.visibility.convertVisibility
		prop.applyStereotype(field.const, Const)
		prop.applyStereotype(field.volatile, Volatile)
		prop.applyStereotype(field.mutable, Mutable)

		var createAssociations = false;
		if (createAssociations) {
			var asso = TypeOperationsEnhanced.createAssociationFromProperty(prop, true, AggregationKind.NONE_LITERAL,
				false, AggregationKind.NONE_LITERAL, (prop.eContainer as Classifier).name.toFirstLower, 1, 1)
			asso.name = "A_" + prop.name + "_" + (prop.eContainer as Classifier).name.toFirstLower
		}
		if (prop.isSimpleAssociation) {
			prop.aggregation = AggregationKind.NONE_LITERAL
		} else if (prop.isAggregation) {
			prop.aggregation = AggregationKind.SHARED_LITERAL
		} else if (prop.isComposition) {
			prop.aggregation = AggregationKind.COMPOSITE_LITERAL
		}
	}
	
}