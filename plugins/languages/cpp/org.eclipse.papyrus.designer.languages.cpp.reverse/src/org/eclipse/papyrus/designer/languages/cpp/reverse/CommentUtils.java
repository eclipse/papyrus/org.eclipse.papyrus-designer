package org.eclipse.papyrus.designer.languages.cpp.reverse;

import java.util.regex.Pattern;

import org.eclipse.cdt.core.dom.ast.IASTComment;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.model.ISourceReference;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;

/**
 * Utility classes around comments retrieved from source code
 */
public class CommentUtils {

	/**
	 * Cleanup multi-line comments starting with the double start, having an
	 * indented star in intermediate lines and ending with star+slash The method
	 * will remove the start and end tag as well as intermediate stars
	 * 
	 * @param comment original comment
	 * @return cleaned-up comment
	 */
	public static String cleanupComment(String comment) {
		return comment.replaceFirst(Pattern.quote(StringConstants.DOC_START), StringConstants.EMPTY)
				.replace(StringConstants.COMMENT_END, StringConstants.EMPTY)
				.replaceAll("\n[ \t]* ", StringConstants.EOL). //$NON-NLS-1$
				trim();
	}

	/**
	 * Return the comment that is preceding a declaration in the sense that it ends
	 * in the line before the passed node.
	 * 
	 * @param node an ASTNode
	 * @return a comment or null, if none is found
	 */
	public static String getPreceedingComment(IASTNode node) {
		for (IASTComment comment : node.getTranslationUnit().getComments()) {
			// get comment from previous line?
			if (comment.getFileLocation().getEndingLineNumber() == node.getFileLocation().getStartingLineNumber() - 1) {
				return new String(comment.getComment());
			}
		}
		return null;
	}

	/**
	 * Add a comment to a UML element from a source reference
	 * 
	 * @param element   a UML element
	 * @param sourceRef the source reference
	 */
	public static void addComment(Element element, ISourceReference sourceRef) {
		IASTNode node = ASTUtils.findEnclosingNode(sourceRef);
		addComment(element, node);
	}

	/**
	 * Add a comment to a UML element from a given AST node
	 * 
	 * @param element a UML element
	 * @param node    an AST node having eventually a preceding comment
	 */
	public static void addComment(Element element, IASTNode node) {
		String comment = CommentUtils.getPreceedingComment(node);
		if (comment != null) {
			Comment umlComment = element.createOwnedComment();
			umlComment.setBody(CommentUtils.cleanupComment(comment));
		}
	}
}
