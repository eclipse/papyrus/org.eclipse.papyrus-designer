/*******************************************************************************
 * Copyright (c) 2016, 2022 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   Van Cam Pham (CEA LIST) <vancam.pham@cea.fr> - Reverse implementation
 *   Ansgar Radermacher (CEA LIST) - Larger refactoring
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.reverse

import java.util.List
import org.eclipse.cdt.core.dom.ast.IASTFunctionDeclarator
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition
import org.eclipse.cdt.core.dom.ast.IASTNode
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTCompositeTypeSpecifier
import org.eclipse.cdt.core.model.ICElement
import org.eclipse.cdt.core.model.IDeclaration
import org.eclipse.cdt.core.model.IEnumeration
import org.eclipse.cdt.core.model.IField
import org.eclipse.cdt.core.model.IFunction
import org.eclipse.cdt.core.model.IInclude
import org.eclipse.cdt.core.model.IMacro
import org.eclipse.cdt.core.model.IMethod
import org.eclipse.cdt.core.model.IMethodDeclaration
import org.eclipse.cdt.core.model.IParent
import org.eclipse.cdt.core.model.ISourceReference
import org.eclipse.cdt.core.model.IStructure
import org.eclipse.cdt.core.model.IStructureTemplate
import org.eclipse.cdt.core.model.ITranslationUnit
import org.eclipse.cdt.core.model.ITypeDef
import org.eclipse.cdt.core.model.IUsing
import org.eclipse.core.runtime.IPath
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Template
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Visibility
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.ClassifierTemplateParameter
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Generalization
import org.eclipse.uml2.uml.Model
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.PrimitiveType
import org.eclipse.uml2.uml.RedefinableTemplateSignature
import org.eclipse.uml2.uml.TemplateSignature
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.VisibilityKind
import org.eclipse.uml2.uml.util.UMLUtil

import static org.eclipse.papyrus.designer.languages.cpp.reverse.MethodUtils.*

import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ASTUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ReverseUtils.*

/**
 * Pass2: refine classifiers ((attributes and methods) and relationships between them
 * The objective of having two passes is that all type references (set in pass2) and forward declared elements are already
 * available in the UML model
 */
class GetOrCreateP2 {
	/**
	 * Refine the classifiers within a parent (called initially with a translation unit, then recursively)
	 * @param model the model to get/create classifiers in
	 * @param parent the CDT model parent
	 */
	static def void getOrCreateClassifiers(IParent parent) throws Exception {
		var ICElement[] children = parent.safeGetChilds
		for (var i = 0; i < children.length; i++) {
			var child = children.get(i)
			if (child.isActive) {
				// don't analyze inactive code, as AST is no available (TODO: clarify options/requirements)
				switch (child) {
					case child instanceof IStructure,
					case child instanceof IEnumeration: {
						getOrCreateClassifier(child)
					}
					case child instanceof IMethod: {
						// batch reverse of method bodies that are not within a structure
						if ((child as IMethod).translationUnit.isHeaderUnit && !(child.parent instanceof IStructure)) {
							val IMethod method = child as IMethod

							val type = getTypeOfVariableOrMethod(method, method.translationUnit)

							if (type !== null) {
								var batchReverser = new BatchReverseFunctionBody(method.translationUnit,
									method.CProject.elementName, type)

								// function declaration is a superclass for method declaration
								// (but need to trace functions differently?)
								var String name = method.elementName
								var IASTNode node = method.findEnclosingNode
								if (node instanceof IASTFunctionDefinition) {
									var IASTFunctionDefinition definition = node as IASTFunctionDefinition
									var IASTFunctionDeclarator declarator = definition.getDeclarator();
									var String body = getBody(method);

									var Operation operation = batchReverser.updateMethod(method, node, 0, parent, name,
										body, declarator)
									if (operation !== null) {
										// new DependencyAnalysis(operation, definition, method.translationUnit).
										//	analyzeDependencies()
									}
								}
							}
						}
					}
					case child instanceof IFunction: {
//					var func = child as IFunction
//					var parentPack = func.translationUnit.containerPackage
//					var Class classifier = null
//					if (parentPack.getOwnedMember(func.translationUnit.elementName) === null) {
//						parentPack.createOwnedClass(func.translationUnit.elementName, false)
//					}
//					classifier = parentPack.getOwnedMember(func.translationUnit.elementName) as Class
//					classifier.createOwnedOperation(func.elementName, null, null)
					}
					case child instanceof IParent: {
						getOrCreateClassifiers(child as IParent)
					}
					case child instanceof ITypeDef: {
						getOrCreateClassifier(child)
					}
					case child instanceof IUsing: {
						// IUsing is only used for "using namespace", handled internally by CDT
					}
					case child instanceof IInclude: {
						val headerUnit = (child as IInclude).getTranslationUnitFromInclude(ReverseData.current.project)
						if (headerUnit !== null && headerUnit.sourceFolder !== null) {
							ReverseCpp2Uml.currentCpp2Uml.reverseHeader(headerUnit)
						}
					}
				}
			}
		}
	}

	/*
	 * Create classifier for a declaration, either a (template) class, an enumeration, struct/union or typedefs, using statements
	 * The function also creates nested classifiers, if present. For instance a typedef or using statements within a class.
	 * It takes care of creating the nested elements at the right moment, e.g. before creating attributes and methods that eventually
	 * reference these types.
	 * 
	 * @param container the owner to create a classifier in, must be a class or package
	 * @return the associated UML type
	 */
	static def Classifier getOrCreateClassifier(ICElement ideclaration) {
 
		val map = ReverseData.current.map
		val analyzeMap = ReverseData.current.analyzeMap

		if (ideclaration.elementType != ICElement.C_CLASS && ideclaration.elementType != ICElement.C_TEMPLATE_CLASS &&
			ideclaration.elementType != ICElement.C_STRUCT &&
			ideclaration.elementType != ICElement.C_UNION && ideclaration.elementType != ICElement.C_TEMPLATE_STRUCT) {
			return null
		}

		var Classifier existing = null

		if (map.get(ideclaration) instanceof Classifier) {
			existing = map.get(ideclaration) as Classifier
		}
		if (analyzeMap.containsKey(ideclaration)) {
			return existing
		}
		analyzeMap.put(ideclaration, true)

		ReverseData.current.monitor.subTask("Completing type " + ideclaration.elementName)

		val iStructure = ideclaration as IStructure
		if (existing instanceof Class) {
			var Class classifier = existing as Class
			classifier.addSuperTypes(iStructure)

			// now create nested definitions (before adding properties and methods)
			// iStructure.getOrCreateClassifiers

			for (child : iStructure.children) {
				if (child instanceof IField) {
					PropertyUtils.createProperty(child, classifier)
				} else if (child instanceof IMethodDeclaration) {
					createMethod(child, classifier)
				}
			}

			if (iStructure.elementType == ICElement.C_TEMPLATE_CLASS) {
				var istructureTemplate = iStructure as IStructureTemplate
				val template = StereotypeUtil.applyApp(classifier, Template)
				template.declaration = istructureTemplate.templateSignature
			}
			return classifier

		} else if (existing instanceof DataType) {
			var DataType dataType = existing as DataType
			dataType.addSuperTypes(iStructure)

			// now create nested definitions (before adding properties)
			iStructure.getOrCreateClassifiers

			for (child : iStructure.children) {
				if (child.elementType == ICElement.C_FIELD) {
					PropertyUtils.createProperty(child as IField, dataType)
				}
			}
			return dataType
		}
	}

	/**
	 * Add superTypes to a classifier
	 */
	static def addSuperTypes(Classifier classifier, ISourceReference sourceRef) {
		val astNode = sourceRef.findEnclosingNode
		if (astNode instanceof ICPPASTCompositeTypeSpecifier) {
			for (superTypeAST : astNode.getBaseSpecifiers) {
				val qName = superTypeAST.nameSpecifier.qualifiedNameFromNSpec
				val superType = qName.getUMLType(sourceRef as ICElement)
				if (superType instanceof Classifier) {
					PkgDependencies.createDependency(classifier, superType)
					var Generalization generalization = classifier.createGeneralization(superType)
					val visibility = superTypeAST.visibility.convertVisibility
					if (visibility != VisibilityKind.PUBLIC_LITERAL) {
						val visibilitySt = StereotypeUtil.applyApp(generalization, Visibility)
						visibilitySt.value = visibility.literal.toLowerCase
					}
				}
			}
		}
	}

	/**
	 * Get or create the Include stereotype of a classifier.
	 * TODO: currently unused, can remove? (originally getOrCreateTypes), when to call?
	 * @param iStructure, a structure or enumeration
	 * */
	static def getOrCreateIncludes(IDeclaration iStructure, IParent parent, Model model) {
		var List<Type> ret = new UniqueEList<Type>
		ReverseData.current.monitor.subTask("Parsing type " + iStructure.elementName)
		var Classifier tempType = null
		var IDeclaration istructure = iStructure as IDeclaration
		// istructure.elementType
		var elementName = istructure.elementName
		var container = istructure.container
		tempType = container.getOwnedMember(elementName) as Classifier
		if (tempType === null) {
			tempType = getOrCreateClassifier(istructure) as Classifier
		}

		if (tempType !== null) {
			ret.add(tempType)
			if (istructure instanceof IParent) {
				var List<ICElement> nestedStructures = new UniqueEList
				nestedStructures.addAll(istructure.children.filter(typeof(IDeclaration)))
				nestedStructures.addAll(istructure.children.filter(typeof(ITypeDef)))
				for (i : 0 ..< nestedStructures.size) {
					var ns = nestedStructures.get(i)
					if (!ns.elementName.trim.empty) { // TODO anonymous struct?
						var t = getOrCreateClassifier(ns) as Type
						if (t !== null) {
							ret.add(t)
						}
					} else if (ns instanceof IEnumeration) {
						var isAnonEnumInstance = false

						try {
							if (i + 1 < nestedStructures.size) {
								var nextNs = nestedStructures.get(i + 1)
								if (nextNs instanceof IField) {
									var nsRaw = ns.findEnclosingNode.rawSignature
									var nextNsRaw = nextNs.findEnclosingNode.rawSignature

									if (nextNsRaw.contains(nsRaw)) {
										isAnonEnumInstance = true
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace
						}

						if (!isAnonEnumInstance) {
							var t = getOrCreateClassifier(ns) as Type
							if (t !== null) {
								ret.add(t)
							}
						}
					}
				}
			}
		}

		if (istructure instanceof IStructureTemplate) {
			var istructureTemplate = istructure as IStructureTemplate
			for (var ti = 0; ti < istructureTemplate.templateParameterTypes.size; ti++) {
				tempType.getOrCreateTemplateParameter(istructureTemplate.templateParameterTypes.get(ti), "class")

			}
		}

		// Includes handling
		if (ret.size > 0) {
			if (!(tempType instanceof Enumeration) && !(tempType instanceof PrimitiveType)) {
				// Get the ITranslationUnit that contains the temporally created type
				// If no file is found previously, we set it to parent by default
				var file = parent;
				if (!StereotypeUtil.isApplied(tempType, Include)) {
					StereotypeUtil.apply(tempType, Include)
				}
				val include = UMLUtil.getStereotypeApplication(tempType, Include)
				if (include !== null) {
					var header = include.header
					var escape = ""

					for (j : file.children) {
						if (j instanceof IInclude) {
							if (!header.contains(j.elementName)) {
								if (j.elementName.contains("Pkg_")) {
									try {
										var includeTokens = j.elementName.split("/")
										var String includeTrimmed = ""
										for (i : 0 ..< includeTokens.length - 1) {
											includeTrimmed += includeTokens.get(i)
										}

										var typeQualifiedNameTokens = tempType.qualifiedName.split("::")
										var String typeQualifiedNameTrimmed = ""
										for (i : 1 ..< typeQualifiedNameTokens.length - 1) {
											typeQualifiedNameTrimmed += typeQualifiedNameTokens.get(i)
										}

										if (!includeTrimmed.equals(typeQualifiedNameTrimmed)) {
											header = header + escape + "#include " + "\"" + j.elementName + "\""
											escape = "\n"
										}
									} catch (Exception e) {
										e.printStackTrace
										header = header + escape + "#include " + "\"" + j.elementName + "\""
										escape = "\n"
									}
								} else {
									header = header + escape + "#include " + "\"" + j.elementName + "\""
									escape = "\n"
								}
							}

						} else if (j instanceof IMacro) {
							if (!header.contains(j.elementName)) {
								var IASTNode node = j.findEnclosingNode
								if (node !== null) {
									val nodeString = node.toString
									var value = nodeString.substring(nodeString.indexOf("=") + 1)
									if (value === null || value.equals("")) {
										if (!j.elementName.contains(tempType.name.replaceAll(" ", "").toUpperCase)) {
											header = header + escape + "#define " + j.elementName + " " + value
											escape = "\n"
										}
									} else {
										header = header + escape + "#define " + j.elementName + " " + value
										escape = "\n"
									}

								}
							}
						}
					}
					include.header = header

					// Body and pre-body
					// The strategy is to find a file in the same folder with the same name by with cpp extension
					// It's very specific to generated code, but so is how body and pre-body handled (based on generated comments)
					if (file instanceof ITranslationUnit) {
						try {
							var IPath sourcePath = file.path.removeFileExtension.addFileExtension("cpp")
							var ICElement sourceFile = (file as ITranslationUnit).CProject.findElement(sourcePath)
							if (sourceFile instanceof ITranslationUnit) {
								BatchReverseFunctionBody.updateCppInclude(sourceFile, tempType)
							}
						} catch (Exception e) {
							// Fail silently
						}
					}
				}
			}
		}

		return ret
	}

	/**
	 * Create a template parameter within a template signature. If the
	 * signature does not exist, it will be created.
	 * TODO: dead code, only used by getOrCreateIncludes
	 */
	static def Classifier getOrCreateTemplateParameter(NamedElement element, String parameterTypeName, String keyWord) {
		var Classifier ret = null
		var TemplateSignature templateSignature
		if (element instanceof Classifier) {
			var classifier = element as Classifier
			if (classifier.ownedTemplateSignature === null ||
				!(classifier.ownedTemplateSignature instanceof RedefinableTemplateSignature)) {
				templateSignature = classifier.createOwnedTemplateSignature(
					UMLPackage.Literals.REDEFINABLE_TEMPLATE_SIGNATURE) as RedefinableTemplateSignature
				(templateSignature as RedefinableTemplateSignature).name = TEMPLATE_PARAMETER_SIGNATURE_NAME
			}
			templateSignature = classifier.ownedTemplateSignature as RedefinableTemplateSignature
		} else if (element instanceof Operation) {
			var operation = element as Operation
			if (operation.ownedTemplateSignature === null ||
				!(operation.ownedTemplateSignature instanceof TemplateSignature)) {
				templateSignature = operation.createOwnedTemplateSignature(
					UMLPackage.Literals.TEMPLATE_SIGNATURE) as TemplateSignature
			}
			templateSignature = operation.ownedTemplateSignature as TemplateSignature
		} else {
			return null
		}

		var classifierTemplates = templateSignature.ownedParameters.filter(typeof(ClassifierTemplateParameter))
		var classifierTemplatesContainClassifier = classifierTemplates.filter [
			it.ownedParameteredElement instanceof Classifier
		]
		var containedClassifiers = classifierTemplatesContainClassifier.map[it.ownedParameteredElement]
		ret = containedClassifiers.filter(typeof(Classifier)).filter[it.name.equals(parameterTypeName)].head
		if (ret === null) {
			var classifierTemplate = templateSignature.createOwnedParameter(
				UMLPackage.Literals.CLASSIFIER_TEMPLATE_PARAMETER) as ClassifierTemplateParameter
			ret = classifierTemplate.createOwnedParameteredElement(UMLPackage.Literals.CLASS) as Classifier
			ret.name = parameterTypeName
			classifierTemplate.addKeyword(keyWord)
		}

		return ret
	}

	/**
	 * Get an existing classifier, i.e. do not create, if it does not exist yet
	 */
	static def Type getClassifier(ICElement declaration) throws Exception {
		val map = ReverseData.current.map
		if (map.get(declaration) !== null && map.get(declaration) instanceof Type) {
			return map.get(declaration) as Type
		}
		var Type ret = getOrCreateClassifier(declaration) as Type
		return ret;
	}
}
