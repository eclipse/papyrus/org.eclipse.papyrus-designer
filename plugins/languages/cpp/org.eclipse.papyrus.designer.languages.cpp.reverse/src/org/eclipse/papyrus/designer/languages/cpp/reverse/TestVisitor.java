package org.eclipse.papyrus.designer.languages.cpp.reverse;

import org.eclipse.cdt.core.dom.IPDOMNode;
import org.eclipse.cdt.core.dom.IPDOMVisitor;
import org.eclipse.cdt.internal.core.pdom.dom.PDOMNamedNode;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

/**
 * Visitor to find named nodes in a BTree or below a PDOMNode. Nested nodes are not visited.
 * 
 * @since 4.0
 */
public class TestVisitor implements IPDOMVisitor {
	private final char[] name;
	private IProgressMonitor monitor;
	private int monitorCheckCounter;

	/**
	 * Collects all nodes with given name, passing the filter.
	 *
	 * @param linkage
	 * @param name
	 * @param prefixLookup
	 *            If set to <code>true</code> a binding is considered if its name starts with the given prefix
	 *            Otherwise, the binding will only be considered if its name matches exactly. This parameter
	 *            is ignored if <code>contentAssistLookup</code> is true.
	 * @param contentAssistLookup
	 *            If set to <code>true</code> a binding is considered if its names matches according to the
	 *            current content assist matching rules.
	 * @param caseSensitive
	 *            Ignored if <code>contentAssistLookup</code> is true.
	 */
	public TestVisitor(char[] name) {
		this.name = name;
		this.monitor = new NullProgressMonitor();
	}

	/**
	 * Allows to cancel a visit. If set a visit may throw an OperationCancelledException.
	 * 
	 * @since 4.0
	 */
	public void setMonitor(IProgressMonitor pm) {
		monitor = pm;
	}

	@Override
	final public boolean visit(IPDOMNode node) throws CoreException {
		if (monitor != null)
			checkCancelled();

		if (node instanceof PDOMNamedNode) {
			PDOMNamedNode pb = (PDOMNamedNode) node;
			if (pb.getDBName().equals(name)) {
				System.err.println(pb);
			}
		}
		return false; // don't visit children
	}

	private void checkCancelled() {
		if (++monitorCheckCounter % 0x1000 == 0 && monitor.isCanceled()) {
			throw new OperationCanceledException();
		}
	}

	@Override
	final public void leave(IPDOMNode node) throws CoreException {
	}
}
