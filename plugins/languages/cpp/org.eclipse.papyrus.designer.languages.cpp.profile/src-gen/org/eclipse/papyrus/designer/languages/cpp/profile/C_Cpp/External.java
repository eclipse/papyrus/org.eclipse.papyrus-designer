/**
 * Copyright (c) 2013 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    CEA LIST - Initial API and implementation
 *
 */
package org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * External classes reference existing C++ classes, but are typcially empty in the model. An include directive is generated if classes depend on these.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.External#getIncPath <em>Inc Path</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.C_CppPackage#getExternal()
 * @model
 * @generated
 */
public interface External extends org.eclipse.papyrus.designer.languages.common.profile.Codegen.External {
	/**
	 * Returns the value of the '<em><b>Inc Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inc Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Path to use in include directives when this external type is referenced. If unset, no additional
	 * include directive is generated for the usage of this type. The latter may be useful if the
	 * external type is used in conjunctions with others and a single header include provides
	 * definitions for all required types
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Inc Path</em>' attribute.
	 * @see #setIncPath(String)
	 * @see org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.C_CppPackage#getExternal_IncPath()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getIncPath();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.External#getIncPath <em>Inc Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inc Path</em>' attribute.
	 * @see #getIncPath()
	 * @generated
	 */
	void setIncPath(String value);

} // External
