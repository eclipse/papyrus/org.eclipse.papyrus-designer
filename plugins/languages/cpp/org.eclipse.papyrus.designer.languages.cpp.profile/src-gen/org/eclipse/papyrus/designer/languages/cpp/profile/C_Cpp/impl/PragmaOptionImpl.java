/**
 */
package org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.C_CppPackage;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.PragmaOption;

import org.eclipse.uml2.uml.Classifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pragma Option</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.impl.PragmaOptionImpl#getBase_Classifier <em>Base Classifier</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.impl.PragmaOptionImpl#getPreDecl <em>Pre Decl</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.impl.PragmaOptionImpl#getPostDecl <em>Post Decl</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PragmaOptionImpl extends MinimalEObjectImpl.Container implements PragmaOption {
	/**
	 * The cached value of the '{@link #getBase_Classifier() <em>Base Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Classifier()
	 * @generated
	 * @ordered
	 */
	protected Classifier base_Classifier;

	/**
	 * The default value of the '{@link #getPreDecl() <em>Pre Decl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreDecl()
	 * @generated
	 * @ordered
	 */
	protected static final String PRE_DECL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPreDecl() <em>Pre Decl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreDecl()
	 * @generated
	 * @ordered
	 */
	protected String preDecl = PRE_DECL_EDEFAULT;

	/**
	 * The default value of the '{@link #getPostDecl() <em>Post Decl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostDecl()
	 * @generated
	 * @ordered
	 */
	protected static final String POST_DECL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPostDecl() <em>Post Decl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostDecl()
	 * @generated
	 * @ordered
	 */
	protected String postDecl = POST_DECL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PragmaOptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return C_CppPackage.Literals.PRAGMA_OPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Classifier getBase_Classifier() {
		if (base_Classifier != null && base_Classifier.eIsProxy()) {
			InternalEObject oldBase_Classifier = (InternalEObject)base_Classifier;
			base_Classifier = (Classifier)eResolveProxy(oldBase_Classifier);
			if (base_Classifier != oldBase_Classifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
			}
		}
		return base_Classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier basicGetBase_Classifier() {
		return base_Classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Classifier(Classifier newBase_Classifier) {
		Classifier oldBase_Classifier = base_Classifier;
		base_Classifier = newBase_Classifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER, oldBase_Classifier, base_Classifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPreDecl() {
		return preDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPreDecl(String newPreDecl) {
		String oldPreDecl = preDecl;
		preDecl = newPreDecl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, C_CppPackage.PRAGMA_OPTION__PRE_DECL, oldPreDecl, preDecl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPostDecl() {
		return postDecl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPostDecl(String newPostDecl) {
		String oldPostDecl = postDecl;
		postDecl = newPostDecl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, C_CppPackage.PRAGMA_OPTION__POST_DECL, oldPostDecl, postDecl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER:
				if (resolve) return getBase_Classifier();
				return basicGetBase_Classifier();
			case C_CppPackage.PRAGMA_OPTION__PRE_DECL:
				return getPreDecl();
			case C_CppPackage.PRAGMA_OPTION__POST_DECL:
				return getPostDecl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER:
				setBase_Classifier((Classifier)newValue);
				return;
			case C_CppPackage.PRAGMA_OPTION__PRE_DECL:
				setPreDecl((String)newValue);
				return;
			case C_CppPackage.PRAGMA_OPTION__POST_DECL:
				setPostDecl((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER:
				setBase_Classifier((Classifier)null);
				return;
			case C_CppPackage.PRAGMA_OPTION__PRE_DECL:
				setPreDecl(PRE_DECL_EDEFAULT);
				return;
			case C_CppPackage.PRAGMA_OPTION__POST_DECL:
				setPostDecl(POST_DECL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case C_CppPackage.PRAGMA_OPTION__BASE_CLASSIFIER:
				return base_Classifier != null;
			case C_CppPackage.PRAGMA_OPTION__PRE_DECL:
				return PRE_DECL_EDEFAULT == null ? preDecl != null : !PRE_DECL_EDEFAULT.equals(preDecl);
			case C_CppPackage.PRAGMA_OPTION__POST_DECL:
				return POST_DECL_EDEFAULT == null ? postDecl != null : !POST_DECL_EDEFAULT.equals(postDecl);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (preDecl: "); //$NON-NLS-1$
		result.append(preDecl);
		result.append(", postDecl: "); //$NON-NLS-1$
		result.append(postDecl);
		result.append(')');
		return result.toString();
	}

} //PragmaOptionImpl
