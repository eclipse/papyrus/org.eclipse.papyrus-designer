/*******************************************************************************
 * Copyright (c) 2013, 2021 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation,
 *                    Bug 574784 - Parameters from copy constructor erroneously added to ...
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.sync;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.eclipse.cdt.core.dom.ast.IASTCompoundStatement;
import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTFileLocation;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDeclarator;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTNodeSelector;
import org.eclipse.cdt.core.dom.ast.IASTParameterDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTPointerOperator;
import org.eclipse.cdt.core.dom.ast.IASTStatement;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.index.IIndex;
import org.eclipse.cdt.core.model.CModelException;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.model.IFunctionDeclaration;
import org.eclipse.cdt.core.model.IMethodDeclaration;
import org.eclipse.cdt.core.model.IParent;
import org.eclipse.cdt.core.model.ISourceRange;
import org.eclipse.cdt.core.model.ISourceReference;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.cdt.core.model.IWorkingCopy;
import org.eclipse.cdt.core.parser.IToken;
import org.eclipse.cdt.ui.CDTUITools;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.infra.base.StringUtils;
import org.eclipse.papyrus.designer.languages.common.base.codesync.SyncStatus;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.SyncInformation;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.TextEditorConstants;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.Utils;
import org.eclipse.papyrus.designer.languages.cpp.codegen.Constants;
import org.eclipse.papyrus.designer.languages.cpp.library.CppUriConstants;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Array;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.EStorageClass;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ref;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.StorageClass;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Volatile;
import org.eclipse.papyrus.designer.languages.cpp.reverse.CommentUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.ParameterUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.papyrus.infra.core.Activator;
import org.eclipse.ui.IEditorInput;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;

public class SyncCDTtoModel implements Runnable {

	// special treatment of void in case of return parameters
	public static final String VOID_TYPE = "void"; //$NON-NLS-1$

	public static final String REGISTER = "register"; //$NON-NLS-1$

	public static final String CONST = "const"; //$NON-NLS-1$

	public static final String VOLATILE = "volatile"; //$NON-NLS-1$

	public static final String sAtParam = "@param"; //$NON-NLS-1$

	public static final String sAtReturn = "@return"; //$NON-NLS-1$

	public static final String ansiCLib = "AnsiCLibrary"; //$NON-NLS-1$

	/**
	 * input of the CDT editor. Used to obtain code within editor.
	 */
	protected IEditorInput m_input;

	/**
	 * The classifier (class) that is currently edited
	 */
	protected Classifier m_classifier;

	/**
	 * name of CDT project in which the generated code is stored.
	 */
	protected String m_projectName;

	/**
	 * reference to code generator
	 */
	protected ILangCodegen m_codegen;

	/**
	 * The node selector, enabling the navigation from the CDT model towards the AST
	 */
	IASTNodeSelector selector;

	public SyncCDTtoModel(IEditorInput input, Classifier classifier, String projectName, String generatorID) {
		m_input = input;
		m_classifier = classifier;
		m_projectName = projectName;
		m_codegen = LanguageCodegen.getGenerator(TextEditorConstants.CPP, generatorID);
	}

	public final String c_cpp_langID = "C/C++"; //$NON-NLS-1$

	public void syncCDTtoModel() {
		CommandSupport.exec(m_classifier, "update model from CDT", this); //$NON-NLS-1$
	}

	@Override
	public void run() {
		ICElement ice = CDTUITools.getEditorInputCElement(m_input);
		SyncStatus.syncFromEditor = true;

		if (ice instanceof ITranslationUnit) {
			ICProject project = CoreModel.getDefault().getCModel().getCProject(m_projectName);

			IIndex index = null;
			try {
				index = CCorePlugin.getIndexManager().getIndex(project);
				index.acquireReadLock();

				ITranslationUnit itu = (ITranslationUnit) ice;

				IASTTranslationUnit ast = itu.getAST(index, ITranslationUnit.AST_SKIP_INDEXED_HEADERS);
				selector = ast.getNodeSelector(null);

				examineChildren(itu, itu);
				updateCppInclude(itu);

				// CUIPlugin.getDefault().getProblemMarkerManager();
				if (itu instanceof IWorkingCopy) {
					((IWorkingCopy) itu).reconcile(true, new NullProgressMonitor());
				}

			} catch (CModelException e) {
				Activator.log.error(e);
			} catch (Exception e) {
				Activator.log.error(e);
			} finally {
				if (index != null) {
					index.releaseReadLock();
				}
			}
		}
		SyncStatus.syncFromEditor = false;
	}

	/**
	 * Examine the children of a translation unit in order to extract the methods
	 * that are defined within hte unit
	 *
	 * @param itu
	 * @param selector
	 * @param parent
	 * @throws CModelException
	 */
	public void examineChildren(ITranslationUnit itu, IParent parent) throws CModelException {

		int position = 0;
		// if (parent instanceof Namespace) {
		for (ICElement child : parent.getChildren()) {
			if (child instanceof IParent) {
				examineChildren(itu, (IParent) child);
			}
			ISourceRange range = null;
			if (child instanceof ISourceReference) {
				range = ((ISourceReference) child).getSourceRange();
			}
			if (child instanceof IFunctionDeclaration) {
				// function declaration is a superclass for method declaration
				// (but need to trace functions differently?)
				String name = ((IFunctionDeclaration) child).getElementName();
				IASTNode node = selector.findEnclosingNode(range.getStartPos(), range.getLength());
				if (node instanceof IASTFunctionDefinition) {
					IASTFunctionDefinition definition = (IASTFunctionDefinition) node;
					IASTFunctionDeclarator declarator = definition.getDeclarator();
					String unfilteredBody = getBody(itu, definition);
					// get additional information about method synchronization from generator
					SyncInformation syncInfo = m_codegen.getSyncInformation(name, unfilteredBody);
					String body = Utils.removeGenerated(unfilteredBody);
					if (syncInfo == null || !syncInfo.isGenerated) {
						// only update method, if it is not generated
						NamedElement ne = updateMethod(position, parent, name, body, declarator, syncInfo);
						updateComment(itu, definition, ne);
					}
					// System.err.println("body source <" + body + ">");
				}
				position++;
			}
		}
	}

	/**
	 * update the contents of the CppInclude directive
	 *
	 * @param itu the translation unit
	 */
	public void updateCppInclude(ITranslationUnit itu) {
		String contents = new String(itu.getContents());
		int preBodyStart = contents.indexOf(Constants.cppIncPreBodyStart);
		int preBodyEnd = contents.indexOf(Constants.cppIncPreBodyEnd);
		String preBody = ""; //$NON-NLS-1$
		String body = ""; //$NON-NLS-1$
		if (preBodyStart != -1) {
			preBodyStart += Constants.cppIncPreBodyStart.length();
			if (preBodyEnd > preBodyStart) {
				preBody = contents.substring(preBodyStart, preBodyEnd).trim();
			}
		}

		int bodyStart = contents.indexOf(Constants.cppIncBodyStart);
		int bodyEnd = contents.indexOf(Constants.cppIncBodyEnd);

		if (bodyStart != -1) {
			bodyStart += Constants.cppIncBodyStart.length() + 1;
			if (bodyEnd > bodyStart) {
				body = contents.substring(bodyStart, bodyEnd).trim();
			}
		}
		if (body.length() > 0 || preBody.length() > 0) {
			Include include = StereotypeUtil.applyApp(m_classifier, Include.class);
			if (include != null) {
				include.setPreBody(preBody);
				include.setBody(body);
			}
		}
	}

	/**
	 * Update a method in the model based on the qualified name.
	 *
	 * @param position      The position of the method within the file. Used to
	 *                      identify renaming operations
	 * @param parent        the CDT parent which is used to get a list of children
	 * @param qualifiedName the qualified name of a method
	 * @param body          the method body
	 * @param declarator    the declarator for the method
	 * @return the operation or the behavior within the model that got updated. The
	 *         latter is returned in case of behaviors that do not have a
	 *         specification (e.g. the effect of a transition).
	 */
	public NamedElement updateMethod(int position, IParent parent, String qualifiedName, String body,
			IASTFunctionDeclarator declarator, SyncInformation syncInfo) {

		String names[] = qualifiedName.split(TextEditorConstants.nsSep);
		String name = names[names.length - 1];

		Operation operation = null;
		Behavior behavior = null;

		if (syncInfo == null || (syncInfo.behavior == null && syncInfo.createBehaviorName == null)) {
			operation = getModelOperationFromName(name, parent, declarator, position);
			if (operation != null) {
				operation.setName(name);
			} else {
				// it is possible that the C++ method corresponds to the effect
				// of a transition. try to locate the behavior (without using an operation)
				behavior = FindTransition.findBehavior(m_classifier, name);
				if (behavior == null) {
					// still null => create new operation in model
					if (m_classifier instanceof Class) {
						operation = ((Class) m_classifier).createOwnedOperation(name, null, null);
					} else if (m_classifier instanceof DataType) {
						operation = ((DataType) m_classifier).createOwnedOperation(name, null, null);
					}
				}
			}
		} else if (syncInfo.behavior != null) {
			// operation is still null (=> does not enter operation != null case below)
			behavior = syncInfo.behavior;
		} else if ((syncInfo.createBehaviorName != null) && (m_classifier instanceof Class)) {
			Class clazz = (Class) m_classifier;
			behavior = (OpaqueBehavior) clazz.createOwnedBehavior(syncInfo.createBehaviorName,
					UMLPackage.eINSTANCE.getOpaqueBehavior().eClass());
		}

		if (operation != null) {
			if (operation.getMethods().size() == 0) {
				// operation exists, but does not have any method => create

				if (m_classifier instanceof Class) {
					behavior = ((Class) m_classifier).createOwnedBehavior(name,
							UMLPackage.eINSTANCE.getOpaqueBehavior());
				} else if (m_classifier instanceof DataType) {
					// ob = (OpaqueBehavior) ((DataType)
					// m_classifier).createOwnedBehavior(name,
					// UMLPackage.eINSTANCE.getOpaqueBehavior());
				}
				behavior.setSpecification(operation);
				behavior.setIsReentrant(false);
			} else {
				behavior = operation.getMethods().get(0);
				// operation has at least one method, this may not be null.
				if (!behavior.getName().equals(name)) {
					behavior.setName(name);
				}
			}
		}

		updateParameters(declarator, operation);

		if (behavior instanceof OpaqueBehavior) {
			updateParameters(declarator, behavior);

			OpaqueBehavior ob = (OpaqueBehavior) behavior;
			if (ob.getBodies().size() == 0) {
				ob.getLanguages().add(c_cpp_langID);
				ob.getBodies().add(""); //$NON-NLS-1$
			}
			for (int i = 0; i < ob.getLanguages().size(); i++) {
				// update first body of one of the languages supported by CDT. This implies that
				// it is actually not possible to have separate C and C++ bodes in the same
				// opaque
				// behavior (which is rarely a good idea).
				String language = ob.getLanguages().get(i);
				if (TextEditorConstants.CPP.matcher(language).matches() || c_cpp_langID.equals(language)) {
					if (i < ob.getBodies().size()) {
						// should always be true, unless sync between languages/bodies is lost
						ob.getBodies().set(i, body);
					}
				}
			}
		}
		if (operation != null) {
			return operation;
		} else {
			return behavior;
		}
	}

	/**
	 * Update the parameters of an operation or behavior
	 * 
	 * @param declarator   an AST function-declarator
	 * @param opOrBehavior an operation or behavior
	 */
	protected void updateParameters(IASTFunctionDeclarator declarator, NamedElement opOrBehavior) {
		List<Parameter> paramList = new ArrayList<Parameter>();
		List<Parameter> existingParamList = ParameterUtils.getOwnedParametersCopy(opOrBehavior);
		IASTFunctionDefinition definition = (IASTFunctionDefinition) declarator.getParent();

		for (IASTNode declaratorChild : declarator.getChildren()) {
			if (declaratorChild instanceof IASTParameterDeclaration) {
				IASTParameterDeclaration parameter = (IASTParameterDeclaration) declaratorChild;
				IASTName parameterName = parameter.getDeclarator().getName();
				IASTDeclSpecifier parameterType = parameter.getDeclSpecifier();
				ParameterModifiers modifiers = new ParameterModifiers();
				String parameterTypeName = ""; //$NON-NLS-1$
				try {
					IToken token = parameter.getDeclarator().getSyntax();
					updateModifierFromParameter(token, modifiers);
					token = parameterType.getSyntax();
					parameterTypeName = updateModifierFromParameterType(token, parameterTypeName, modifiers);
				} catch (ExpansionOverlapsBoundaryException e) {
				}

				if (parameterType.isRestrict()) {
				}
				Parameter umlParameter = ParameterUtils.getParameterViaName(existingParamList,
						parameterName.toString());
				Type umlType = getParameterType(parameterName, parameterTypeName, umlParameter);
				if (umlParameter == null) {
					// does not exist, create
					umlParameter = ParameterUtils.createOwnedParameter(opOrBehavior, parameterName.toString(), umlType);
				} else {
					umlParameter.setType(umlType);
				}
				applyParameterModifiers(parameterType, umlParameter, modifiers);
				paramList.add(umlParameter);
			}
		}

		IASTDeclSpecifier declSpecifier = definition.getDeclSpecifier();
		String retTypeStr = declSpecifier.toString();
		ParameterModifiers retParamModifiers = new ParameterModifiers();

		for (IASTPointerOperator ptrOp : declarator.getPointerOperators()) {
			// don't update the typename, only add modifiers
			try {
				updateModifierFromParameterType(ptrOp.getSyntax(), retTypeStr, retParamModifiers);
			} catch (ExpansionOverlapsBoundaryException e) {
			}
		}
		// remove return type, if void (unless pointer operators are used)
		if (retTypeStr != null && retTypeStr.length() > 0
				&& (!retTypeStr.equals(VOID_TYPE) || declarator.getPointerOperators().length > 0)) {
			Parameter umlRetParameter = ParameterUtils.getParameterViaName(existingParamList, null);
			Type retParamType = getParameterType(null, retTypeStr, umlRetParameter);
			if (umlRetParameter == null) {
				// does not exist, create
				umlRetParameter = ParameterUtils.createReturnResult(opOrBehavior, retParamType);
			} else {
				umlRetParameter.setType(retParamType);
			}
			applyParameterModifiers(declSpecifier, umlRetParameter, retParamModifiers);
			paramList.add(umlRetParameter);
		}

		for (Parameter existingParam : existingParamList) {
			if (!paramList.contains(existingParam)) {
				existingParam.destroy();
			}
		}

		// reorder parameter list
		ParameterUtils.resetParameters(opOrBehavior, paramList);
	}

	/**
	 * Get a modifier that is next to a parameter variable
	 * 
	 * @param token     the token associated with a parameter
	 * @param modifiers a ParameterModifiers object (that gets modified)
	 */
	protected void updateModifierFromParameter(IToken token, ParameterModifiers modifiers) {
		while (token != null) {
			String tokenStr = token.toString();
			if (tokenStr.equals("*")) { //$NON-NLS-1$
				modifiers.isPointer = true;
			} else if (tokenStr.equals("&")) { //$NON-NLS-1$
				modifiers.isRef = true;
			} else if (tokenStr.equals("[")) { //$NON-NLS-1$
				while (token != null) {
					modifiers.array += token.toString();
					token = token.getNext();
				}
				if (token == null) {
					break;
				}
			}
			token = token.getNext();
		}
	}

	/**
	 * Get a modifier that is next to a parameter type
	 * 
	 * @param token     the token associated with a parameter type
	 * @param modifiers a ParameterModifiers object (that gets modified)
	 * @return an eventually modified parameter type name
	 */
	protected String updateModifierFromParameterType(IToken token, String parameterTypeName,
			ParameterModifiers modifiers) {
		while (token != null) {
			String tokenStr = token.toString();
			if (tokenStr.equals("*")) { //$NON-NLS-1$
				// TODO: check, if this can be called (depending on
				// * position with different semantics?)
				modifiers.isPointer = true;
			} else if (tokenStr.equals("&")) { //$NON-NLS-1$
				modifiers.isRef = true;
			} else if (tokenStr.equals(REGISTER)) {
				modifiers.isRegister = true;
			} else if (tokenStr.equals(CONST) || tokenStr.equals(VOLATILE)) {
				// do nothing (use isConst() or isVolatile() operation of parameterType)
				// is not part of parameter type
			} else {
				if (parameterTypeName.length() > 0) {
					parameterTypeName += " "; //$NON-NLS-1$
				}
				parameterTypeName += tokenStr;
			}
			token = token.getNext();
		}
		return parameterTypeName;
	}

	/**
	 * Obtain the type of a parameter, checking (and loading) ANSI-C as well.
	 * 
	 * @param parameterName
	 * @param typeName           the name of a type (might be qualified, might be a
	 *                           simple type name)
	 * @param existingParameters
	 * @return
	 */
	protected Type getParameterType(IASTName parameterName, String typeName, Parameter existingParameter) {
		NamedElement namedElemParamType = ElementUtils.getQualifiedElement(PackageUtil.getRootPackage(m_classifier),
				typeName);
		if (namedElemParamType == null) {
			String qParName = ansiCLib + TextEditorConstants.nsSep + typeName;
			namedElemParamType = ElementUtils.getQualifiedElementFromRS(m_classifier, CppUriConstants.ANSIC_LIB_URI,
					qParName);

			// still null? recover type from existing definition (provided that name has not
			// changed)
			if (namedElemParamType == null && existingParameter != null) {
				return existingParameter.getType();
			}
		}
		return namedElemParamType instanceof Type ? (Type) namedElemParamType : null;
	}

	/**
	 * Apply the modifiers for a parameter, notably the stereotypes of the C++
	 * profile
	 * 
	 * @param parameterType the CDT AST parameter specification
	 * @param umlParameter  the UML parameter (to which a stereotype should be
	 *                      applied)
	 * @param modifiers     the modifiers that should be applied (stored in an
	 *                      instance of class ParameterModifiers)
	 */
	public void applyParameterModifiers(IASTDeclSpecifier parameterType, Parameter umlParameter,
			ParameterModifiers modifiers) {
		if (parameterType.isConst()) {
			StereotypeUtil.apply(umlParameter, Const.class);
		}
		if (parameterType.isVolatile()) {
			StereotypeUtil.apply(umlParameter, Volatile.class);
		}
		if (modifiers.isRegister) {
			StorageClass sc = StereotypeUtil.applyApp(umlParameter, StorageClass.class);
			if (sc != null) {
				sc.setStorageClass(EStorageClass.REGISTER);
			}
		}
		ParameterDirectionKind direction = umlParameter.getDirection();
		if (umlParameter.getUpper() == 1 && (direction == ParameterDirectionKind.IN_LITERAL
				|| direction == ParameterDirectionKind.RETURN_LITERAL)) {
			// apply the following modifiers only, if multiplicity is 1 and direction is
			// "in" or "return",
			// modifiers otherwise caused by out/inout or list (and eventual list hints)
			if (modifiers.isPointer) {
				StereotypeUtil.apply(umlParameter, Ptr.class);
			} else if (modifiers.isRef) {
				StereotypeUtil.apply(umlParameter, Ref.class);
			}
			if (modifiers.array.length() > 0) {
				Array arraySt = StereotypeUtil.applyApp(umlParameter, Array.class);
				if (arraySt != null && !modifiers.array.equals("[]") && (!modifiers.array.equals("[ ]"))) { //$NON-NLS-1$//$NON-NLS-2$
					arraySt.setDefinition(modifiers.array);
				}
			}
		}
	}

	/**
	 * Obtain an operation from the model by using the name of a CDT method. If an
	 * operation of the given name does not exist, it might indicate that the method
	 * has been renamed.
	 * 
	 * @param name     the operation name within CDT
	 * @param parent   the parent of the CDT method within CDT editor model
	 * @param position the position within the other methods. This information is
	 *                 used to locate methods within the model that might have been
	 *                 renamed in the CDT editor.
	 * @return a UML operation
	 */
	public Operation getModelOperationFromName(String name, IParent parent, IASTFunctionDeclarator currentDecl,
			int position) {
		Operation operation = null;
		List<Operation> opListMatching = new ArrayList<Operation>();
		for (Operation op : m_classifier.getOperations()) {
			if (op.getName() != null && op.getName().equals(name)) {
				opListMatching.add(op);
			}
		}

		if (opListMatching.size() == 0) {
			// operation is not found via name in the model
			// try to locate the operation in the model at the same "position" as the method
			// in the file and verify that this method does not have the same name as any
			// method in the CDT file.
			if (position < m_classifier.getOperations().size()) {
				operation = m_classifier.getOperations().get(position);
				String modelName = operation.getName();
				try {
					for (ICElement child : parent.getChildren()) {
						if (child instanceof IMethodDeclaration) {
							String cdtName = ((IMethodDeclaration) child).getElementName();
							if (cdtName.equals(modelName)) {
								// an existing operation in the CDT file already
								// has this name
								operation = null;
								break;
							}
						}
					}
				} catch (CModelException e) {
				}
			}
		} else if (opListMatching.size() == 1) {
			operation = opListMatching.get(0);
		} else {
			// operation is found more than once (overloading) via name in the model
			// determine the relative position of the current declarator within the list
			// of matching operations.
			// This method only works, if the user does not change positions. But it allows
			// for adding or removing parameters.
			int pos = 0;
			int matchPos = -1;
			try {
				for (ICElement child : parent.getChildren()) {
					if (child instanceof IMethodDeclaration) {
						IMethodDeclaration md = (IMethodDeclaration) child;
						ISourceRange range = md.getSourceRange();
						IASTNode node = selector.findEnclosingNode(range.getStartPos(), range.getLength());
						if (node instanceof IASTFunctionDefinition) {
							IASTFunctionDefinition fctDef = (IASTFunctionDefinition) node;
							if (fctDef.getDeclarator() == currentDecl) {
								matchPos = pos;
							}
						}
						String cdtName = md.getElementName();
						String names[] = cdtName.split(TextEditorConstants.nsSep);
						String shortName = names[names.length - 1];
						if (shortName.equals(name)) {
							pos++;
						}
					}
				}
			} catch (CModelException e) {
				Activator.log.error(e);
			}
			if (matchPos >= 0 && matchPos < opListMatching.size()) {
				operation = m_classifier.getOperations().get(matchPos);
			}
		}
		return operation;
	}

	public static String getBody(ITranslationUnit itu, IASTFunctionDefinition definition) {
		IASTStatement body = definition.getBody();

		if (body instanceof IASTCompoundStatement) {
			IASTCompoundStatement bodyComp = (IASTCompoundStatement) body;

			IASTFileLocation bodyLoc = bodyComp.getFileLocation();
			int start = bodyLoc.getNodeOffset();
			int end = start + bodyLoc.getNodeLength();
			char contents[] = itu.getContents();
			// body contains enclosing { } which we need to remove (+2, -2). We
			// cannot use the first and last statement, since leading and trailing
			// comments are not part of the AST tree.
			return StringUtils.decreaseIndent(contents, start + 2, end - 2, 4);
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * update a comment of a named element. Besides the comment of the element
	 * itself, comments on contained parameters are handled.
	 * 
	 * @param itu        a translation unit
	 * @param definition a function definition
	 * @param ne         a named element that is either an operation or a behavior
	 *                   (in order to update parameters)
	 */
	public void updateComment(ITranslationUnit itu, IASTFunctionDefinition definition, NamedElement ne) {
		String comment = CommentUtils.getPreceedingComment(definition);
		if (comment.length() > 0) {
			comment = CommentUtils.cleanupComment(comment);
			// filter @param
			int atParam = comment.indexOf(sAtParam);
			int atReturn = comment.indexOf(sAtReturn);
			// does atReturn appear before @atParam?
			int atParamOrReturn = (atReturn != -1 && (atReturn < atParam || atParam == -1)) ? atReturn : atParam;
			String commentMethodOnly = (atParamOrReturn != -1) ? comment.substring(0, atParamOrReturn).trim() : comment;

			while (atParam != -1) {
				int currentAtParam = atParam;
				atParam = comment.indexOf(sAtParam, atParam + 1);
				String commentParam = (atParam != -1) ? comment.substring(currentAtParam, atParam)
						: comment.substring(currentAtParam);
				Comment commentParamUML;
				int atParamName = sAtParam.length();

				while ((atParamName < commentParam.length())
						&& Character.isWhitespace(commentParam.charAt(atParamName))) {
					atParamName++;
				}
				int atParamNameEnd = atParamName;
				while ((atParamNameEnd < commentParam.length())
						&& !Character.isWhitespace(commentParam.charAt(atParamNameEnd))) {
					atParamNameEnd++;
				}
				if (atParamNameEnd < commentParam.length() - 1) {
					String parameterName = commentParam.substring(atParamName, atParamNameEnd);
					String commentParamText = commentParam.substring(atParamNameEnd).trim();
					Parameter parameter = null;
					if (ne instanceof BehavioralFeature) {
						parameter = ((BehavioralFeature) ne).getOwnedParameter(parameterName, null, false, false);
					} else if (ne instanceof Behavior) {
						parameter = ((Behavior) ne).getOwnedParameter(parameterName, null, false, false);
					}
					if (parameter != null) {
						EList<Comment> commentsParamUML = parameter.getOwnedComments();
						if (commentsParamUML.size() == 0) {
							commentParamUML = parameter.createOwnedComment();
							commentParamUML.getAnnotatedElements().add(commentParamUML);
						} else {
							commentParamUML = commentsParamUML.get(0);
						}
						commentParamUML.setBody(commentParamText);
					} else {
						// parameter is not found in model, e.g. either renamed
						// or not yet existing
						// store comment in operation comment
						commentMethodOnly += "\n " + sAtParam + parameterName + //$NON-NLS-1$
								" not found(!) " + commentParamText; //$NON-NLS-1$
					}
				}
			}
			if (commentMethodOnly.equals(StringConstants.STAR)) {
				commentMethodOnly = StringConstants.EMPTY;
			}
			// update/create comment, if a non-empty comment context has been detected.
			EList<Comment> commentsUML = ne.getOwnedComments();
			if (commentMethodOnly.length() > 0) {
				Comment commentUML;
				if (commentsUML.size() == 0) {
					commentUML = ne.createOwnedComment();
					commentUML.getAnnotatedElements().add(commentUML);
				} else {
					commentUML = commentsUML.get(0);
				}
				commentUML.setBody(commentMethodOnly);
			} else if (commentsUML.size() > 0) {
				// destroy first comment
				commentsUML.get(0).destroy();
			}
		}
	}

	/**
	 * Accessor
	 * 
	 * @return value of codegen attribute
	 */
	public ILangCodegen getCodeGen() {
		return m_codegen;
	}
}
