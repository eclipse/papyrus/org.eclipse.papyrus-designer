/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.cpp.cdt.project.ui;

import org.eclipse.cdt.ui.wizards.CDTCommonProjectWizard;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.languages.cpp.cdt.project.C_CppProjectSupport;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Supports the creation and configuration of CDT projects
 */
public class C_CppProjectSupportUI extends C_CppProjectSupport implements ILangProjectSupport {

	// TODO specific "root" is only required for component based code generation
	public static final String WS_PREFIX = "ws:"; //$NON-NLS-1$

	private static final String C = "c"; //$NON-NLS-1$

	private static final String CPP = "cpp"; //$NON-NLS-1$

	protected int dialogStatus;

	/**
	 * Create a C++ project. Caller should test before calling, whether the
	 * project exists already
	 *
	 * @param projectName
	 * @return the created project
	 */
	@Override
	public IProject createProject(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IProject project = root.getProject(projectName);
		dialogStatus = 0;
		try {
			IWorkbench wb = PlatformUI.getWorkbench();

			// create CDT wizard for C++ or C
			final CDTCommonProjectWizard wiz = this instanceof CppProjectSupportUI ? new CCNamedProjectWizard(projectName)
					: new CNamedProjectWizard(projectName);

			wiz.setWindowTitle("create project " + projectName); //$NON-NLS-1$
			wiz.init(wb, null);

			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					WizardDialog wizDiag = new WizardDialog(Display.getCurrent().getActiveShell(), wiz);

					wizDiag.create();
					dialogStatus = wizDiag.open();
				}
			});
			if (dialogStatus == Window.OK) {
				// update project (name might have changed by user)
				project = wiz.getLastProject();
			} else if (dialogStatus == Window.CANCEL) {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			project = null;
		}

		if ((project == null) || !project.exists()) {
			throw new RuntimeException(
					"Could not create CDT project. This might indicate that there is a problem with your CDT installation."); //$NON-NLS-1$
		}
		return project;
	}
}
