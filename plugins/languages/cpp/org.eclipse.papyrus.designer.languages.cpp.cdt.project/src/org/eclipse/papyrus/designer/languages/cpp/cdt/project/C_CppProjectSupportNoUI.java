/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.cpp.cdt.project;

import java.util.Map;
import java.util.SortedMap;

import org.eclipse.cdt.core.CCProjectNature;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.settings.model.ICProjectDescriptionManager;
import org.eclipse.cdt.managedbuilder.buildproperties.IBuildPropertyValue;
import org.eclipse.cdt.managedbuilder.core.IProjectType;
import org.eclipse.cdt.managedbuilder.core.IToolChain;
import org.eclipse.cdt.managedbuilder.core.ManagedBuildManager;
import org.eclipse.cdt.managedbuilder.internal.core.ManagedBuildInfo;
import org.eclipse.cdt.managedbuilder.internal.core.ManagedProject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;

/**
 * Supports the creation and configuration of CDT projects
 */
public class C_CppProjectSupportNoUI extends C_CppProjectSupport implements ILangProjectSupport {

	private static final String EXECUTABLE = "Executable"; //$NON-NLS-1$

	protected String natureId;

	/**
	 * Create a C++ project. Caller should test before calling, whether the
	 * project exists already
	 *
	 * @param projectName
	 * @return the created project
	 */
	@Override
	public IProject createProject(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IProject project = root.getProject(projectName);
		dialogStatus = 0;

		project = root.getProject(projectName);
		if (project == null) {
			throw new RuntimeException(
					"Could not create a project."); //$NON-NLS-1$

		}
		try {
			if (!project.exists()) {
				project.create(null);
				if (!project.isOpen()) {
					project.open(null);
				}
			}

			if (!project.hasNature(natureId)) {
				IProjectDescription description = ResourcesPlugin.getWorkspace().newProjectDescription(projectName);
				var location = project.getLocationURI();
				if (location != null) {
					description.setLocationURI(location);
				}
				CCorePlugin.getDefault().createCDTProject(description, project, null);
				CCProjectNature.addCNature(project, null);
				if (natureId == CCProjectNature.CC_NATURE_ID) {
					CCProjectNature.addCCNature(project, null);
				}
				configureCDT(project);
			}
		} catch (CoreException e) {
			Activator.log.error(e);
		}
		return project;
	}

	/**
	 * Basic configuration of a CDT project with managed builds created from type map.
	 * The function uses the first (supported) tool-chain for the current platform that
	 * produces an executable.
	 * 
	 * @throws CoreException
	 */
	@SuppressWarnings("restriction")
	static void configureCDT(IProject project) throws CoreException {
		ICProjectDescriptionManager mngr = CoreModel.getDefault().getProjectDescriptionManager();
		var cdesc = mngr.getProjectDescription(project, true);
		if (cdesc == null) {
			cdesc = mngr.createProjectDescription(project, true);
		}

		SortedMap<String, IProjectType> typeMap = ManagedBuildManager.getExtensionProjectTypeMap();
		for (Map.Entry<String, IProjectType> e : typeMap.entrySet()) {
			IProjectType pt = e.getValue();
			IBuildPropertyValue type = pt.getBuildArtefactType();
			// always only check supported toolchains (might change, if running in docker?)
			boolean isValid = type.getName().equals(EXECUTABLE) && pt.isSupported()
					&& !pt.isAbstract() && !pt.isSystemObject();
			if (!isValid) {
				continue;
			}
			IToolChain[] tcs = ManagedBuildManager.getExtensionToolChains(pt);

			for (IToolChain tc : tcs) {
				if (ManagedBuildManager.isPlatformOk(tc)) {
					var cfgs = ManagedBuildManager.getExtensionConfigurations(tc, pt);
					ManagedBuildInfo info = ManagedBuildManager.createBuildInfo(project);
					ManagedProject mProj = new ManagedProject(project, pt);
					info.setManagedProject(mProj);
					// typically debug and release configurations
					boolean firstCF = true;
					for (var cf : cfgs) {
						if (firstCF) {
							firstCF = false;
							info.setDefaultConfiguration(cf);
						}
						// set artifact name (otherwise build does not run)
						cf.setArtifactName(mProj.getDefaultArtifactName());
						ManagedBuildManager.createConfigurationForProject(cdesc, mProj, cf, ManagedBuildManager.CFG_DATA_PROVIDER_ID);
					}
					// a tool-chain for the current platform has been added, don't check further
					break;
				}

			}
		}
		mngr.setProjectDescription(project, cdesc, true, null);
		ManagedBuildManager.saveBuildInfo(project, true);
	}
}
