/*******************************************************************************
 * Copyright (c) 2014, 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils.cgu
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.Modifier.*
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.Signal
import org.eclipse.uml2.uml.ValueSpecification
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenUtils
import org.eclipse.uml2.uml.InstanceValue
import org.eclipse.uml2.uml.Namespace

/**
 * @author Önder GÜRCAN (onder.gurcan@cea.fr)
 */
class CppAttribute {

	// Loop over attributes. Check that the attribute is not a static const.
	// Constant static attributes are declared within the class declaration 
	static def CppStaticAttributes(Classifier classifier) {
		var code = '''
			// static attributes (if any)
			«FOR ownedAttribute : getOwnedAttributes(classifier)»
				«IF (ownedAttribute.isStatic)»
					«CppStaticAttributeImplementation(ownedAttribute)»
				«ENDIF»
			«ENDFOR»
		'''
		return code
	}

	// return a list of owned attributes, empty set, if null
	static def getOwnedAttributes(Classifier cl) {
		val attributes = getOwnedAttributesWNull(cl)
		if (attributes === null) {
			emptySet
		} else {
			attributes
		}
	}

	// return a list of owned attributes, since this is not supported directly on a classifier
	static def getOwnedAttributesWNull(Classifier cl) {
		if (cl instanceof Class) {
			(cl as Class).ownedAttributes
		} else if (cl instanceof DataType) {
			(cl as DataType).ownedAttributes
		} else if (cl instanceof Interface) {
			(cl as Interface).ownedAttributes
		} else if (cl instanceof Signal) {
			(cl as Signal).ownedAttributes
		} else {
			// Sequence{}
		}
	}

	static def CppStaticAttributeImplementation(Property attribute) '''
		«CppDocumentation.CppElementDoc(attribute)»
		«attribute.modCVQualifier»«CppTypedElement.cppType(attribute)»«attribute.modPtr»«attribute.modRef»  «attribute.class_.name»::«attribute.name»«attribute.modArray»«attribute.defaultValue(true)»;
	'''

	/**
	 * create a default-value assignment, optionally for static values
	 */
	static def defaultValue(Property attribute, boolean forStatic) {
		if ((attribute.defaultValue !== null) && (attribute.defaultValue.textValue !== null) &&
			attribute.isStatic == forStatic) {
			"=" + attribute.defaultValue.textValue()
		}
	}

	static def CppAttributeDeclaration(Property attribute) '''

		«CppDocumentation.CppElementDoc(attribute)»
		«staticValue(attribute)»«attribute.modCVQualifier»«CppTypedElement.cppType(attribute)»«
			attribute.modPtr»«attribute.modRef» «attribute.name»«attribute.modArray»«attribute.defaultValue(false)»;
	'''

	static def staticValue(Property attribute) {
		if(attribute.isStatic) 'static '
	}

	/**
	 * Similar to UML's stringValue, but with a particular handling of
	 * enumerations
	 */
	static def textValue(ValueSpecification value) {
		if (value instanceof InstanceValue) {
			val iv = value as InstanceValue
			if (iv.instance instanceof EnumerationLiteral) {
				val literal = iv.instance as EnumerationLiteral
				if (CppCodeGenUtils.getC11ClassEnum(literal.enumeration)) {
					// qualify with enumeration 
					return value.cgu.cppQualifiedName(literal.enumeration) + Namespace.SEPARATOR + literal.name
				} else {
					// qualify with namespace of enumeration (literal is visible in whole namespace)
					return value.cgu.cppQualifiedName(literal.enumeration.namespace) + Namespace.SEPARATOR + literal.name
				}
			}
		}
		return value.stringValue
	}
}
