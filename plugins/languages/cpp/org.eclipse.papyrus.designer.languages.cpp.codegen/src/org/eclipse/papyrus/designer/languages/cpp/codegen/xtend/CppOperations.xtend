/*******************************************************************************
 * Copyright (c) 2014, 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *     Ansgar Radermacher - Bug 581531 - handle return parameter multiplicity
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.cpp.codegen.Constants
import org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppClassUtils
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.ConstInit
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Default
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Delete
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Explicit
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Inline
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Variadic
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Virtual
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Region
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.profile.standard.Destroy
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils.cgu
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.Modifier.*

class CppOperations {
	static def CppOperationImplementation(Operation operation) '''
		«CppDocumentation.CppOperationDoc(operation)»
		«IF (operation.name == 'main')»
			«CppReturnSpec(operation)»«operation.name»(«CppParameter.CppOperationParameters(operation,false)») {
				«GenUtils.getBody(operation, Constants.supportedLanguages)»
			} 
		«ELSE»
			«CppTemplates.templateSignature(operation)»«InlineTxt(operation)»«operation.modCEQualifier»«CppReturnSpec(operation)»«GenUtils.getNestedOperationFarthestClassifierOwnerNamespace(operation)»«CppTemplates.templateShortSignature(operation)»::«destructor(operation)»«operation.name»(«
					CppParameter.CppOperationParameters(operation, false)»«variadicParameter(operation)»)«operation.modCVQualifier(true)»«operation.throwss»«CppConstInit(operation)» {
				«GenUtils.getBody(operation, Constants.supportedLanguages)»
			}
		«ENDIF»
	'''

	static def CppReturnSpec(Operation operation) {
		if ((operation.type === null) || isConsOrDestructor(operation)) {
			ConsDestructorOrVoid(operation)
		} else {
			val retParam = operation.getReturnResult
			retParam.modCVQualifier + retParam.modSCQualifier + CppTypedElement.cppType(retParam) + retParam.modPtr + retParam.modRef + retParam.modArray + ' ' 
		}
	}

	static def CppReturnSpec(Behavior behavior) '''
		«IF (GenUtils.returnResult(behavior) === null)»void «ELSE»«GenUtils.returnResult(behavior).modCVQualifier» «
			behavior.cgu.cppQualifiedName(GenUtils.returnResult(behavior).type)»«GenUtils.returnResult(behavior).modPtr»«GenUtils.returnResult(behavior).modRef» «ENDIF»
	'''

	static def throwss(Operation operation) '''
	«IF operation.raisedExceptions.length > 0»
		«" "»throw(«FOR re : operation.raisedExceptions SEPARATOR ','»«operation.cgu.cppQualifiedName(re)»«ENDFOR»)«ENDIF»'''

	static def ConsDestructorOrVoid(Operation operation) {
		if (isConsOrDestructor(operation)) {
		} else {
			'void '
		}
	}

	static def isConsOrDestructor(Operation operation) {
		GenUtils.hasStereotype(operation, Create) || GenUtils.hasStereotype(operation, Destroy)
	}

	static def CppConstInit(Operation operation) {
		if (GenUtils.hasStereotype(operation, ConstInit) && GenUtils.hasStereotype(operation, Create)) {
			": " + UMLUtil.getStereotypeApplication(operation, ConstInit).initialisation
		}
	}

	// return a list of owned operations, return emptyset, if null
	static def getOwnedOperations(Classifier cl) {
		val operations = getOwnedOperationsWNull(cl)
		if (operations === null) {
			emptySet
		} else {
			operations
		}
	}

	// return a list of owned operations, since this is not supported directly on a classifier
	static def getOwnedOperationsWNull(Classifier cl) {
		if (cl instanceof Class) {
			(cl as Class).ownedOperations
		} else {
			if (cl instanceof DataType) {
				(cl as DataType).ownedOperations
			} else {
				if (cl instanceof Interface) {
					(cl as Interface).ownedOperations
				} else {
					// Sequence{}
				}
			}
		}
	}

	static def getNestedOperations(Classifier c1) {
		val operations = getNestedOperationsWNull(c1)
		if (operations === null) {
			emptySet
		} else {
			operations
		}
	}

	static def getNestedOperationsWNull(Classifier cl) {
		if (cl instanceof Class || cl instanceof Interface) {
			CppClassUtils.nestedOperations(cl)
		}
	}

	static def CppBehaviorImplementation(OpaqueBehavior behavior) '''
		«CppDocumentation.CppBehaviorDoc(behavior)»
		«CppReturnSpec(behavior)»«GenUtils.getNestedBehaviorFarthestClassifierOwnerNamespace(behavior)»::«behavior.qualifiedBehaviorName»(«CppParameter.CppBehaviorParameters(behavior, false)»)«behavior.modCVQualifier(true)» {
			«GenUtils.getBodyFromOB(behavior, Constants.supportedLanguages)»
		}
	'''

	static def CppOperationDeclaration(Operation operation) '''
		«CppDocumentation.CppOperationDoc(operation)»
		«operation.explicitTxt»«operation.modCEQualifier»«InlineTxt(operation)»«virtualTxt(operation)»«staticTxt(operation)»«CppReturnSpec(operation)»«destructor(operation)»«operation.name»(«CppParameter.CppOperationParameters(operation,true)»«variadicParameter(operation)»)«operation.modCVQualifier»«operation.throwss»«virtualSuffix(operation)»;
	'''

	static def InlineTxt(Element element) {
		if(GenUtils.hasStereotype(element, Inline)) 'inline '
	}

	static def explicitTxt(Element element) {
		if(GenUtils.hasStereotype(element, Explicit)) 'explicit '
	}

	static def virtualTxt(Operation operation) {
		if((operation.interface !== null) || (operation.isAbstract) ||
			(GenUtils.hasStereotype(operation, Virtual))) 'virtual '
	}

	static def staticTxt(Operation operation) {
		if(operation.isStatic) 'static '
	}

	static def destructor(Operation operation) {
		if (GenUtils.hasStereotype(operation, Destroy) && (!operation.name.startsWith('~'))) {
			'~'
		} else {
			''
		}
	}

	/**
	 * Common method to handle virtual, default and deleted operations
	 */
	static def virtualSuffix(Operation operation) {
		if ((operation.interface !== null) || (operation.isAbstract))
			' = 0'
		else if(StereotypeUtil.isApplied(operation, Delete)) ' = delete' else if(StereotypeUtil.isApplied(operation,
			Default)) ' = default'
	}

	static def CppBehaviorDeclaration(Classifier cl, Behavior behavior) '''
		«CppDocumentation.CppBehaviorDoc(behavior)»
		«InlineTxt(behavior)»«CppReturnSpec(behavior)»«behavior.qualifiedBehaviorName»(«CppParameter.CppBehaviorParameters(behavior, true)»)«behavior.modCVQualifier»;
	'''

	static def qualifiedBehaviorName(Behavior behavior) {
		var ne = behavior as NamedElement
		var name = behavior.name
		while ((ne !== null) && !(ne instanceof Classifier) || (ne instanceof Behavior)) {
			if (ne.owner instanceof NamedElement) {
				ne = ne.owner as NamedElement
			}
			if (ne.owner instanceof Region) {
				name = ne.name + "_" + name
			}
		}
		return name
	}

	static def variadicParameter(Operation operation) {
		var hasParameters = false;
		var i = 0;
		if (GenUtils.hasStereotype(operation, Variadic)) {
			while (i < operation.ownedParameters.size && !hasParameters) {
				if (operation.ownedParameters.get(i).direction != ParameterDirectionKind.RETURN_LITERAL) {
					hasParameters = true;
				}
				i++
			}

			if (hasParameters) {
				', ...'
			} else {
				'...'
			}
		}
	}
}
