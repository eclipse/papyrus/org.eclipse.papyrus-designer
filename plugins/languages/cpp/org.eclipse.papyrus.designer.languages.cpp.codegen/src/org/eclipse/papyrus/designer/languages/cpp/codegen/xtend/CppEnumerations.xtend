/*******************************************************************************
 * Copyright (c) 2014, 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
 
 package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.uml2.uml.Enumeration
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.CppInit
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenUtils

/**
 * Emit C++ enumerations
 */
class CppEnumerations {
	static def CppEnumerationDefinition(Enumeration enumeration) '''

		«CppDocumentation.CppElementDoc(enumeration)»
		«IF CppCodeGenUtils.getC11ClassEnum(enumeration)»enum class«ELSE»enum«ENDIF» «enumeration.name» {
			«FOR literal : enumeration.ownedLiterals SEPARATOR ","»
				«CppDocumentation.CppElementDoc(literal)»
				«literal.name»«literal.defaultValue»
			«ENDFOR»
		};

	'''
	
	static def defaultValue(EnumerationLiteral literal) {
		if (literal.specification !== null) {
			" = " + literal.specification.stringValue()
		} else if (GenUtils.hasStereotype(literal, CppInit)) {
			" = " +  UMLUtil.getStereotypeApplication(literal, CppInit).value
		}
	}
}
