/*******************************************************************************
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
 
 package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Parameter
import org.eclipse.papyrus.designer.languages.cpp.codegen.utils.Modifier
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils.cgu
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.Modifier.*
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Behavior
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Array
import org.eclipse.uml2.uml.util.UMLUtil

/**
 * @author Önder GÜRCAN (onder.gurcan@cea.fr)
 */
class CppParameter {

	static def CppOperationParameters(Operation operation, boolean showDefault) '''
		«FOR ownedParameter : operation.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL] SEPARATOR ', '»«CppParameter(ownedParameter, showDefault)»«ENDFOR»'''

	/**  
	 * comment signature for a given behavior (e.g. effect within state machine)
	 */ 
	static def CppBehaviorParameters(Behavior behavior, boolean showDefault) '''
		«FOR ownedParameter : behavior.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL] SEPARATOR ', '»«CppParameter(ownedParameter, showDefault)»«ENDFOR»
	'''

	/**
	 * C++ parameter. Default values are added, if parameter showDefault is true (implementation signature
	 */ 
	static def CppParameter(Parameter parameter, boolean showDefault) {
		parameter.modCVQualifier + parameter.modSCQualifier + CppTypedElement.cppType(parameter) +
			parameter.modPtr + parameter.modRef + parameter.dirInfo + " " + parameter.name +
			parameter.modArray + {if (showDefault) defaultValue(parameter) else ""}
	}

	/**
	 * CppParameterCalculation for CDT
	 */
	static def CppParameterForCDT(Parameter parameter) {
		var paramStr = Modifier.modCVQualifier(parameter) + Modifier.modSCQualifier(parameter) + parameter.cgu.cppQualifiedName(parameter.type) +
			Modifier.modPtr(parameter) + Modifier.modRef(parameter)
		if (UMLUtil.getStereotypeApplication(parameter, Array) !== null) {
			paramStr += "[]"
		}
		return paramStr
	}
	 
	static def defaultValue(Parameter parameter) {
		if (parameter.defaultValue !== null)  " = " + parameter.defaultValue.stringValue() else ""
	}
}