/*******************************************************************************
 * Copyright (c) 2006 - 2012, 2019, 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *     Ansgar Radermacher - Bug 546545 - support of list hints
 *     Ansgar Radermacher - Bug 581531 - handle return parameter multiplicity
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.utils;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.infra.base.StringUtils;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.AOverride;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.ListHint;
import org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenUtils;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Array;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Constexpr;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Mutable;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ref;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.StorageClass;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Volatile;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Utility functions managing the "modifier" of an element, i.e. additional
 * information whether a passed parameter or an attribute is a pointer, a
 * reference, an array or constant.
 */
public class Modifier {

	protected static final String EMPTY = StringConstants.EMPTY;

	/**
	 * constant for pointer modifier
	 */
	protected static final String PTR = StringConstants.STAR;

	protected static final String EMPTY_ARRAY = "[]"; //$NON-NLS-1$

	public static String modPtr(Element propertyOrParameter) {
		// Pointer
		String ptr;
		Ptr cppPtr = UMLUtil.getStereotypeApplication(propertyOrParameter, Ptr.class);
		if (cppPtr != null) {
			ptr = (cppPtr.getDeclaration() != null && !cppPtr.getDeclaration().equals(EMPTY)) ? cppPtr.getDeclaration() : PTR;
		} else {
			if (propertyOrParameter instanceof Property
					&& ((Property) propertyOrParameter).getAggregation() == AggregationKind.SHARED_LITERAL) {
				ptr = PTR;
			} else {
				ptr = EMPTY;
			}
		}

		boolean ptrOrRef = GenUtils.hasStereotype(propertyOrParameter, Ref.class)
				|| GenUtils.hasStereotype(propertyOrParameter, Ptr.class);

		// out and inout parameter are realized by means of a pointer
		if (propertyOrParameter instanceof Parameter) {
			ParameterDirectionKind directionKind = ((Parameter) propertyOrParameter).getDirection();
			if (directionKind == ParameterDirectionKind.OUT_LITERAL || directionKind == ParameterDirectionKind.INOUT_LITERAL) {
				// parameter is an out or inout parameter. If the user already either a pointer or reference, use this.
				if (!ptrOrRef) {
					// .. otherwise add the oeprator from the preferences
					ptr += CppCodeGenUtils.getOutInoutOp();
				}
			}
		}
		return ptr;
	}

	public static String modRef(Element propertyOrParameter) {
		// Reference
		String ref;
		Ref cppRef = UMLUtil.getStereotypeApplication(propertyOrParameter, Ref.class);
		if (cppRef != null) {
			ref = (cppRef.getDeclaration() != null && !cppRef.getDeclaration().equals(EMPTY)) ? cppRef.getDeclaration() : "&"; //$NON-NLS-1$
		} else {
			ref = EMPTY;
		}

		return ref;
	}

	/**
	 * Add array modifier ([] or [<upper>] postfix to attribute or parameter declaration
	 * (unless a list hint is applied)
	 * 
	 * @param propertyOrParameter
	 *            a property or parameter (a multiplicity element)
	 * @return the array modifier, ir an empty string
	 */
	public static String modArray(MultiplicityElement propertyOrParameter) {
		// Array
		Array cppArray = UMLUtil.getStereotypeApplication(propertyOrParameter, Array.class);
		String array = EMPTY;
		if (cppArray != null) {
			// explicit array definition
			if (cppArray.getDefinition() != null && !cppArray.getDefinition().isEmpty()) {
				array = cppArray.getDefinition();
			} else {
				array = EMPTY_ARRAY;
			}
		} else {
			// calculate array from multiplicity definition
			ListHint listHint = GenUtils.getApplicationTree(propertyOrParameter, ListHint.class);
			int lower = propertyOrParameter.getLower();
			int upper = propertyOrParameter.getUpper();
			if (upper == -1) {
				// only add array tag, if there is no list hint
				if (listHint == null || listHint.getVariable() == null) {
					// C++ does not allow for [] in return, map to pointer
					array = isReturn(propertyOrParameter) ? PTR : EMPTY_ARRAY;
				}
			} else if (upper > 1) {
				if (isReturn(propertyOrParameter)) {
					// also map [..n] multiplicities to pointer, since [n]
					// is not supported by C++.
					array = PTR;
				} else if (upper == lower) {
					if (listHint == null || listHint.getFixed() == null) {
						array = String.format("[%d]", upper); //$NON-NLS-1$
					}
				} else {
					if (listHint == null || listHint.getBounded() == null) {
						array = String.format("[%d]", upper); //$NON-NLS-1$
					}
				}
			}
		}
		return array;
	}

	/**
	 * @param element
	 *            an operation, property or parameter
	 * @return the modifier for const, volatile or override
	 */
	public static String modCVQualifier(Element element) {
		return modCVQualifier(element, false);
	}

	/**
	 * @param element
	 *            an operation, property or parameter
	 * @param isImplem
	 *            true, if method or attribute declaration outside of class
	 * @return the modifier for const, volatile or override
	 */
	public static String modCVQualifier(Element element, boolean isImplem) {
		String modifier = StringConstants.EMPTY;

		// const and volatile qualifiers cannot be used with static functions
		if (element instanceof Operation && ((Operation) element).isStatic()) {
			// do nothing
		} else {
			if (GenUtils.hasStereotype(element, Const.class)) {
				modifier = StringUtils.append(modifier, "const"); //$NON-NLS-1$
			}
			if (GenUtils.hasStereotype(element, Volatile.class)) {
				modifier = StringUtils.append(modifier, "volatile"); //$NON-NLS-1$
			}
		}
		// Mutable (non-static attribute only)
		if (GenUtils.hasStereotype(element, Mutable.class)) {
			if (element instanceof Property && !((Property) element).isStatic()) {
				modifier = StringUtils.append(modifier, "mutable"); //$NON-NLS-1$
			}
		}
		if (GenUtils.hasStereotype(element, AOverride.class) && !isImplem) {
			// only add override in method declarations
			if (element instanceof Operation && !((Operation) element).isStatic()) {
				modifier = StringUtils.append(modifier, "override"); //$NON-NLS-1$
			}
		}

		if (modifier.length() > 0) {
			modifier = (element instanceof Operation) ? StringConstants.SPACE + modifier : // added at the end of operation, prefix with space
					modifier + StringConstants.SPACE; // before property or parameter, postfix with space
		}

		return modifier;
	}

	/**
	 * @param propertyOrOperation
	 *            a property or an operation
	 * @return the modifier for constexpr
	 */
	public static String modCEQualifier(Element propertyOrOperation) {
		if (propertyOrOperation instanceof Property && !((Property) propertyOrOperation).isStatic()) {
			// constexpr attributes must be static => do nothing, if non-static
		} else {
			if (GenUtils.hasStereotype(propertyOrOperation, Constexpr.class)) {
				return "constexpr "; //$NON-NLS-1$
			}
		}
		return StringConstants.EMPTY;
	}

	/**
	 * @param propertyOrParameter
	 *            a property or parameter
	 * @return the modifier for storage class
	 */
	public static String modSCQualifier(Element propertyOrParameter) {
		StorageClass sc = UMLUtil.getStereotypeApplication(propertyOrParameter, StorageClass.class);
		if (sc != null) {
			return sc.getStorageClass().getLiteral() + " "; //$NON-NLS-1$
		}
		return EMPTY;
	}

	/**
	 * @param propertyOperationOrParameter
	 *            an operation or a parameter
	 * @return information about the direction of a parameter in form of a comment
	 */
	public static String dirInfo(Element propertyOperationOrParameter) {
		if (propertyOperationOrParameter instanceof Parameter) {
			ParameterDirectionKind directionKind = ((Parameter) propertyOperationOrParameter).getDirection();
			if (directionKind == ParameterDirectionKind.IN_LITERAL) {
				return " /*in*/"; //$NON-NLS-1$
			} else if (directionKind == ParameterDirectionKind.OUT_LITERAL) {
				return " /*out*/"; //$NON-NLS-1$
			} else if (directionKind == ParameterDirectionKind.INOUT_LITERAL) {
				return " /*inout*/"; //$NON-NLS-1$
			}
		}
		return EMPTY;
	}

	/**
	 * @param propertyOrParameter
	 *            a property or parameter
	 * @return true, if passed element is a return parameter
	 */
	protected static boolean isReturn(Element propertyOrParameter) {
		if (propertyOrParameter instanceof Parameter) {
			return ((Parameter) propertyOrParameter).getDirection() == ParameterDirectionKind.RETURN_LITERAL;
		}
		return false;
	}

	/**
	 * initialize the ptr/ref/array/isConst attributes.
	 *
	 * @param propertyOperationOrParameter
	 */
	public static void update(Element propertyOperationOrParameter) {

	}
}
