/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.common.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueBehavior;

/**
 * Check whether all behaviors have a specification. This happens for instance, if the user
 * deletes a specification and "forgets" to delete the associated behavior.
 */
public class BehaviorsWithoutSpecification extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {

		OpaqueBehavior behavior = (OpaqueBehavior) ctx.getTarget();

		if (behavior.getSpecification() == null) {
			Element class_ = behavior.getOwner();
	
			// it's quite normal that behaviors in state-machines have no specification. Check whether owner is class
			if (class_ instanceof org.eclipse.uml2.uml.Class) {
				return ctx.createFailureStatus("The behavior '%s' of '%s' has no specification", //$NON-NLS-1$
						behavior.getName(), ((NamedElement) class_).getQualifiedName());
			}
		}
		return ctx.createSuccessStatus();
	}
}
