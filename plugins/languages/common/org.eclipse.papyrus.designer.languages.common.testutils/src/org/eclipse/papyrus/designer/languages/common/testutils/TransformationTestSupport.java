/*******************************************************************************
 * Copyright (c) 2017 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.testutils;

import static org.hamcrest.MatcherAssert.assertThat;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.papyrus.designer.infra.ui.UIProjectManagement;
import org.eclipse.papyrus.designer.languages.common.base.TestInfo;
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.junit.utils.rules.AbstractHouseKeeperRule;
import org.eclipse.papyrus.junit.utils.rules.PapyrusEditorFixture;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class TransformationTestSupport {

	static {
		// This system property avoids opening dialogs during Papyrus operations. It must
		// be set before trying to load any of the Papyrus classes.
		System.setProperty(TestInfo.PAPYRUS_RUN_HEADLESS, Boolean.TRUE.toString());
	}

	/**
	 * The house keeper
	 */
	protected AbstractHouseKeeperRule houseKeeper;

	/**
	 * The modelSetFixture
	 */
	protected PapyrusEditorFixture modelSetFixture;

	/**
	 * The test class (used to get access to its bundle)
	 */
	Class<?> testClass;

	public TransformationTestSupport(Class<?> testClass, AbstractHouseKeeperRule houseKeeper, PapyrusEditorFixture modelSetFixture) {
		this.houseKeeper = houseKeeper;
		this.modelSetFixture = modelSetFixture;
		this.testClass = testClass;
	}

	/**
	 * Run a transformation with a given deployment plan
	 *
	 * @param depPlanOrTrafoPkg
	 *            the qualified name of a deployment plan or package with transformation stereotype
	 *            within the provided example
	 *
	 */
	public void runTransformation(String depPlanOrTrafoPkg) {

		NamedElement ne = ElementUtils.getQualifiedElement(modelSetFixture.getModel(), depPlanOrTrafoPkg);

		if (ne instanceof Package) {
			IProject project = UIProjectManagement.getCurrentProject();
			Package nePkg = (Package) ne;

			final int genOptions = 0;
			new ExecuteTransformationChain(nePkg, project).executeTransformation(new NullProgressMonitor(), genOptions);
		}
	}

	/**
	 * Validate the results
	 * 
	 * @param genProject
	 *            the project into which generation has been done;
	 * @param folderInTestBundle
	 *            The folder within the source bundle containing the expected results
	 * @param srcGen
	 *            The folder name containing the generated code (a folder of the same name is created within the model project to facilitate the comparison)
	 */
	public void validateResults(IProject genProject, String folderInTestBundle, String srcGen) {
		IProject modelProject = modelSetFixture.getProject().getProject();
		RecursiveCopy copier = new RecursiveCopy(houseKeeper);
		Bundle srcBundle = FrameworkUtil.getBundle(testClass);
		// copy expected results folder to model project
		copier.copy(srcBundle, folderInTestBundle, modelProject, TestConstants.EXPECTED_RESULT);

		IContainer expectedSrcGen = modelProject.getFolder(TestConstants.EXPECTED_RESULT);
		assertThat("expected source folder must exist", expectedSrcGen.exists()); //$NON-NLS-1$
		IContainer generatedSrcGen = srcGen.equals(".") ? genProject : genProject.getFolder(srcGen); //$NON-NLS-1$
		assertThat("generated source folder must exist", generatedSrcGen.exists()); //$NON-NLS-1$

		FileComparison.assertGeneratedMatchesExpected(generatedSrcGen, expectedSrcGen);
	}
}
