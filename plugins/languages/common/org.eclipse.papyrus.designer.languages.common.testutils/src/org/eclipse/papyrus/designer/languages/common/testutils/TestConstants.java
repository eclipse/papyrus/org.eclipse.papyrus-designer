/*******************************************************************************
 * Copyright (c) 2017 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.testutils;

@SuppressWarnings("nls")
public class TestConstants {
	public static final String FILE_SEP = "/";
	
	/**
	 * By convention, the folder in which expected results are stored
	 */
	public static final String EXPECTED_RESULT = "expectedResult";

	/**
	 * By convention, the folder in which generated code is stored
	 */
	public static final String SRC_GEN = "src-gen";

}
