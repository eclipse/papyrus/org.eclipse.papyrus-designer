/*******************************************************************************
 * Copyright (c) 2006 - 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base;

import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * C language support
 *
 */
public abstract class CommonLangCodegen implements ILangCodegen {

	protected ModelElementsCreator creator = null;

	protected IProject lastProject = null;

	@Override
	public void generateCode(IProject project, PackageableElement element, IProgressMonitor monitor) {
		manageCreator(project, element);
		creator.createPackageableElement(element, monitor);
	}

	@Override
	public void cleanCode(IProject project, PackageableElement element, IProgressMonitor monitor) {
		manageCreator(project, element);
		creator.removePackageableElement(element, monitor);
	}

	@Override
	public void cleanCode(IProject project, PackageableElement element, List<String> keepFiles, IProgressMonitor monitor) {
		manageCreator(project, element);
		IFolder folder = project.getFolder(GenUtils.getSourceFolder(element));
		creator.cleanUntouched(folder, keepFiles, monitor);
	}

	@Override
	public String getFileName(IProject project, NamedElement element) {
		if (element instanceof PackageableElement) {
			manageCreator(project, (PackageableElement) element);
			return creator.getFileName(element);
		}
		return null;
	}

	/**
	 * Manage a creator, i.e. make a new creator or use the existing
	 * 
	 * @param project
	 *            a project
	 * @param element
	 *            an element (used for instance to determine the source folder)
	 */
	protected void manageCreator(IProject project, PackageableElement element) {
		if ((project == null) && (element instanceof PackageableElement)) {
			project = getTargetProject((PackageableElement) element, false);
		}
		if ((creator == null) || (project != lastProject)) {
			lastProject = project;
			creator = newCreator(project, element);
		}
	}

	/**
	 * Subclasses must override this functions that creates a new model elements creator
	 * 
	 * @param project
	 *            a project
	 * @return the model elements creator
	 */
	protected abstract ModelElementsCreator newCreator(IProject project, PackageableElement element);
}
