/*******************************************************************************
 * Copyright (c) 2024 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base.preferences;

/**
 * Common code generation preference constants
 */
public class CodeGenConstants {

	/**
	 * State whether a standard wizard should be use (UI, several pages) or the project should be configured automatically.
	 */
	public static final String P_USE_PROJECT_WIZARD_KEY = "useProjectWizard"; //$NON-NLS-1$

	/**
	 * Use wizard by default (the preference is not consulted, when running headless)
	 */
	public static final boolean P_USE_PROJECT_WIZARD_DVAL = true;
}
