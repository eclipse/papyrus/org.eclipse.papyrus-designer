/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base.file;

import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;

public class GenHelper {
	private static IPFileSystemAccess fsa;

	public static void setFSA(IPFileSystemAccess fsa) {
		GenHelper.fsa = fsa;
		
	}
	public static void file(String fileName, CharSequence contents) {
		if (fileName.startsWith(IPFileSystemAccess.SEP_CHAR)) {
			fileName = fileName.substring(1);
		}
		fsa.generateFile(fileName, contents.toString());
	}	
}