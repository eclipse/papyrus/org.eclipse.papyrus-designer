/*******************************************************************************
 * Copyright (c) 2024 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base;

import org.eclipse.papyrus.designer.languages.common.base.preferences.CodeGenUtils;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageProjectSupport;

/**
 * Automatically choose a project support for that is either based on UI or non-UI mechanisms
 */
public class AutoLanguageProjectSupport {

	public static final boolean HEADLESS = Boolean.getBoolean("papyrus.run-headless"); //$NON-NLS-1$

	/**
	 * Prefix to use for UI variant of project generation (if any). Background: code generators
	 * are also used in headless mode. In this case, we cannot call a project creation wizard with
	 * UI pages
	 */
	public static final String UI_PREFIX = "UI:"; //$NON-NLS-1$
	
	/**
	 * Get the project support for a given language editor.
	 * 
	 * @param language
	 *            a programming language
	 * @return the project support
	 */
	public static ILangProjectSupport getProjectSupport(String language) {
		if (!HEADLESS) {
			if (CodeGenUtils.useProjectWizard()) {
				// use UI variant of creation mechanism
				ILangProjectSupport ps = LanguageProjectSupport.getProjectSupport(AutoLanguageProjectSupport.UI_PREFIX + language);
				if (ps != null) {
					return ps;
				}
				// fallback to support without UI prefix
			}
		}
		return LanguageProjectSupport.getProjectSupport(language);
	}
}
