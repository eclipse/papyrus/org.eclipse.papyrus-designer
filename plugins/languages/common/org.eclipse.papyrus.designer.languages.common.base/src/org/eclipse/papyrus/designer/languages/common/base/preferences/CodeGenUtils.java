/*******************************************************************************
 * Copyright (c) 2024 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base.preferences;

import org.eclipse.papyrus.designer.infra.base.ScopedPreferences;
import org.eclipse.papyrus.designer.languages.common.base.Activator;

/**
 * Utility class that returns the C++ preference values (eventually taking
 * stereotypes into account)
 */
public class CodeGenUtils {

	protected static ScopedPreferences prefs = null;

	public static boolean useProjectWizard() {
		initPreferenceStore();
		return prefs.getBoolean(CodeGenConstants.P_USE_PROJECT_WIZARD_KEY, CodeGenConstants.P_USE_PROJECT_WIZARD_DVAL);
	}

	public static void initPreferenceStore() {
		if (prefs == null) {
			prefs = new ScopedPreferences(Activator.PLUGIN_ID);
		}
	}
}
