/*******************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base;

import java.util.List;

import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.GeneratorHint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;


/**
 * A set of static methods related to generator hints
 * Moved from DepUtils in the context of 574776 - Remove MARTE dependency from the Designer common feature
 */
public class HintUtils {

	/**
	 * Determine whether the stereotype GeneratorHint is applied to the passed element
	 * or one of its parents
	 *
	 * @param element
	 *            an element (typically a classifier or a package)
	 * @return the applied stereotype or null
	 */
	public static GeneratorHint getGeneratorHintFromElement(Element element) {
		GeneratorHint codeGenOpt = UMLUtil.getStereotypeApplication(element, GeneratorHint.class);
		if ((codeGenOpt != null) && (codeGenOpt.getLanguage() != null)) {
			return codeGenOpt;
		} else if (element.getOwner() instanceof Package) {
			return getGeneratorHintFromElement(element.getOwner());
		} else {
			return null;
		}
	}
	
	/**
	 * Determine which programming language should be generated for a classifier. The
	 * stereotype GeneratorHint (which could be on any owning package) is evaluated.
	 *
	 * @param element
	 *            an element (typically a classifier or a package)
	 * @return the programming language
	 */
	public static String getLanguageFromElement(Element element) {
		GeneratorHint codeGenOpt = HintUtils.getGeneratorHintFromElement(element);
		if ((codeGenOpt != null) && (codeGenOpt.getLanguage() != null)) {
			return codeGenOpt.getLanguage().getBase_Class().getName();
		} else {
			List<ILangCodegen> eligibleGenerators = LanguageCodegen.getEligibleGeneratorList(LanguageCodegen.MATCH_ALL, element);
			// get language from first eligible generator
			if (eligibleGenerators.size() > 0) {
				return LanguageCodegen.getLanguage(eligibleGenerators.get(0));
			}
			else {
				// Use C++ as default generation language
				return "C++"; //$NON-NLS-1$
			}
		}
	}
}
