/*******************************************************************************
 * Copyright (c) 2013, 2021 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation, bug 574776
 *   
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.codegen.ui;

import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.GeneratorSelectionDialog;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.Messages;
import org.eclipse.papyrus.designer.languages.common.profile.CommonProfileResource;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.GeneratorHint;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.Language;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ChooseGenerator {
	// name of package in which languages are stored
	public static final String LANGUAGES_PKG = "languages"; //$NON-NLS-1$

	protected static ILangCodegen codeGen;

	/**
	 * Interactively choose a generator for a given set of languages (pattern).
	 * If only one generator is available, it returns this generator
	 * immediately.
	 * 
	 * @param languagePattern
	 * @param pe
	 *            a packageable element for which we want to choose a code generator
	 * @return a list of code generators
	 */
	public static ILangCodegen choose(Pattern languagePattern, PackageableElement pe) {
		final List<ILangCodegen> eligibleGenerators = LanguageCodegen.getEligibleGeneratorList(languagePattern, pe);
		if (eligibleGenerators.size() == 0) {
			// retry with all generators (eligibility is currently based on the application of language-specific
			// profiles which might not be applied by users)
			eligibleGenerators.addAll(LanguageCodegen.getCodegenList(languagePattern));
		}

		if (eligibleGenerators.size() == 1) {
			return eligibleGenerators.get(0);
		}
		if (eligibleGenerators.size() > 1) {
			codeGen = null;
			Display.getDefault().syncExec(new Runnable() {

				@Override
				public void run() {
					Shell shell = Display.getCurrent().getActiveShell();
					GeneratorSelectionDialog dialog = new GeneratorSelectionDialog(shell, eligibleGenerators);
					if (dialog.open() == IDialogConstants.OK_ID) {

						codeGen = (ILangCodegen) dialog.getResult()[0];
						Boolean storeResult = (Boolean) dialog.getResult()[1];
						if (storeResult) {
							storeGeneratorID(pe, LanguageCodegen.getLanguage(codeGen), LanguageCodegen.getID(codeGen));
						}
					}
				}
			});
			return codeGen;
		} else {
			// echo language pattern in a more readible way: without escape characters that are used for
			// the "+" in C++
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					Shell shell = Display.getCurrent().getActiveShell();
					String pattern = languagePattern.pattern().replace("\\", ""); //$NON-NLS-1$ //$NON-NLS-2$
					MessageDialog.openError(shell, Messages.LanguageCodegen_NoGeneratorsFound,
							String.format(Messages.LanguageCodegen_NoGeneratorsFoundLong, pattern));
				}
			});
		}

		return null;
	}

	public static void storeGeneratorID(PackageableElement pe, String languageName, final String generatorID) {
		final Package root = PackageUtil.getRootPackage(pe);
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(pe);

		domain.getCommandStack().execute(new RecordingCommand(domain, Messages.ChooseGenerator_CHOOSE_GENERATOR) {

			@Override
			public void doExecute() {
				Package commonProfile =
						PackageUtil.loadPackage(CommonProfileResource.PROFILE_PATH_URI, pe.eResource().getResourceSet());
				root.applyProfile((Profile) commonProfile);

				NamedElement languageNE = ElementUtils.getQualifiedElementFromRS(pe, LANGUAGES_PKG + NamedElement.SEPARATOR + languageName);
				if (languageNE == null) {
					PackageUtil.loadPackage(CommonProfileResource.LANGUAGES_PATH_URI, pe.eResource().getResourceSet());
					languageNE = ElementUtils.getQualifiedElementFromRS(pe, LANGUAGES_PKG + NamedElement.SEPARATOR + languageName);
				}
				final Language language = (languageNE != null) ? UMLUtil.getStereotypeApplication(languageNE, Language.class) : null;

				GeneratorHint hint = StereotypeUtil.applyApp(root, GeneratorHint.class);
				hint.setLanguage(language);
				hint.setGeneratorID(generatorID);
			}
		});
	}
}
