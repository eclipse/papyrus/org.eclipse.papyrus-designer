/*******************************************************************************
 * Copyright (c) 2024 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.codegen.ui.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.papyrus.designer.languages.common.base.Activator;
import org.eclipse.papyrus.designer.languages.common.base.preferences.CodeGenConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * This class defines the common code generation preferences of Papyrus SW
 * designer
 */
public class CodegenPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public CodegenPreferencePage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE, Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("Customize common aspects of Papyrus SW designer code generation"); //$NON-NLS-1$
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI
	 * blocks needed to manipulate various types of preferences. Each field editor
	 * knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		addField(new BooleanFieldEditor(CodeGenConstants.P_USE_PROJECT_WIZARD_KEY,
				"Use wizard for project creation (don't create automatically)", getFieldEditorParent())); //$NON-NLS-1$
	}

	@Override
	public void init(IWorkbench workbench) {
	}

}
