/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.common.profile;

/**
 * COnstants used by TemplateBinding replacement
 *
 */
public class TemplateBindingConstants {
	public static final String TYPE_NAME_TAG = "[typeName]"; //$NON-NLS-1$

	public static final String LOWER = "[lower]"; //$NON-NLS-1$

	public static final String UPPER = "[upper]"; //$NON-NLS-1$
}
