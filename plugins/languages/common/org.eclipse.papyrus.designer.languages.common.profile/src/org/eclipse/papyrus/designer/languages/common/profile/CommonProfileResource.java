/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.common.profile;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations on common codegen profile
 */
public final class CommonProfileResource {

	public static final String PROFILE_PATHMAP = "pathmap://Codegen_PROFILES/"; //$NON-NLS-1$

	public static final String PROFILE_PATH = PROFILE_PATHMAP + "Codegen.profile.uml"; //$NON-NLS-1$

	public static final String LANGUAGES_PATH = PROFILE_PATHMAP + "languages.uml"; //$NON-NLS-1$

	public static final URI PROFILE_PATH_URI = URI.createURI(PROFILE_PATH);

	public static final URI LANGUAGES_PATH_URI = URI.createURI(LANGUAGES_PATH);

	public static final String PROFILE_URI = "http://www.eclipse.org/papyrus/Codegen/1"; //$NON-NLS-1$
}
