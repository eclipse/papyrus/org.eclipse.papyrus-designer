/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.extensionpoints;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * Extension point that is implemented by a code generator for a specific
 * programming language
 */
public interface ILangCodegen {

	/**
	 * FILE KIND enumeration. It supports two distinction options (this might
	 * need revision in the future, but is sufficient for all currently
	 * supported languages)
	 */
	public enum FILE_KIND {
		HEADER, BODY
	}
	
	/**
	 * Generate code for a specific language
	 *
	 * @param project
	 *            a project into which code is generated. if null, project is
	 *            determined automatically (see getTargetProject below)
	 * @param element
	 *            a packageable element, typically a classifier or a package
	 * @param monitor
	 *            a progress monitor
	 */
	public void generateCode(IProject project, PackageableElement element, IProgressMonitor monitor);

	/**
	 * Code might be generated into a specific project. It might be the project
	 * that hosts the UML file, but it may also be another project that is
	 * determined for instance by means of a naming convention (such as a common
	 * prefix and the name of the model). This functions locates and returns the
	 * target project for the given packageable element. If the project exists,
	 * but does not have the nature that is required by the language specific
	 * editor, the user is asked whether the nature should be added.
	 *
	 * Ensures that the target project is correctly setup to contain generated
	 * C/C++ code. Does not create a new project, but may modify existing ones.
	 *
	 * @param pe
	 *            a packageable element within a model
	 * @param createIfMissing
	 *            if true, a new project is created if it does not exist yet. if
	 *            false, null is returned if the project does not exist.
	 * @return the associated project (or null)
	 */
	public IProject getTargetProject(PackageableElement pe, boolean createIfMissing);

	/**
	 * Obtain fileName of file(s) generated for a named element. Delegates to
	 * location strategy. Caller must eventually add extensions.
	 * 
	 * @param project
	 *            a project into which code is generated. if null, project is
	 *            determined automatically
	 * @param element
	 *            a named element
	 * @return a project relative file name
	 */
	public String getFileName(IProject project, NamedElement element);

	/**
	 * Remove all elements that have NOT been generated
	 * This method assures that code files are removed for instance
	 * after the associated class has been renamed.
	 *
	 * @param project
	 *            project in which code should be generated
	 * @param element
	 *            the element for which the generate code should be removed
	 */
	public void cleanCode(IProject project, PackageableElement element, IProgressMonitor monitor);
	
	/**
	 * Remove all elements that have NOT been generated
	 * Accept a list of files that should be kept
	 * 
	 * @param project
	 *            project in which code should be generated
	 * @param element
	 *            the element for which the generate code should be removed
	 * @param keepFiles
	 *            a list of filenames corresponding to files that should be kept.
	 */
	public void cleanCode(IProject project, PackageableElement element, List<String> keepFiles, IProgressMonitor monitor);

	/**
	 * Provide a user friendly description of the generator
	 */
	public String getDescription();

	/**
	 * Some code generators use a non-trivial mapping from behaviors to methods
	 * in the generated code or add methods that are not part of a existing
	 * behavior in the model. This is a problem for code synchronization (update
	 * of model from code) as done for instance with the CDT editor integration.
	 * 
	 * @param methodName
	 *            the name of the method as in the code
	 * @param body
	 *            the body
	 * @return the associated synchronization information. Null indicates that a
	 *         default mapping is used
	 */
	public SyncInformation getSyncInformation(String methodName, String body);

	/**
	 * Return true, if the generator is eligible for a certain element within a
	 * model. The code generator may check the element itself, but also for
	 * instance whether a certain profile (such as UML-RT) has been applied.
	 * Generators are allowed to return true for all models, if they do not have
	 * specific (profile related) requirements.
	 * 
	 * @param modelElement
	 *            An element of a UML model (for which code should be generated)
	 * @return true, iff the generator is eligible for this model
	 */
	public boolean isEligible(Element modelElement);

	/**
	 * Return the suffix of a code generator. Some languages use different
	 * suffixes depending on the file kind, e.g. C distinguishes between HEADER
	 * and BODY languages. Other languages can ignore this parameter.
	 * 
	 * @param fileKind
	 *            The file kind
	 * @return the suffix
	 */
	public String getSuffix(FILE_KIND fileKind);

	/**
	 * return additional information about the method that has been generated
	 * for a UML behavior. This information is used by the CDT editor
	 * integration to locate the method in the code.
	 * 
	 * @param operationOrBehavior
	 *            an operation or behavior (named element is just a common ancestor)
	 */
	public MethodInfo getMethodInfo(NamedElement operationOrBehavior);
}
