/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.idl.codegen;

import org.eclipse.uml2.uml.NamedElement

/**
 * Namespace related tools
 * As for C++, skip top-level namespace (test whether owner !== null)
 */
class Utils {
	static def openNamespaceIDL(NamedElement type) '''
		«FOR ns : type.allNamespaces.clone.reverse»
			«IF ns.owner !== null»
				module «ns.name» {
			«ENDIF»
		«ENDFOR»
	'''

	static def closeNamespaceIDL(NamedElement type) '''
		«FOR ns : type.allNamespaces.clone.reverse»
			«IF ns.owner !== null»
				}; // of module «ns.name»
			«ENDIF»
		«ENDFOR»
	'''


	static def dirName(NamedElement type) '''
		«FOR ns : type.allNamespaces SEPARATOR('/')»
			«ns.name»
		«ENDFOR»
	'''
}
