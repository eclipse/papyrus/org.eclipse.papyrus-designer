# Papyrus SW Designer

Papyrus SW designer is a code generation extension for [Papyrus](https://eclipse.dev/papyrus/). It also supports model transformations (prior to code generation) for high level concepts. For more information, please consult the [SW Designer wiki](https://gitlab.eclipse.org/eclipse/papyrus/org.eclipse.papyrus-designer/-/wikis/home).

## Source code build

Papyrus SW designer can be installed using maven. Besides the standard goals, it supports a profile to create an update site (-P p2) or an Eclipse RCP (-P product). Replace targetrelease with a supported platform (see folder targetplatform).

```
mvn clean verify -Declipse.targetrelease=2023-03

mvn clean verify -Declipse.targetrelease=2023-03 -P p2
```
